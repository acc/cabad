#!/bin/bash

# Allow to overwrite the location of the Yocto SDK
: "${YOCTO_SDK_ENV:="/common/usr/embedded/yocto/fesa/current/sdk/environment-setup-core2-64-ffos-linux"}"

GLOBAL_RELEASE_DIR=/common/usr/cscofe/opt/cabad
TARGET=x86_64

usage(){
  echo "Usage: $0 [--release [path]] [--target yocto]"
  
  echo "-r | --release"
  echo "   Install path prefix. If only option is provided without any paths $GLOBAL_RELEASE_DIR is used."
  echo "-t | --target"
  echo "   $TARGET when not specified"
  echo "-h | --help"
  
  exit 1
}

DEBUG=y
USER_RELEASE_DIR=""
USER_TARGET=$TARGET

# Loop through all the arguments and determine provided options.
while [ "${1:-}" != "" ]; do
    case $1 in
                                # In case the next argument is empty or it starts with a dash,
                                # it means we only got --release, otherwise we expect an argument
                                # after --release and we need to shift.
        -r | --release )        if [[ $2 == -* ]] || [[ $2 == "" ]]; then
                                    USER_RELEASE_DIR=$GLOBAL_RELEASE_DIR
                                else
                                    shift
                                    USER_RELEASE_DIR=$1
                                fi
                                DEBUG=""
                                ;;
        -t | --target )         shift
                                USER_TARGET=$1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

# Based on the build target select the build directory (build by default & build-yocto for yocto SDK).
# Using a different build directory for yocto builds since mixing between the two cmake versions doesn't work to well.
if [[ $USER_TARGET == x86_64 ]]; then
    BUILD_DIR_NAME=build-x86_64
elif [[ $USER_TARGET == yocto ]]; then
    TARGET=yocto
    BUILD_DIR_NAME=build-yocto
else
    echo "Unsupported target" $USER_TARGET
    usage
    exit 1
fi

SCRIPT_PATH=$(dirname $(readlink -f "$0"))
BUILD_DIR=$SCRIPT_PATH/$BUILD_DIR_NAME

# If user didn't provide a release directory use the build directory. Otherwise use whatever the user provided.
if [[ $USER_RELEASE_DIR == "" ]]; then
    RELEASE_DIR=$BUILD_DIR
else
    RELEASE_DIR=$USER_RELEASE_DIR
fi

BUILD_TYPE=Release
# Enable debug compilation when not releasing.
if [[ $DEBUG == y ]]; then
    BUILD_TYPE=Debug
fi

echo "------------------------------------------------------------------------"
echo "Build type:       " $BUILD_TYPE
echo "Build target:     " $TARGET
echo "Build directory:  " $BUILD_DIR
echo "Install directory:" $RELEASE_DIR
echo "------------------------------------------------------------------------"

if [[ $TARGET == yocto ]]; then
    if [ ! -f "$YOCTO_SDK_ENV" ]; then
      echo "Error: Build environment for target '$TARGET' not found at '$YOCTO_SDK_ENV'."
      echo "configure aborted"
      exit 1
    fi

    # Configure yocto environment.
    unset LD_LIBRARY_PATH; source $YOCTO_SDK_ENV
    cmake -S . -B $BUILD_DIR -DBUILD_TARGET=$TARGET -DCMAKE_INSTALL_PREFIX=$RELEASE_DIR -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DCABAD_LIB_ONLY=ON
else
    cmake -S . -B $BUILD_DIR -DBUILD_TARGET=$TARGET -DCMAKE_INSTALL_PREFIX=$RELEASE_DIR -DCMAKE_BUILD_TYPE=$BUILD_TYPE
fi

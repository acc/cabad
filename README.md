# **ATTENTION !! THIS PROJECT IS WIP !!!**

## cabad

**c**ircular **a**cquisition **b**uffer for **a**ccelerator **d**evices

The main target of this library is to buffer and package incoming data of accelerator devices in a standardized way. Metadata (E.g. Timing-System related) will be attached to the data, if available. Incoming data/metadata can be pushed asynchroniusly for each channel.

The packaging is tailored to the use-case "accelerator devices". E.g. there is a packaging mode called "FULL_SEQUENCE" which shows a spefic time-window relevant for accelerator operation.

The buffer is lock-free in the way, that readers will not block the writer. In case of multiple writers, the user must use a lock to synchronize access to the writer threads.

- Meta tags are used to store related timing metainfo and to allign the data in time
- Supported acquisition modes are STREAMING, FULL_SEQUENCE and TRIGGERED.
- Clients can listen to a queue in order to obtain specific data-packages, based on several parameters

### Some notes on the bare circular buffer:
- A statical allocated buffer is used
- The buffer supports single-writer and multiple-reader threads.
- Readers dont need to mutex lock the buffer for reading. They will not block the write iterator.
- The reader can copy the acquisition data from reader windows. Afterwards the reader should validate the window to make sure that the write head did not write on it during the copy process. For implementation details see comments on CircularBufferBase::n_data_pushed_total_.
- If multiple writers are required the user of the library must use a mutex to snychronize the writer threads.

### Requirements which led to the current design
- Possibility to use memcopy on the data (to be as fast as possibly)
- Fixed buffer size (no memory allocation during runtime)
- read iterators should not block write iterators
- A simple way to use the buffer in FESA classes

Why would you want to use this buffer over some other public implementations (e.g. boost circular buffer) ?
- There was no public implementation which fulfilled our requirements. E.g. check https://stackoverflow.com/a/890269/1599887
- When writing this, there was no circular buffer arround, which solved the "non-blocking write iter" aspect for a fixed buffer size

### Build from source
- ./configure.sh
- ./build.sh

### Build and run Unit and Integration Tests
- ./configure.sh
- ./build.sh
- cd build && ctest

### Run only a specific test 
- cd build
- ctest -R name_of_your_test

### Run only a specific test via gdb
- gdb --args ./build/cabad-test --gtest_filter=CircularBufferManagerBaseTestFixture*

### Install to default install location
- ./configure.sh --release
- ./build.sh
- ./install.sh

### Simple Usage Example
Check the basic example inside the `example` folder for a simple test:
- build cabad following the steps in [Build from source](#build-from-source).
- cd build && ./cabad_example

### More Information
- Subscribe to the <a href='http://www-listserv.gsi.de/cgi-bin/wa?A0=CABAD-NEWS ' target='_self'>Cabad News Mailing List</a> to be notified on new versions and bugfixes
- Ask any questions on the <a href='https://www-listserv.gsi.de/cgi-bin/wa?A0=CABAD-SUPPORT ' target='_self'>Cabad Support Mailing List</a>

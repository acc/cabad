
#include <cabad/ContextTracker.h>
#include <cabad/DataReadyQueue.h>
#include <cabad/DeviceDataBufferBase.h>

#include <example/SignalSource.h>
#include <example/CircularBufferManager.h>

#include <chrono>
#include <thread>//sleep

#include <memory>//smart pointers

// global, in order to keep things simple (needed in the callback)
CabadExample::SignalSource* source1;
CabadExample::SignalSource* source2;

void circular_buffer_log_function(cabad::LogLevel severity, const std::string& file, int line, const std::string& message, const std::string& topic)
{
    std::string severityStr;
    switch (severity)
    {
    case cabad::LogLevel::DEBUG:
        severityStr = "Debug";
        break;
    case cabad::LogLevel::TRACE:
        severityStr = "Trace";
        break;
    case cabad::LogLevel::INFO:
        severityStr = "Info";
        break;
    case cabad::LogLevel::WARNING:
        severityStr = "Warning";
        break;
    case cabad::LogLevel::ERROR:
        severityStr = "Error";
        break;
    default:
        break;
    }
    std::cout << severityStr << ": " << file  << ":" << line << " Message: " << message << " Topic: " << topic << std::endl;
}

void cb_data_ready(uint32_t data_id, cabad::CircularBufferManagerBase<CabadExample::DataSample> *manager)
{
    cabad::ClientNotificationData<CabadExample::DataSample> data;
    std::string signal_name = data.sinkName_;
    std::cout << signal_name << ", ";
    
    // Find the according source,etc.
    CabadExample::SignalSource* source;
    if(signal_name == source1->signal_name_)
        source = source1;
    else
        source = source2;
    
    manager->getDataByID(data_id, data);

    std::size_t buffer_size = 10000;
    std::size_t n_samples_received = 0;
    CabadExample::DataSample values[buffer_size];
    source->circular_buffer_manager_->copyDataWindow(&data.window_, &values[0], buffer_size, n_samples_received);
    std::cout << n_samples_received << " samples received for the signal: " << signal_name <<  "(update freq " << data.updateFrequency_ << "Hz): ";
    for(std::size_t i = 0; i < n_samples_received; i++)
    {
        std::cout << values[i].x << ",";
        // print only first 5 samples to dont pollute the screen
        if(i==4)
        {
            std::cout << "..." << std::endl;
            break;
        }
    }
}

int main (void)
{
    // set a log function for the circular buffer
    cabad::setLogFunction(circular_buffer_log_function);
    cabad::Events::initEvents(257, 258);

    // Buffer specific to each (FESA) device, specifying on which events trigger data is expected
    const std::vector<std::string> triggerEvents;
    cabad::DeviceDataBufferBase deviceData(triggerEvents, "FesaDeviceNomen");

    // Used to track e.g. WhiteRabbit context information. (Which is not stored long enough by FESA)
    cabad::ContextTracker contextTracker(100);

    float sample_rate_hz = 1000; // we simulate a 1kHz data source
    float buffer_time_seconds = 10;
    source1 = new CabadExample::SignalSource(deviceData,"Signal1",sample_rate_hz,buffer_time_seconds,contextTracker, 1000, cb_data_ready);
    source2 = new CabadExample::SignalSource(deviceData,"Signal2",sample_rate_hz,buffer_time_seconds,contextTracker, 1000, cb_data_ready);

    while(true)
    {
        // Simplified ... each source as well could push new data in its own thread
        source1->simulate_data(100);
        source2->simulate_data(100);
        std::this_thread::sleep_for(std::chrono::milliseconds(100)); //since we have client updaterate 10Hz
    }

    return 0;
}



/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

namespace CabadExample
{

/* Usually required in order to inject a WR context into the cabad library */
class TimingContext: public cabad::TimingContext
{
    public:
        // returns timestamp in nanosecond precision
        int64_t getTimeStamp() const override
        {
            return 4711;
        }

        // We assume that different events which can occur are numbered
        unsigned int getEventNumber() const override
        {
            return 1234;
        }

        int64_t getID() const override
        {
            return 0;
        }

    private:

        // Mostly likely you wnat the following (together with an extra constrictor and a getter)
        // boost::shared_ptr<fesaGSI::TimingContextWR> contextWR_;

};

} // namespace

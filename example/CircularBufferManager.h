/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/CircularBufferManagerBase.h>

namespace CabadExample
{

struct DataSample
{
    float x;
    float y;
};

/* Manages all Iterators and Meta-Data of a single CircularSampleBuffer */ 
class CircularBufferManager: public cabad::CircularBufferManagerBase<DataSample>
{

public:

    CircularBufferManager(std::string signal_name,
                            float sample_rate_hz,
                            std::vector<cabad::MaxClientUpdateFrequencyType> maxClientUpdateFrequencies,
                            cabad::ContextTracker* contextTracker,
                            cabad::DeviceDataBufferBase* deviceDataBuffer,
                            cabad::ClientNotificationType clientNotificationType,
                            cabad::DataPushMode dataPushMode,
                            std::size_t circularBufferSize,
                            std::size_t metadataBufferSize,
                            std::size_t dataQueueSize,
                            std::function<void(uint32_t data_id, cabad::CircularBufferManagerBase<DataSample>* manager)> callback):
                 cabad::CircularBufferManagerBase<DataSample>(signal_name,
                                                    sample_rate_hz,
                                                    maxClientUpdateFrequencies,
                                                    contextTracker,
                                                    deviceDataBuffer,
                                                    clientNotificationType,
                                                    dataPushMode,
                                                    circularBufferSize,
                                                    metadataBufferSize,
                                                    dataQueueSize,
                                                    callback)
    {

    }

    void push(float            *values,
                                     float            *errors,
                                     std::size_t             size)
    {
        try
        {
            //    std::cout << "CircularBufferManager::pushing " << values_size << " samples - Number of tags found: " << tags.size() << std::endl;
            std::vector<DataSample> samples;
            for (size_t i = 0; i < size; i++)
            {
                DataSample sample;
                sample.x = values[i];
                sample.y = errors[i];
                samples.push_back(sample);
            }
            
            circularBuffer_.push(samples);

            // TODO: Example on how to push trigger tags or SampleStamps

            pushBackDataFinished();
        }
        catch(std::exception& ex)
        {
            std::cout << "Something went wrong: " << ex.what() << std::endl;
        }
    }

    void copyDataWindow(cabad::CircularWindowIterator<DataSample>* window_iter, DataSample* values, const std::size_t& buffer_size, std::size_t& n_data_written)
    {
		circularBuffer_.copyDataWindow(window_iter->getDataStart(), window_iter->getDataEnd(), window_iter->getWindowValidationCheckpoint(), values, buffer_size, n_data_written);
		std::cout << "copyDataWindow - copied " << n_data_written << "samples" << std::endl;
    }

};

} //namespace


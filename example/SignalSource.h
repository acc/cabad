/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include "CircularBufferManager.h"

#include <cabad/CircularBufferManagerBase.h>
#include <cabad/DeviceDataBufferBase.h>
#include <cabad/ContextTracker.h>

#include <math.h>

namespace CabadExample
{

class SignalSource
{

public:
    // circular data buffer
    std::unique_ptr<CircularBufferManager> circular_buffer_manager_;

    std::string signal_name_;
    double sim_signal_angle_;

    void simulate_data(std::size_t n_samples)
    {
        float values[n_samples];
        float errors[n_samples];

          for (std::size_t i = 0; i < n_samples; i++)
          {
              values[i]  = sin(sim_signal_angle_);
              errors[i]  = 0;
              sim_signal_angle_ += 0.1;
          }
         circular_buffer_manager_->push(&values[0], &errors[0], n_samples);
    }

    SignalSource(cabad::DeviceDataBufferBase& deviceDataBuffer,
                 std::string signal_name,
                 float sample_rate,
                 float buffer_time,
                 cabad::ContextTracker& context_tracker,
                 std::size_t dataQueueSize,
                 std::function<void(uint32_t data_id, cabad::CircularBufferManagerBase<DataSample>* manager)> callback) : signal_name_(signal_name)
    {
        sim_signal_angle_ = 0.0;
        std::size_t meta_buffer_max_size = 100;
        std::vector<cabad::MaxClientUpdateFrequencyType> maxClientUpdateFrequencies = {1,10}; // In Hz

        // TODO: Add Example for triggered SignalSource

        std::size_t size_buffer = std::size_t( ceil( sample_rate * buffer_time));
        cabad::ClientNotificationType clientNotifType = cabad::ClientNotificationType::STREAMING | cabad::ClientNotificationType::FULL_SEQUENCE;

        circular_buffer_manager_.reset(new CircularBufferManager(signal_name,
                                                                 sample_rate,
                                                                 maxClientUpdateFrequencies,
                                                                 &context_tracker,
                                                                 &deviceDataBuffer,
                                                                 clientNotifType,
                                                                 cabad::DataPushMode::CONTINOUS_MULTI_SAMPLES,
                                                                 size_buffer,
                                                                 meta_buffer_max_size,
                                                                 dataQueueSize,
                                                                 callback));
    }
};

} //namespace


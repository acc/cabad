/* Jenkinsfile to build and install cabad repository.
   Supports `yocto` (label 'yocto') and `x86_64` (label 'el9') targets. */

properties([pipelineTriggers([pollSCM('H * * * *')])])

def selectAgent() {
    if(env.TARGET == 'yocto'){
        return 'yocto'
    }
    else if(env.TARGET == 'x86_64'){
        return 'el9'
    }
    else {
        error("The target '${env.TARGET}' is not supported")
    }
}

def selectWorkspace() {
    return "workspace/${env.JOB_NAME}/${env.TARGET}"
}

def sourceYoctoSDK() {
    if(env.TARGET == 'yocto'){
        sh("unset LD_LIBRARY_PATH && source ${env.YOCTO_SDK_ENV}")
    }
}

pipeline {
    agent none
    options {
        skipDefaultCheckout(true)
    }
    environment {
        YOCTO_SDK_ENV = "/common/jenkins/yocto/fesa/environment-setup-core2-64-ffos-linux"
    }
    stages {
        stage('BuildAndTest'){
            matrix {
                agent any
                axes {
                    axis {
                        name 'TARGET'
                        values 'yocto', 'x86_64'
                    }
                }
                stages{
                    stage('Configure') {
                        agent {
                            node {
                                label selectAgent()
                                customWorkspace selectWorkspace()
                            }
                        }
                        steps {
                            cleanWs()
                            checkout scm
                            script{
                                sourceYoctoSDK()
                                sh("./configure.sh --target ${TARGET}")
                            }
                        }
                    }
                    stage('Build') {
                        agent {
                            node {
                                label selectAgent()
                                customWorkspace selectWorkspace()
                            }
                        }
                        steps {
                            script{
                                sourceYoctoSDK()
                                sh("./build.sh --target ${TARGET}")
                            }
                        }
                    }
                    stage('Install') {
                        agent {
                            node {
                                label selectAgent()
                                customWorkspace selectWorkspace()
                            }
                        }
                        steps {
                            script{
                                sourceYoctoSDK()
                                sh("./install.sh --target ${TARGET}")
                            }
                        }
                    }
                    stage('Test') {
                        agent {
                            node {
                                label selectAgent()
                                customWorkspace selectWorkspace()
                            }
                        }
                        when {
                            expression { return env.TARGET == 'x86_64' }
                        }
                        steps {
                            sh("cd build-${env.TARGET} && ctest")
                        }

                    }
                }
            }
        }
    }
    post{
        unstable {
            emailext(
                subject: "Unstable Build: ${env.JOB_NAME} #${env.BUILD_NUMBER}",
                body: """
                    Job: ${env.JOB_NAME}
                    Build: #${env.BUILD_NUMBER}
                    Build URL: ${env.BUILD_URL}
                """,
                to: 'al.schwinn@gsi.de'
            )
        }
        failure {
            emailext(
                subject: "Build failed after commit ${env.GIT_COMMIT}!",
                body: """
                    Job: ${env.JOB_NAME}
                    Build: #${env.BUILD_NUMBER}
                    Build URL: ${env.BUILD_URL}
                """,
                recipientProviders: [developers()]
            )
        }
    }
}

#!/bin/bash

TARGET=x86_64

usage(){
  echo "Usage: $0 [--target yocto]"
  
  echo "-t | --target"
  echo "   $TARGET when not specified"
  echo "-h | --help"
  
  exit 1
}

USER_TARGET=$TARGET

# Loop through all the arguments and determine provided options.
while [ "${1:-}" != "" ]; do
    case $1 in
        -t | --target )         shift
                                USER_TARGET=$1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

# Based on the build target select the build directory (build by default & build-yocto for yocto SDK).
# Using a different build directory for yocto builds since mixing between the two cmake versions doesn't work to well.
if [[ $USER_TARGET == x86_64 ]]; then
    BUILD_DIR_NAME=build-x86_64
elif [[ $USER_TARGET == yocto ]]; then
    TARGET=yocto
    BUILD_DIR_NAME=build-yocto
else
    echo "Unsupported target" $USER_TARGET
    usage
    exit 1
fi

SCRIPT_PATH=$(dirname $(readlink -f "$0"))
BUILD_DIR=$SCRIPT_PATH/$BUILD_DIR_NAME

echo "------------------------------------------------------------------------"
echo "Build target:     " $TARGET
echo "Build directory:  " $BUILD_DIR
echo "------------------------------------------------------------------------"

cd $SCRIPT_PATH
cmake --install $BUILD_DIR

/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <test/ContextTrackerFixture.h>
#include <test/TimingContextMock.h>

#include <cabad/DeviceDataBufferBase.h>

namespace cabad
{

TEST_F(ContextTrackerFixture, getContextRange)
{
	std::size_t size = 10;
	ContextTracker tracker(size);
	std::deque < std::shared_ptr<const TimingContext> > result;

	// Fill the tracker with stamps [ 0 ... 9 ] eventNumbers [10 ... 19] and is's [ 20 ... 29 ]
	for (std::size_t i= 0;i<size;i++)
	{
		std::shared_ptr<const TimingContext> context(new TimingContextMock(i, i+10,i+20));
		tracker.addContext(context);
	}

	// Nothing found
	result = tracker.getContextRange(10,12);
	ASSERT_TRUE(result.empty());

	// Invalid arguments
    ASSERT_THROW(
    		tracker.getContextRange(5,4);
            ,std::exception);

	// Multiple matches
    result = tracker.getContextRange(3,5);
    ASSERT_EQ(result[0]->getTimeStamp(),3);
    ASSERT_EQ(result[1]->getTimeStamp(),4);
    ASSERT_EQ(result[2]->getTimeStamp(),5);
}

TEST_F(ContextTrackerFixture, getNextTriggerContext)
{
	std::size_t size = 10;
	ContextTracker tracker(size);
	std::shared_ptr<const TimingContext> result;

	std::vector<std::string> fesaConcreteEventNames;
    fesaConcreteEventNames.push_back("MyLayer::CMD_BEAM_INJECTION#283");
	DeviceDataBufferBase device_data_buffer(fesaConcreteEventNames, "MyDevice");

	// Fill the tracker with stamps [10,20,30,40] eventNumbers [283,1,283,1] and is's [0,1,2,3]

	{
		std::shared_ptr<const TimingContext> context(new TimingContextMock(10, 283,0));
		tracker.addContext(context);
	}
	{
		std::shared_ptr<const TimingContext> context(new TimingContextMock(20, 1,1));
		tracker.addContext(context);
	}
	{
		std::shared_ptr<const TimingContext> context(new TimingContextMock(30, 283,2));
		tracker.addContext(context);
	}
	{
		std::shared_ptr<const TimingContext> context(new TimingContextMock(40, 1,3));
		tracker.addContext(context);
	}

	// Nothing found
	result = tracker.getNextTriggerContext(35, device_data_buffer );
	ASSERT_TRUE(!result);

	// Found
	result = tracker.getNextTriggerContext(9, device_data_buffer );
	ASSERT_FALSE(!result);
	ASSERT_EQ(result->getTimeStamp(),10);

	result = tracker.getNextTriggerContext(10, device_data_buffer );
	ASSERT_FALSE(!result);
	ASSERT_EQ(result->getTimeStamp(),10);

	// Skip non-trigger event
	result = tracker.getNextTriggerContext(11, device_data_buffer );
	ASSERT_FALSE(!result);
	ASSERT_EQ(result->getTimeStamp(),30);
}

TEST_F(ContextTrackerFixture, getContextByID)
{
	std::size_t size = 10;
	ContextTracker tracker(size);
	std::shared_ptr<const TimingContext> result;

	// Fill the tracker with stamps [ 0 ... 9 ] eventNumbers [10 ... 19] and is's [ 20 ... 29 ]
	for (std::size_t i= 0;i<size;i++)
	{
		std::shared_ptr<const TimingContext> context(new TimingContextMock(i, i+10,i+20));
		tracker.addContext(context);
	}

	// Nothing found
	result = tracker.getContextByID(19);
	ASSERT_TRUE(!result);

	// Found
	result = tracker.getContextByID(21);
	ASSERT_FALSE(!result);
	ASSERT_EQ(result->getTimeStamp(),1);
}

TEST_F(ContextTrackerFixture, addEvents_overflow)
{
	std::size_t size = 10;
	ContextTracker tracker(size);
	std::shared_ptr<const TimingContext> result;

	// Fill the tracker with stamps twice
	// stamps   [ 0 ... 9 ] eventNumbers [10 ... 19] and is's [ 20 ... 29 ] should be phased out
	// stamps [ 10 ... 19 ] eventNumbers [20 ... 29] and is's [ 30 ... 39 ] should be in
	for (std::size_t i= 0;i<(2*size);i++)
	{
		std::shared_ptr<const TimingContext> context(new TimingContextMock(i, i+10,i+20));
		tracker.addContext(context);
	}

	// Old ones are phased out
	result = tracker.getContextByID(29);
	ASSERT_TRUE(!result);

	// New ones are found
	result = tracker.getContextByID(30);
	ASSERT_FALSE(!result);
	ASSERT_EQ(result->getTimeStamp(),10);
	result = tracker.getContextByID(39);
	ASSERT_FALSE(!result);
	ASSERT_EQ(result->getTimeStamp(),19);
}

} // end namespace

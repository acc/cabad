/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/DataReadyQueue.h>
#include <cabad/SampleMetaData.h>
#include <cabad/CircularBuffer.h>
#include <cabad/DeviceDataBufferBase.h>
#include <gtest/gtest.h>

#include <test/Util.h>

#include <string>
#include <iostream>
#include <memory>

namespace cabad
{

class DataReadyQueueFixture : public ::testing::Test
{

public:

    DataReadyQueueFixture()
    {
        dummy_notificationType_ = ClientNotificationType::FULL_SEQUENCE;
        dummy_sinkName_ = "Sink1";
        dummy_updateFrequency_ = 10;

        device_data_buffer_.reset(new DeviceDataBufferBase(trigger_events_, "FakeDevice", "", "", ""));
        metadata_buffer_.reset(new MetaDataBuffer<float>(100, &(*device_data_buffer_)));
        circular_buffer_.reset(new CircularBuffer<float>(100));
        dummy_window_ = CircularWindowIterator<float>("Dummy", circular_buffer_.get(), metadata_buffer_.get(), false, DataPushMode::CONTINOUS_MULTI_SAMPLES, 1.0);
    }

    ClientNotificationType dummy_notificationType_;
    std::string dummy_sinkName_;
    std::shared_ptr<const TimingContext> dummy_context_;
    MaxClientUpdateFrequencyType dummy_updateFrequency_;
    CircularWindowIterator<float> dummy_window_;
    std::vector<std::string> trigger_events_;
    std::shared_ptr<MetaDataBuffer<float>> metadata_buffer_;
    std::shared_ptr<CircularBuffer<float>> circular_buffer_;
    std::shared_ptr<DeviceDataBufferBase> device_data_buffer_;

    // Is called once for all tests of this fixture
    static void SetUpTestCase();

    //called on each test
    void SetUp();
    void TearDown();

};


void DataReadyQueueFixture::SetUpTestCase()
{
    setLogFunction(fake_log_function);
}
void DataReadyQueueFixture::SetUp()
{

}

void DataReadyQueueFixture::TearDown()
{

}
} // end namespace

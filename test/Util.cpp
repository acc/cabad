/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <test/Util.h>

#include <iostream>

//global variables
bool DEBUG_ENABLED = false;
uint64_t fakeClock = 0;

namespace cabad
{

uint64_t fake_clock_get_utc_now()
{
    // Nanoseconds UTC
    return fakeClock;
}

void fake_log_function(LogLevel severity, const std::string& file, int line, const std::string& message, const std::string& topic)
{
    if(severity == LogLevel::ERROR)
        std::cout << "ERROR: File: " << file << " line: " << std::to_string(line) << " message: " << message << " topic: " << topic << std::endl;
    else if(severity == LogLevel::WARNING)
        std::cout << "WARNING: File: " << file << " line: " << std::to_string(line) << " message: " << message << " topic: " << topic << std::endl;
    else if(severity == LogLevel::INFO && DEBUG_ENABLED)
        std::cout << "INFO: File: " << file << " line: " << std::to_string(line) << " message: " << message << " topic: " << topic << std::endl;
    else if(severity == LogLevel::TRACE && DEBUG_ENABLED)
        std::cout << "TRACE: File: " << file << " line: " << std::to_string(line) << " message: " << message << " topic: " << topic << std::endl;
    else if (severity == LogLevel::DEBUG && DEBUG_ENABLED)
        std::cout << "DEBUG: File: " << file << " line: " << std::to_string(line) << " message: " << message << " topic: " << topic << std::endl;
}

} // end namespace

/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <gtest/gtest.h>

#include <string>
#include <iostream>

#include <cabad/CircularBufferManagerBase.h>

#include <test/TimingContextMock.h>
#include <test/Util.h>

namespace cabad
{

class TestPayload : public MetaPayload
{
public:
    TestPayload(uint64_t x):
        x(x)
    {
    }
    uint64_t x;

    bool operator==(const TestPayload& other) const
    {
        return other.x == x;
    }
};

class CircularBufferManager: public CircularBufferManagerBase<float>
{

public:

    struct FakeMetaInfo
    {
        // Stamp-Only meta
        FakeMetaInfo(int64_t atrigger_stamp, uint64_t aoffset, int32_t astatus)
        {
            trigger_stamp = atrigger_stamp;
            abs_offset = aoffset;
            status = astatus;
            fesaConcreteEventName = "";
        }

        FakeMetaInfo(int64_t atrigger_stamp, uint64_t aoffset, int32_t astatus, std::string afesaConcreteEventName, std::shared_ptr<const TimingContext> acontext, std::shared_ptr<TestPayload> metaPayload)
        {
            trigger_stamp = atrigger_stamp;
            abs_offset = aoffset;
            status = astatus;
            fesaConcreteEventName = afesaConcreteEventName;
            context = acontext;
            payload = metaPayload;
        }
        int64_t trigger_stamp;
        uint64_t abs_offset;
        int32_t status;
        std::string fesaConcreteEventName;
        std::shared_ptr<const TimingContext> context;
        std::shared_ptr<TestPayload> payload;
    };

    CircularBufferManager(const std::string& signal_name,
                           float sample_rate_hz,
                           std::vector<MaxClientUpdateFrequencyType> maxClientUpdateFrequencies,
                           ContextTracker* contextTracker,
                           DeviceDataBufferBase* deviceDataBuffer,
                           ClientNotificationType clientNotificationType,
                           DataPushMode dataPushMode,
                           std::size_t circularBufferSize,
                           std::size_t metadataBufferSize,
                           std::size_t dataQueueSize,
                           std::function<void(uint32_t data_id, CircularBufferManagerBase<float>*)> callback):
         CircularBufferManagerBase(   signal_name,
                                      sample_rate_hz,
                                      maxClientUpdateFrequencies,
                                      contextTracker,
                                      deviceDataBuffer,
                                      clientNotificationType,
                                      dataPushMode,
                                      circularBufferSize,
                                      metadataBufferSize,
                                      dataQueueSize,
                                      callback)
    {
        fakeClock = START_TIME;
    }

    ~CircularBufferManager()
    {

    }

    void push(  const float                 *data,
                std::size_t                  data_size,
                std::vector<FakeMetaInfo>    meta_info);

    void copyDataWindow(const CircularWindowIterator<float>* window_iter, float* buffer1, const std::size_t& buffer_size, std::size_t& n_data_written);
};

class CircularBufferManagerBaseTestFixture : public ::testing::Test
{

public:

    void cbDataReady(uint32_t id, CircularBufferManagerBase<float>* manager)
    {
        ClientNotificationData<float> data;
        manager->getDataByID(id, data);
        clientData_.emplace_back(std::move(data));
        data_ready_flagged_ = true;
    }

    CircularBufferManagerBaseTestFixture()
        : data_ready_flagged_(false),
          preTriggerTime_(0.0),
          postTriggerTime_(0.0),
          dataPushMode_(DataPushMode::DISJUNCT_SINGLE_SAMPLES),
          sampRate_(0.0),
          nSamplesTillPush_(0.0f)
    {
    }

    std::vector<ClientNotificationData<float>> retrieveData()
    {
        auto data = std::move(clientData_);
        clientData_.clear();
        data_ready_flagged_ = false;
        return data;
    }

    std::shared_ptr<CircularBufferManager> buffer_manager_;
    std::shared_ptr<ContextTracker> context_tracker_;
    std::shared_ptr<DeviceDataBufferBase> device_data_buffer_;

    std::vector<ClientNotificationData<float>> clientData_;
    bool data_ready_flagged_;

    uint64_t fakeSampleOffset_;
    uint64_t fakeSamplesPushed_;
    uint64_t fakeSamplesChecked_;

    std::vector<int64_t> fakeSampleStamps_;

    uint64_t preTriggerSamples_;
    uint64_t postTriggerSamples_;

    double preTriggerTime_;
    double postTriggerTime_;

    DataPushMode dataPushMode_;
    std::vector<CircularBufferManager::FakeMetaInfo> triggerTimePushedMeta_;
    // Consists of the number of pushed samples before each trigger-event (used in case of DIJUNCT_SINGLE_SAMPLE mode). Used for testing.
    std::vector<uint64_t> triggerTimeSamplesPushedBeforeEachEvent_;

    std::size_t nTriggersChecked_;

    uint64_t random_measurement_jitter_max_ns_;
    uint64_t random_measurement_jitter_min_ns_;

    // All fake ref trigger stamps with their names
    std::map<int64_t, std::string> fakeRefMetaCol_;

    std::shared_ptr<const TimingContext> prevSeqStartEvent_;
    std::shared_ptr<const TimingContext> currentSeqStartEvent_;

    std::vector<std::string> trigger_events_;
    std::vector<CircularBufferManager::FakeMetaInfo> meta_to_push_;
    std::vector<CircularBufferManager::FakeMetaInfo> meta_pushed_;

    double sampRate_; //Hz
    float nSamplesTillPush_; //number of new samples after which the data is pushed

    void createCircularBufferManager (float sampleRate,
            std::size_t size_buffer,
            std::size_t meta_buffer_size,
            std::size_t contextTrackerSize,
            ClientNotificationType clientNotificationType,
            DataPushMode dataPushMode);

    void wait(int milliseconds);
    void waitWithPush(int milliseconds);
    void injectFakeEvent(std::string fesaConcreteEventName, bool addToMeta=true);
    void injectFakeEventWithTriggerData(std::string fesaConcreteEventName, bool addToMeta=true);

    void pushData();
    void pushSingleSampleWithMeta();
    void injectFakeSequence1_digitizer_streaming(bool end_sequence_with_gap_start = true);
    void injectFakeSequence1_digitizer_triggered();
    void injectFakeSequence1_powersupply_streaming(bool end_sequence_with_gap_start = true);
    void injectFakeSequence1_streaming(DataPushMode dataPushMode, bool end_sequence_with_gap_start = true);
    void checkSequenceStreaming(MaxClientUpdateFrequencyType update_frequency_hz, DataPushMode dataPushMode, bool check_ref_meta = true);
    void checkSequenceStreaming_noTiming(MaxClientUpdateFrequencyType update_frequency_hz, bool expect_ref_meta, bool timeSinceRefMetaSanityCheck);
    void checkSequenceFullSeq(DataPushMode dataPushMode);
    void checkSequenceTriggered();
    void checkSequenceTriggeredTime();
    void validateExpectedSamples(CircularWindowIterator<float>& window_iterator, std::size_t& expectedSamples, std::size_t n_data_written);

    // Is called once for all tests of this fixture
    void SetUp() override;

    // Is called once for all tests of this fixture
    void TearDown() override;

};


class CircularBufferManagerContMultiStreaming : public CircularBufferManagerBaseTestFixture, public ::testing::WithParamInterface<std::tuple<float, MaxClientUpdateFrequencyType>>
{};

class CircularBufferManagerContMultiStreamingTiming : public CircularBufferManagerBaseTestFixture, public ::testing::WithParamInterface<std::tuple<float, MaxClientUpdateFrequencyType>>
{};

class CircularBufferManagerContMultiFullSeq : public CircularBufferManagerBaseTestFixture, public ::testing::WithParamInterface<float>
{};

class CircularBufferManagerTriggered : public CircularBufferManagerBaseTestFixture, public ::testing::WithParamInterface<float>
{};

class CircularBufferManagerDisjSingleStreaming : public CircularBufferManagerBaseTestFixture, public ::testing::WithParamInterface<std::tuple<float, MaxClientUpdateFrequencyType>>
{};

class CircularBufferManagerDisjSingleFullSeq : public CircularBufferManagerBaseTestFixture, public ::testing::WithParamInterface<float>
{};

} // end namespace

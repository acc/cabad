/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/SampleMetaData.h>
#include <cabad/DeviceDataBufferBase.h>
#include <gtest/gtest.h>

#include <string>
#include <iostream>

namespace cabad
{

class SampleMetaDataFixture : public ::testing::Test
{

public:

    SampleMetaDataFixture()
    {
        trigger_events_.push_back("CMD_SEQ_START#257");
        trigger_events_.push_back("CMD_BEAM_INJECTION#283");
        trigger_events_.push_back("EVT_COMMAND#255");
        device_data_buffer_.reset(new DeviceDataBufferBase(trigger_events_, "FakeDevice", "CMD_BEAM_INJECTION#283", "CMD_SEQ_START#257", "EVT_COMMAND#255"));
    }

    // Is called once for all tests of this fixture
    static void SetUpTestCase();

    //called on each test
    void SetUp();
    void TearDown();

    std::vector<std::string> trigger_events_;
    std::shared_ptr<DeviceDataBufferBase> device_data_buffer_;
};

void SampleMetaDataFixture::SetUpTestCase()
{

}
void SampleMetaDataFixture::SetUp()
{
}

void SampleMetaDataFixture::TearDown()
{

}
} // end namespace

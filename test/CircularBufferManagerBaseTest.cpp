/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <test/CircularBufferManagerBaseTestFixture.h>

#include <cabad/CircularBuffer.h>
#include <cabad/CircularBufferManagerBase.h>
#include <cabad/DeviceDataBufferBase.h>
#include <cabad/ContextTracker.h>
#include <cabad/Util.h>

#include <algorithm>
#include <cmath>
#include <string.h> //memcopy

#define samp_rate_1Hz            1.f
#define samp_rate_10Hz          10.f
#define samp_rate_100Hz        100.f
#define samp_rate_1kHz        1000.f
#define samp_rate_10kHz      10000.f
#define samp_rate_100kHz    100000.f
#define samp_rate_1MHz     1000000.f
#define samp_rate_10MHz   10000000.f
#define samp_rate_100MHz 100000000.f

#define FLOAT_COMPARISON_TOLERANCE 1.0e-7 // 100ns tolerance for tests. TODO: Decrease the tolerance !

namespace cabad
{

void CircularBufferManager::push(  const float                  *data,
                                    std::size_t                  data_size,
                                    std::vector<FakeMetaInfo>    meta_to_push)
{
    auto firstNewSample = circularBuffer_.getCopyOfWriteIterator();
    if(DEBUG_ENABLED)
      std::cout << "pushing " << data_size << " sample(s) starting by index: "<< firstNewSample.index() << std::endl;
    circularBuffer_.push(data, data_size);
    auto lastNewSample = circularBuffer_.getCopyOfWriteIterator();
    --lastNewSample;
    //std::cout << "Number of tags found: " << tags.size() << std::endl;
    for (const auto& meta : meta_to_push)
    {
        if( !meta.fesaConcreteEventName.empty() && deviceDataBuffer_->isTriggerEvent((meta.context)->getEventNumber()) )
        {
            //std::cout << "pushing meta for stamp: "<< meta.stamp << std::endl;
            addTriggerMetaData(meta.trigger_stamp, meta.abs_offset, meta.payload);
        }
        else
        {
            //std::cout << "pushing meta for stamp: "<< meta.stamp << std::endl;
            addWrStampMetaData(meta.trigger_stamp, meta.abs_offset, meta.payload);
        }
    }
    addMultiplexingContextDataForRange(firstNewSample, lastNewSample);
    pushBackDataFinished();
}

void CircularBufferManager::copyDataWindow(const CircularWindowIterator<float>* window_iter, float* buffer1, const std::size_t& buffer_size, std::size_t& n_data_written)
{
    circularBuffer_.copyDataWindow(window_iter->getDataStart(), window_iter->getDataEnd(), window_iter->getWindowValidationCheckpoint(), buffer1, buffer_size, n_data_written);
}

void CircularBufferManagerBaseTestFixture::SetUp()
{
    if(DEBUG_ENABLED)
      std::cout << "Setup called " << std::endl;
    fakeClock = START_TIME;
    fakeSampleOffset_ = 0;
    fakeSamplesPushed_ = 0;
    fakeSamplesChecked_ = 0;
    preTriggerSamples_ = 0;
    postTriggerSamples_ = 0;
    nTriggersChecked_ = 0;
    random_measurement_jitter_min_ns_ = 0;
    random_measurement_jitter_max_ns_ = 100;// 100000;// TODO: Currently 100ns to at least test that it does not need to match.. should be be tested till up to 0,9ms (default tolerance 1ms for triggers)
    trigger_events_.push_back("CMD_SEQ_START#257");
    trigger_events_.push_back("CMD_BEAM_INJECTION#283");
    trigger_events_.push_back("CMD_BEAM_EXTRACTION#284");
    trigger_events_.push_back("CMD_START_ENERGY_RAMP#285");
    trigger_events_.push_back("CMD_CUSTOM_DIAG_1#286");
    trigger_events_.push_back("CMD_CUSTOM_DIAG_2#287");
    trigger_events_.push_back("CMD_CUSTOM_DIAG_3#288");
    trigger_events_.push_back("CMD_FG_START#513");
    trigger_events_.push_back("EVT_COMMAND#255");
    trigger_events_.push_back("EVT_GAP_START#258");

    setLogFunction(fake_log_function);
    setTimeStampFunction(fake_clock_get_utc_now);
}

void CircularBufferManagerBaseTestFixture::TearDown()
{
    if(DEBUG_ENABLED)
      std::cout << "TearDown called " << std::endl;
    context_tracker_.reset();
    device_data_buffer_.reset();
    buffer_manager_.reset();
    trigger_events_.clear();
    meta_to_push_.clear();
    meta_pushed_.clear();
    fakeSampleStamps_.clear();
}

void CircularBufferManagerBaseTestFixture::createCircularBufferManager (
        float sampleRate,
        std::size_t size_buffer,
        std::size_t meta_buffer_size,
        std::size_t contextTrackerSize,
        ClientNotificationType clientNotificationType,
        DataPushMode dataPushMode)
{
    sampRate_ = sampleRate;
    nSamplesTillPush_ = sampleRate / 10;
    if(DEBUG_ENABLED)
    {
        std::cout << "sampRate_: " << sampRate_ << std::endl;
        std::cout << "nSamplesTillPush_: " << nSamplesTillPush_ << std::endl;
    }
    std::vector<MaxClientUpdateFrequencyType> maxClientUpdateFrequencies = {1,10,25};
    device_data_buffer_.reset(new DeviceDataBufferBase(trigger_events_, "FakeDevice", "CMD_BEAM_INJECTION#283", "CMD_SEQ_START#257", "EVT_COMMAND#255"));
    context_tracker_.reset(new ContextTracker(contextTrackerSize));
    buffer_manager_.reset(new CircularBufferManager("FakeSignal",
                                                    sampleRate,
                                                    maxClientUpdateFrequencies,
                                                    context_tracker_.get(),
                                                    device_data_buffer_.get(),
                                                    clientNotificationType,
                                                    dataPushMode,
                                                    size_buffer,
                                                    meta_buffer_size,
                                                    100,
                                                    std::bind(&CircularBufferManagerBaseTestFixture::cbDataReady, this, std::placeholders::_1,std::placeholders::_2)));
}

void CircularBufferManagerBaseTestFixture::wait(int milliseconds)
{
    fakeClock += milliseconds * 1000000;
}

void CircularBufferManagerBaseTestFixture::waitWithPush(int milliseconds)
{
	int64_t last_fake_sample_stamp;
    int64_t newTime = fakeClock + milliseconds * 1000000;
    int64_t sampleTosampleDistance_ns = buffer_manager_->getSampleToSampleDistance() *  1000000000;
    if(DEBUG_ENABLED)
      std::cout << "waitWithPush(" << milliseconds << ") - Adding fake stamps for time [" << fakeClock << ","<< newTime-1 << "]" << std::endl;
    for(int64_t time = (int64_t)fakeClock; time < newTime; time+=sampleTosampleDistance_ns)
    {
    	if(fakeSampleStamps_.empty()) // First sample is on stamp 'START_TIME'
    	{
    		fakeSampleStamps_.push_back(START_TIME);
    	}
    	else
    	{
    		last_fake_sample_stamp = fakeSampleStamps_.back();
        	if ((time - last_fake_sample_stamp) < sampleTosampleDistance_ns)
        		continue;
    		fakeSampleStamps_.push_back(last_fake_sample_stamp + sampleTosampleDistance_ns);
    	}

        //std::cout << "fakeSampleStamps_["<<fakeSampleOffset_<<"]: " << fakeSampleStamps_[fakeSampleOffset_] << std::endl;
        fakeSampleOffset_++;
        //std::cout << "fakeSamplesStamp.size()" << fakeSampleStamps_.size() << std::endl;
        if((fakeSampleOffset_ - fakeSamplesPushed_) >= nSamplesTillPush_)
            pushData();
    }
    fakeClock = newTime;
}

// The event will be added to the latest fake sample (it might not been pushed yet)
// The timestamp of the event will be the stamp of that fake event + some random jitter
void CircularBufferManagerBaseTestFixture::injectFakeEvent(std::string fesaConcreteEventName, bool addToMeta )
{
	// As timestamp we always use the stamp of the latest sample which was pushed to the buffer
	std::size_t related_sample_abs_index = fakeSampleOffset_-1;
	int64_t timestamp = fakeSampleStamps_[related_sample_abs_index];
    if (sampRate_ < samp_rate_100Hz)
    {
        // For sample rates which are < 100Hz we use the fakeClock for timestamping directly,
        // otherwise multiple timing events would match the trigger event.
        timestamp = fakeClock;
    }

    unsigned int eventNumber = 0;
    try
    {
        eventNumber = device_data_buffer_->eventName2eventID(device_data_buffer_->extractEventName(fesaConcreteEventName));
    }
    catch(...)
    {
        // thsi will only suceed if the event is a trigger event.
        //All other events will have event number 0
        eventNumber = 4711;
    }

    std::shared_ptr<const TimingContext> context(new TimingContextMock(timestamp, eventNumber));

    context_tracker_->addContext(context);
    if(DEBUG_ENABLED)
    {
        if(device_data_buffer_->isTriggerEvent(eventNumber))
        {
            if(buffer_manager_->getMetaBuffer().isRefMetaTriggerEvent(eventNumber))
                std::cout << "injecting ref trigger stamp: " << timestamp << " Name: " << fesaConcreteEventName << std::endl;
            else
                std::cout << "injecting     trigger stamp: " << timestamp << " Name: " << fesaConcreteEventName << std::endl;
        }
        else
                std::cout << "injecting             stamp: " << timestamp << " Name: " << fesaConcreteEventName << std::endl;
    }
    if(device_data_buffer_->isTriggerEvent(eventNumber))
    {
        if(buffer_manager_->getMetaBuffer().isRefMetaTriggerEvent(eventNumber))
            fakeRefMetaCol_[timestamp] = device_data_buffer_->eventID2eventName(eventNumber);

        if ( fesaConcreteEventName == "CMD_SEQ_START#257")
        {
            prevSeqStartEvent_ = currentSeqStartEvent_;
            currentSeqStartEvent_ = context;
        }
        // add some random offset to trigger stamp to simulate hardware in a realistic way
        int64_t offset = random_measurement_jitter_min_ns_ + rand() % ( random_measurement_jitter_max_ns_ - random_measurement_jitter_min_ns_);
        //std::cout << "random offset: " << offset << std::endl;

        // Add some unique number as a payload.
        std::shared_ptr<TestPayload> payload = std::make_shared<TestPayload>(eventNumber + related_sample_abs_index);
        CircularBufferManager::FakeMetaInfo meta(timestamp + offset,related_sample_abs_index, 0, fesaConcreteEventName, context, payload);
        if(addToMeta)
            meta_to_push_.push_back(meta);

        if (dataPushMode_ == DataPushMode::DISJUNCT_SINGLE_SAMPLES)
        {
            triggerTimePushedMeta_.push_back(meta);
        }
    }
}

void CircularBufferManagerBaseTestFixture::injectFakeEventWithTriggerData(std::string fesaConcreteEventName, bool addToMeta )
{
    if(!device_data_buffer_->isTriggerEvent(device_data_buffer_->eventName2eventID(device_data_buffer_->extractEventName(fesaConcreteEventName))))
    {
        std::cout << "Test configuration error: '"<< fesaConcreteEventName << "' is not a trigger event. This method only should be usedwith trigger events. Use 'injectFakeEvent' for non-trigger events" << std::endl;
        exit(1);
    }

    if (dataPushMode_ == DataPushMode::DISJUNCT_SINGLE_SAMPLES)
    {
        triggerTimeSamplesPushedBeforeEachEvent_.push_back(fakeSamplesPushed_);
        std::size_t triggerSamples = (preTriggerTime_ + postTriggerTime_) / 1000000000. * sampRate_;
        ++triggerSamples;
        int64_t sampleTosampleDistance_ns = buffer_manager_->getSampleToSampleDistance() *  1000000000;

        uint64_t preTriggerSamples = preTriggerTime_ / 1000000000. * sampRate_;

        for( std::size_t samp_count = 0; samp_count < triggerSamples; samp_count++)
        {
            if( samp_count == preTriggerSamples - 1)
            {
                fakeSampleStamps_.push_back(fakeClock);
                fakeSampleOffset_++;
                // Add (samp_count % 5) to vary the distance between the sample timestamps for DISJUNCT_SINGLE_SAMPLE mode
                fakeClock += sampleTosampleDistance_ns + (samp_count % 5);
                injectFakeEvent(fesaConcreteEventName, addToMeta);
            }
            else
            {
                CircularBufferManager::FakeMetaInfo meta(fakeClock, fakeSampleOffset_, 0);
                meta_to_push_.push_back(meta);
                fakeSampleStamps_.push_back(fakeClock);
                fakeSampleOffset_++;
                // Add (samp_count % 5) to vary the distance between the sample timestamps for DISJUNCT_SINGLE_SAMPLE mode
                fakeClock += sampleTosampleDistance_ns + (samp_count % 5);
            }
        }
    }
    else
    {
        std::size_t samples = preTriggerSamples_ + postTriggerSamples_;
        int64_t sampleTosampleDistance_ns = buffer_manager_->getSampleToSampleDistance() *  1000000000;

        for( std::size_t samp_count = 0; samp_count < samples; samp_count++)
        {
            if( samp_count == preTriggerSamples_ + 1 )//trigger tag on first post trigger sample
                injectFakeEvent(fesaConcreteEventName, addToMeta);

            fakeSampleStamps_.push_back(fakeClock);
            //std::cout << "fakeSampleStamps_["<<fakeSampleOffset_<<"]: " << fakeSampleStamps_[fakeSampleOffset_] <<  " timestamp: " << fakeClock << std::endl;
            fakeSampleOffset_++;
            fakeClock += sampleTosampleDistance_ns;
        }
    }
    pushData();
}

void CircularBufferManagerBaseTestFixture::pushData()
{
    uint64_t newDataSize = fakeSampleOffset_ - fakeSamplesPushed_;
    float data[newDataSize];
    if(DEBUG_ENABLED)
      std::cout << "pushing " << newDataSize<< " fake samples index: ["<< fakeSamplesPushed_<<","<<  fakeSamplesPushed_ + newDataSize - 1 << "] - fake stamps [" << fakeSampleStamps_[fakeSamplesPushed_]<< "," << fakeSampleStamps_[fakeSamplesPushed_ + newDataSize - 1]<< "]" << std::endl;
    for(std::size_t i= 0;i<newDataSize;i++)
    {
        data[i]=float(fakeSamplesPushed_);
        fakeSamplesPushed_++;
    }

    //std::cout << "fakeSamplesPushed_: " << fakeSamplesPushed_ << std::endl;
    buffer_manager_->push(&data[0], newDataSize, meta_to_push_ );

    // insert all pushed data into pushed
    meta_pushed_.insert(meta_pushed_.end(), meta_to_push_.begin(), meta_to_push_.end());
    meta_to_push_.clear();
}

void CircularBufferManagerBaseTestFixture::pushSingleSampleWithMeta()
{
    uint64_t newDataSize = 1;
    float data = fakeSamplesPushed_;
    meta_to_push_.clear();
    CircularBufferManager::FakeMetaInfo meta(fakeClock,fakeSampleOffset_,0);
    meta_to_push_.push_back(meta);
    //std::cout << "pushSingleSampleWithStamp: " << fakeSamplesPushed_ << " to index: "  << buffer_manager_->getCircularBuffer()->getWriteIterIndex()<< std::endl;
    if(DEBUG_ENABLED)
      std::cout << "pushing single sample at fake-sample-index '" << fakeSamplesPushed_ << "' with meta-stamp '" << fakeClock << "'" << std::endl;
    buffer_manager_->push(&data, newDataSize, meta_to_push_ );
    fakeSampleStamps_.push_back(fakeClock);
    fakeSamplesPushed_++;
    fakeSampleOffset_++;
}

// This Sequence will simulate streaming samples and events for the time of one second
void CircularBufferManagerBaseTestFixture::injectFakeSequence1_digitizer_streaming(bool end_sequence_with_gap_start)
{
    if(DEBUG_ENABLED)
        std::cout << "--- starting to inject new sequence ---" << std::endl;
    int64_t fakeClockStart = fakeClock;
    uint64_t fakeSamplesPushedStart = fakeSamplesPushed_;
    waitWithPush(530);
    injectFakeEvent("CMD_SEQ_START#257");
    waitWithPush(100);
    injectFakeEvent("CMD_BP_START#256");
    waitWithPush(100);
    injectFakeEvent("CMD_BEAM_INJECTION#283");
    waitWithPush(10);
    injectFakeEvent("CMD_FG_START#513");
    waitWithPush(10);
    injectFakeEvent("CMD_BP_START#256");
    waitWithPush(10);
    injectFakeEvent("CMD_CUSTOM_DIAG_1#286");
    waitWithPush(2);
    injectFakeEvent("CMD_START_ENERGY_RAMP#285");
    waitWithPush(38);
    injectFakeEvent("CMD_CUSTOM_DIAG_2#287");
    waitWithPush(100);
    injectFakeEvent("CMD_BEAM_EXTRACTION#284");
    waitWithPush(50);
    if (end_sequence_with_gap_start)
    {
        injectFakeEvent("EVT_GAP_START#258");
    }
    else
    {
        injectFakeEvent("CMD_CUSTOM_DIAG_3#288");
    }
    waitWithPush(50);
    if(DEBUG_ENABLED)
      std::cout << "--- This sequence took "<< float(fakeClock - fakeClockStart) / 1000000000 << " seconds" << " - " << fakeSamplesPushed_ - fakeSamplesPushedStart << " new samples added ---" << std::endl;
}

// This Sequence will simulate triggered samples and events for the time of one second
void CircularBufferManagerBaseTestFixture::injectFakeSequence1_digitizer_triggered()
{
    if(DEBUG_ENABLED)
        std::cout << "--- starting to inject new sequence ---" << std::endl;

    int64_t fakeClockStart = fakeClock;
    uint64_t fakeSamplesPushedStart = fakeSamplesPushed_;
    wait(530);
    injectFakeEventWithTriggerData("CMD_SEQ_START#257");
    wait(100);
    injectFakeEvent("CMD_BP_START#256");
    wait(100);
    injectFakeEventWithTriggerData("CMD_BEAM_INJECTION#283");
    wait(10);
    injectFakeEventWithTriggerData("CMD_FG_START#513");
    wait(10);
    injectFakeEvent("CMD_BP_START#256");
    wait(10);
    injectFakeEventWithTriggerData("CMD_CUSTOM_DIAG_1#286");
    wait(10);
    injectFakeEventWithTriggerData("CMD_START_ENERGY_RAMP#285");
    wait(38);
    injectFakeEventWithTriggerData("CMD_CUSTOM_DIAG_2#287");
    wait(100);
    injectFakeEventWithTriggerData("CMD_CUSTOM_DIAG_3#288");
    wait(50);
    injectFakeEventWithTriggerData("CMD_BEAM_EXTRACTION#284");
    wait(50);
    if(DEBUG_ENABLED)
      std::cout << "--- This sequence took "<< float(fakeClock - fakeClockStart) / 1000000000 << " seconds" << " - " << fakeSamplesPushed_ - fakeSamplesPushedStart << " new samples added ---" << std::endl;
}

// This Sequence will simulate a MIL power supply which is streaming samples with 'random' WR-timestamps and few events for the time of one second
void CircularBufferManagerBaseTestFixture::injectFakeSequence1_powersupply_streaming(bool end_sequence_with_gap_start)
{
    if(DEBUG_ENABLED)
        std::cout << "--- starting to inject new sequence ---" << std::endl;
    int64_t fakeClockStart = fakeClock;
    uint64_t fakeSamplesPushedStart = fakeSamplesPushed_;
    pushSingleSampleWithMeta();
    wait(530);
    injectFakeEvent("CMD_SEQ_START#257");
    wait(1);
    pushSingleSampleWithMeta(); // TODO: In reality, there are bursts of measurements (with fixed samp rate) .. to be simulated in a more realistic way !
    wait(100);
    injectFakeEvent("CMD_BP_START#256");
    wait(5);
    pushSingleSampleWithMeta();
    wait(100);
    injectFakeEvent("CMD_BEAM_INJECTION#283");
    wait(2);
    pushSingleSampleWithMeta();
    wait(10);
    injectFakeEvent("CMD_FG_START#513");
    wait(10);
    pushSingleSampleWithMeta();
    wait(10);
    injectFakeEvent("CMD_BP_START#256");
    wait(10);
    pushSingleSampleWithMeta();
    wait(10);
    injectFakeEvent("CMD_CUSTOM_DIAG_1#286");
    wait(10);
    pushSingleSampleWithMeta();
    wait(10);
    injectFakeEvent("CMD_START_ENERGY_RAMP#285");
    wait(10);
    pushSingleSampleWithMeta();
    wait(10);
    injectFakeEvent("CMD_CUSTOM_DIAG_2#287");
    wait(100);
    pushSingleSampleWithMeta();//multiple push without event in between
    wait(50);
    injectFakeEvent("CMD_BEAM_EXTRACTION#284");
    pushSingleSampleWithMeta();
    wait(10);
    pushSingleSampleWithMeta();
    if (end_sequence_with_gap_start)
    {
        injectFakeEvent("EVT_GAP_START#258");
    }
    else
    {
        injectFakeEvent("CMD_CUSTOM_DIAG_3#288");
    }
    wait(10);
    pushSingleSampleWithMeta();
    wait(2);

    if(DEBUG_ENABLED)
      std::cout << "--- This sequence took "<< float(fakeClock - fakeClockStart) / 1000000000 << " seconds" << " - " << fakeSamplesPushed_ - fakeSamplesPushedStart << " new samples added ---" << std::endl;
}

void CircularBufferManagerBaseTestFixture::injectFakeSequence1_streaming(DataPushMode dataPushMode, bool end_sequence_with_gap_start)
{
    if(dataPushMode == DataPushMode::DISJUNCT_SINGLE_SAMPLES)
        injectFakeSequence1_powersupply_streaming(end_sequence_with_gap_start);
    else
        injectFakeSequence1_digitizer_streaming(end_sequence_with_gap_start);
}

TEST_F(CircularBufferManagerBaseTestFixture, push_single_values_no_meta)
{
    createCircularBufferManager(float(10), /* samp rate    */
    		                    1000,      /* buffer size  */
								100,       /* meta size    */
								200,       /* tracker size */
								ClientNotificationType::STREAMING,
								DataPushMode::DISJUNCT_SINGLE_SAMPLES);
    std::size_t data_size = 1;
    float data[data_size];
    for(std::size_t i= 0;i<data_size;i++)
        data[i]=float(i);
    std::vector<CircularBufferManager::FakeMetaInfo>    meta_to_push;
    buffer_manager_->push(&data[0], data_size, meta_to_push );
    buffer_manager_->push(&data[0], data_size, meta_to_push );
    buffer_manager_->push(&data[0], data_size, meta_to_push );
}

TEST_F(CircularBufferManagerBaseTestFixture, digitizer_push_multi_values_no_meta)
{
    createCircularBufferManager(float(10), /* samp rate    */
    		                    1000,      /* buffer size  */
			                    100,       /* meta size    */
			                    200,       /* tracker size */
								ClientNotificationType::STREAMING,
								DataPushMode::CONTINOUS_MULTI_SAMPLES);
    std::size_t data_size = 10;
    float data[data_size];
    for(std::size_t i= 0;i<data_size;i++)
        data[i]=float(i);
    std::vector<CircularBufferManager::FakeMetaInfo>    meta_to_push;
    buffer_manager_->push(&data[0], data_size, meta_to_push );
    buffer_manager_->push(&data[0], data_size, meta_to_push );
    buffer_manager_->push(&data[0], data_size, meta_to_push );
}

void CircularBufferManagerBaseTestFixture::checkSequenceStreaming(MaxClientUpdateFrequencyType update_frequency_hz, DataPushMode dataPushMode, bool check_ref_meta)
{
    std::vector<float> timeSinceRefMeta;
    std::shared_ptr<const SampleMetadata<float>> ref_meta;

    injectFakeSequence1_streaming(dataPushMode);

    ASSERT_TRUE (data_ready_flagged_);
    auto data = retrieveData();

    std::vector<ClientNotificationData<float>> dataWithCorrectFreq;

    for (auto& d : data)
    {
        if (d.updateFrequency_ == update_frequency_hz)
        {
            dataWithCorrectFreq.push_back(d);
        }
    }
    ASSERT_FALSE(dataWithCorrectFreq.empty());
    for (auto& data : dataWithCorrectFreq)
    {
        CircularWindowIterator<float>& window_iterator = data.window_;
        if(DEBUG_ENABLED)
            std::cout << window_iterator;

        std::size_t result_size = 100000, n_data_written = 0;
        float result1[result_size];
        buffer_manager_->copyDataWindow(&window_iterator, &result1[0], result_size, n_data_written);

        ASSERT_TRUE (n_data_written > 0);
        ASSERT_TRUE (window_iterator.windowSize() > 0);

        ASSERT_EQ( (unsigned int)0, window_iterator.getWindowStatus());
        ref_meta = window_iterator.getRefMeta();
        ASSERT_FALSE(!ref_meta);

        auto fakeRefMeta = fakeRefMetaCol_.find(ref_meta->getTimingContext()->getTimeStamp());
        ASSERT_TRUE(fakeRefMeta != fakeRefMetaCol_.end())<< "RefMeta not found in fakeRefMetas";
        ASSERT_STREQ(fakeRefMeta->second.c_str(), device_data_buffer_->eventID2eventName(ref_meta->getTimingContext()->getEventNumber()).c_str());
        int64_t fakeRefMetaStamp = fakeRefMeta->first;

        timeSinceRefMeta = window_iterator.getTimeSinceRefMetaForEachSample();
        if (check_ref_meta)
        {
            ASSERT_EQ (window_iterator.timeSinceRefMetaSanityCheck(timeSinceRefMeta, 0, true),0);
            ASSERT_TRUE (timeSinceRefMeta.size() > 0);

            for (std::size_t i=0,j = fakeSamplesChecked_;i< timeSinceRefMeta.size();i++,j++ )
            {
                if(DEBUG_ENABLED)
                {
                    std::cout << "fakeSampleStamps_[" << j <<"]        : " << fakeSampleStamps_[j] << std::endl;
                    std::cout << "fakeRefMetaStamp         : " << fakeRefMetaStamp << std::endl;
                    std::cout << "measured timeSinceRefMeta["<<i<<"]      : " << timeSinceRefMeta[i] << std::endl;
                    std::cout << "expected timeSinceRefMeta["<<j<<"]      : " << (fakeSampleStamps_[j] - fakeRefMetaStamp)/1000000000. << std::endl;
                }
                if (sampRate_ >= samp_rate_100Hz)
                {
                    // For sample rates which are <100Hz we use the fakeClock for timestamping directly,
                    // otherwise multiple timing events would match the trigger event. This means that
                    // this timestamping calculation is not valid for those cases and we skip it.
                    ASSERT_NEAR((fakeSampleStamps_[j] - fakeRefMetaStamp)/1000000000., timeSinceRefMeta[i], FLOAT_COMPARISON_TOLERANCE ) << "Wrong timeSinceRefMeta at index: " << i;
                }
                ASSERT_EQ(result1[i], j);
            }
        }

        fakeSamplesChecked_ += timeSinceRefMeta.size();
    }
}

void CircularBufferManagerBaseTestFixture::checkSequenceStreaming_noTiming(MaxClientUpdateFrequencyType update_frequency_hz, bool expect_ref_meta, bool timeSinceRefMetaSanityCheck)
{
    std::vector<float> timeSinceRefMeta;
    std::shared_ptr<const SampleMetadata<float>> ref_meta;

    waitWithPush(1000);

    ASSERT_TRUE (data_ready_flagged_);
    auto data = retrieveData();

    std::vector<ClientNotificationData<float>> dataWithCorrectFreq;
    for (auto& d : data)
    {
        // We received data for different update rates, we are only interested in the provided update_frequency_hz.
        if (d.updateFrequency_ == update_frequency_hz)
        {
            dataWithCorrectFreq.push_back(d);
        }
    }
    ASSERT_FALSE(dataWithCorrectFreq.empty());
    for (auto& data : dataWithCorrectFreq)
    {
        CircularWindowIterator<float>& window_iterator = data.window_;

        std::size_t result_size = 100000, n_data_written = 0;
        float result1[result_size];
        buffer_manager_->copyDataWindow(&window_iterator, &result1[0], result_size, n_data_written);
        ref_meta = window_iterator.getRefMeta();

        ASSERT_TRUE (n_data_written > 0);
        if(DEBUG_ENABLED)
            std::cout << "Pushed " << n_data_written << " new samples to buffer" << std::endl;
        if(DEBUG_ENABLED)
            std::cout << window_iterator;
        ASSERT_TRUE (window_iterator.windowSize() > 0);
        ASSERT_EQ(window_iterator.getWindowStatus(), (unsigned int)0);
        if(expect_ref_meta)
        {
            ASSERT_FALSE(!ref_meta);

            auto fakeRefMeta = fakeRefMetaCol_.find(ref_meta->getTimingContext()->getTimeStamp());
            ASSERT_TRUE(fakeRefMeta != fakeRefMetaCol_.end())<< "RefMeta not found in fakeRefMetas";
            ASSERT_STREQ(fakeRefMeta->second.c_str(), device_data_buffer_->eventID2eventName(ref_meta->getTimingContext()->getEventNumber()).c_str());
            int64_t fakeRefMetaStamp = fakeRefMeta->first;

            timeSinceRefMeta = window_iterator.getTimeSinceRefMetaForEachSample();
            if(timeSinceRefMetaSanityCheck)
            {
                ASSERT_EQ (window_iterator.timeSinceRefMetaSanityCheck(timeSinceRefMeta, 0, true),0);
            }

            ASSERT_TRUE (timeSinceRefMeta.size() > 0);

            if(DEBUG_ENABLED)
                std::cout << window_iterator;
            for (std::size_t i=0,j = fakeSamplesChecked_;i< timeSinceRefMeta.size();i++,j++ )
            {
                if(DEBUG_ENABLED)
                {
                    std::cout << "fakeSampleStamps_[j]        : " << fakeSampleStamps_[j] << std::endl;
                    std::cout << "fakeRefMetaStamp         : " << fakeRefMetaStamp << std::endl;
                    std::cout << "measured timeSinceRefMeta["<<i<<"]      : " << timeSinceRefMeta[i] << std::endl;
                    std::cout << "expected timeSinceRefMeta["<<j<<"]      : " << (fakeSampleStamps_[j] - fakeRefMetaStamp)/1000000000. << std::endl;
                }
                // TODO: ASSERT_NEAR((fakeSampleStamps_[j] - fakeRefMetaStamp)/1000000000., timeSinceRefMeta[i], FLOAT_COMPARISON_TOLERANCE ) << "Wrong timeSinceRefMeta at index: " << i;
                ASSERT_EQ(result1[i], j);
            }
        }
        else
        {
            //std::cout << "ref_meta->getTimingContext()->getTimeStamp()        : " << ref_meta->getTimingContext() ==  << std::endl;
            ASSERT_TRUE(!ref_meta);
            for (std::size_t i=0,j = fakeSamplesChecked_;i< timeSinceRefMeta.size();i++,j++ )
            {
                ASSERT_EQ(fakeSampleStamps_[j], 0);
                ASSERT_EQ(result1[i], j);
            }
        }
        fakeSamplesChecked_ += timeSinceRefMeta.size();
    }
}

void CircularBufferManagerBaseTestFixture::validateExpectedSamples(CircularWindowIterator<float>& window_iterator, std::size_t& expectedSamples, std::size_t n_data_written)
{
    auto findEvent = [&](unsigned int event, const std::vector<std::shared_ptr<SampleMetadata<float>>>& metaCol)
        {
            auto it = std::find_if(metaCol.begin(), metaCol.end(),
                [&event](const std::shared_ptr<SampleMetadata<float>>& meta)
                {
                    return meta->getTimingContext()->getEventNumber() == event;
                });

            if (it == metaCol.end())
            {
                throw std::runtime_error("Did not find event: " + std::to_string(event));
            }
            return *it;
        };

        auto& metaCollection = window_iterator.getMetaDataCol();
        auto startMeta = findEvent(257, metaCollection);
        auto endMeta = findEvent(258, metaCollection);

        ASSERT_TRUE(startMeta->getTimingContextStamp() < endMeta->getTimingContextStamp());

        auto sequenceLength = endMeta->getTimingContextStamp() - startMeta->getTimingContextStamp();
        expectedSamples = static_cast<std::size_t>(std::round(sampRate_ * sequenceLength / 1e9));
        // expectedSamples does not yet contain the last sample belonging to sequence end event -> +1
        expectedSamples++;
        ASSERT_EQ(expectedSamples, n_data_written);
        ASSERT_EQ(expectedSamples, window_iterator.windowSize());
}

void CircularBufferManagerBaseTestFixture::checkSequenceFullSeq(DataPushMode dataPushMode)
{
    std::vector<float> timeSinceRefMeta;
    std::shared_ptr<const SampleMetadata<float>> ref_meta;

    // Clear data received until now. We only want the latest sequence which we will get after the call to injectSequence.
    retrieveData();

    uint64_t fakeSamplesPushedBefore = fakeSamplesPushed_;
    injectFakeSequence1_streaming(dataPushMode, true);

    ASSERT_TRUE (data_ready_flagged_);
    auto data = retrieveData();

    ASSERT_EQ(1u, data.size());
    ASSERT_EQ(ClientNotificationType::FULL_SEQUENCE, data.back().notificationType_);
    CircularWindowIterator<float>& window_iterator = data.back().window_;

    std::size_t result_size = 100000, n_data_written = 0;
    float result1[result_size];
    buffer_manager_->copyDataWindow(&window_iterator, &result1[0], result_size, n_data_written);
    if(DEBUG_ENABLED)
        std::cout << "** FULL_SEQ Window Size: " << result_size << std::endl;

    std::size_t expectedSamples = 0;
    if (dataPushMode == DataPushMode::DISJUNCT_SINGLE_SAMPLES)
    {
        expectedSamples = fakeSamplesPushed_ - fakeSamplesPushedBefore;
        ASSERT_GT(expectedSamples, window_iterator.windowSize());
    }
    else
    {
        validateExpectedSamples(window_iterator, expectedSamples, n_data_written);
    }

    ASSERT_EQ((unsigned int)0, window_iterator.getWindowStatus());
    ref_meta = window_iterator.getRefMeta();
    ASSERT_FALSE(!ref_meta);

    // Get the ref-trigger which occured last
    auto lastFakeRefMeta = fakeRefMetaCol_.end();
    lastFakeRefMeta--; // now we have the last list element

    if (sampRate_ == samp_rate_10Hz && dataPushMode != DataPushMode::DISJUNCT_SINGLE_SAMPLES)
    {
        // For 10Hz sample rate we don't get the last full seq notification.
        // So we need to check with the refMeta which is one older than the most recent.
        lastFakeRefMeta--;
    }
    
    ASSERT_STREQ(lastFakeRefMeta->second.c_str(), device_data_buffer_->eventID2eventName(ref_meta->getTimingContext()->getEventNumber()).c_str());
    ASSERT_EQ(lastFakeRefMeta->first, ref_meta->getTimingContext()->getTimeStamp())<< "TestError: RefMeta Skipped";

    timeSinceRefMeta = window_iterator.getTimeSinceRefMetaForEachSample();
    if(DEBUG_ENABLED)
    	std::cout << window_iterator;
    ASSERT_EQ (window_iterator.timeSinceRefMetaSanityCheck(timeSinceRefMeta, 0, true),0);

    if (dataPushMode == DataPushMode::DISJUNCT_SINGLE_SAMPLES)
    {
        ASSERT_GT(expectedSamples, timeSinceRefMeta.size());
    }
    else
    {
        ASSERT_EQ(expectedSamples, timeSinceRefMeta.size());
    }

    auto& meta_data = window_iterator.getMetaDataCol();
    auto& meta = meta_data.front();
    ASSERT_TRUE(DeviceDataBufferBase::isSequenceEndEvent(meta->getTimingContext()->getEventNumber()));
}

void CircularBufferManagerBaseTestFixture::checkSequenceTriggered()
{
    int64_t fakeSamplesChecked = fakeSamplesPushed_;
    std::vector<float> timeSinceRefMeta;
    CircularWindowIterator<float>* window_iterator;
    std::shared_ptr<const SampleMetadata<float>> ref_meta;

    injectFakeSequence1_digitizer_triggered();

    ASSERT_TRUE (data_ready_flagged_);
    auto data = retrieveData();

    ASSERT_EQ(data.size(), meta_pushed_.size() - nTriggersChecked_);

    auto nTriggersBegin = nTriggersChecked_;

    for( ;nTriggersChecked_ !=  meta_pushed_.size(); nTriggersChecked_++)
    {
        auto dataIndex = nTriggersChecked_ - nTriggersBegin;
        ASSERT_EQ(ClientNotificationType::TRIGGERED, data[dataIndex].notificationType_);

        CircularBufferManager::FakeMetaInfo meta = meta_pushed_[nTriggersChecked_];
        window_iterator = &data[dataIndex].window_;
        ASSERT_EQ(data[dataIndex].context_->getEventNumber(),
            device_data_buffer_->eventName2eventID(device_data_buffer_->extractEventName(meta.fesaConcreteEventName)));

        std::size_t result_size = 100000, n_data_written = 0;
        float result1[result_size];
        buffer_manager_->copyDataWindow(window_iterator, &result1[0], result_size, n_data_written);

        ASSERT_EQ(n_data_written, preTriggerSamples_ + postTriggerSamples_);
        ASSERT_EQ(window_iterator->windowSize(), preTriggerSamples_ + postTriggerSamples_);
        ASSERT_EQ(window_iterator->getWindowStatus(), (unsigned int)0);
        ref_meta = window_iterator->getRefMeta();
        ASSERT_FALSE(!ref_meta);

        // Triggerers should be alligned to the correct trigger position (First post trigger sample)
        ASSERT_STREQ( device_data_buffer_->extractEventName(meta.fesaConcreteEventName).c_str(), device_data_buffer_->eventID2eventName(ref_meta->getTimingContext()->getEventNumber()).c_str()) << " nTriggersChecked_: " << nTriggersChecked_;
        ASSERT_EQ( meta.context->getTimeStamp(), ref_meta->getTimingContext()->getTimeStamp())<< "TestError: Wrong WR stamp at nTriggersChecked_: " << nTriggersChecked_;
        ASSERT_EQ( meta.context->getTimeStamp(), ref_meta->getRelatedSampleStamp())<< "TestError: Wrong relatedSample stamp at nTriggersChecked_: " << nTriggersChecked_;

        timeSinceRefMeta = window_iterator->getTimeSinceRefMetaForEachSample();
        ASSERT_EQ (window_iterator->timeSinceRefMetaSanityCheck(timeSinceRefMeta, 0, true),0);
        ASSERT_EQ(timeSinceRefMeta.size(), n_data_written);

        ASSERT_EQ(meta.payload, ref_meta->getPayload());
        ASSERT_EQ(*meta.payload, *std::dynamic_pointer_cast<TestPayload>(ref_meta->getPayload()));
        ASSERT_TRUE(meta.payload != nullptr);
        // The x in payload ships the sum of event number and absolute offset of the sample.
        ASSERT_EQ(std::dynamic_pointer_cast<TestPayload>(ref_meta->getPayload())->x, ref_meta->getTimingContext()->getEventNumber() + ref_meta->getRelatedSampleAbsoluteOffset());

        if(DEBUG_ENABLED)
        	std::cout << *window_iterator;
        for (std::size_t i=0,j = fakeSamplesChecked;i< timeSinceRefMeta.size();i++,j++ )
        {
            if(DEBUG_ENABLED)
            {
                std::cout << "fakeSampleStamps_[j] : " << fakeSampleStamps_[j] << std::endl;
                std::cout << "meta.trigger_stamp : " << meta.trigger_stamp << std::endl;
                std::cout << "measured timeSinceRefMeta["<<i<<"]      : " << timeSinceRefMeta[i] << std::endl;
                std::cout << "expected timeSinceRefMeta["<<j<<"]      : " << (fakeSampleStamps_[j] - meta.trigger_stamp)/1000000000. << std::endl;
            }
            ASSERT_NEAR((fakeSampleStamps_[j] - meta.trigger_stamp)/1000000000., timeSinceRefMeta[i], FLOAT_COMPARISON_TOLERANCE ) << "Wrong timeSinceRefMeta at index: " << i;
            ASSERT_EQ(result1[i], j);
        }
        fakeSamplesChecked += n_data_written;
    }
}

void CircularBufferManagerBaseTestFixture::checkSequenceTriggeredTime()
{
    injectFakeSequence1_digitizer_triggered();

    // Push some samples to make sure that after the last event we will get the last triggered window.
    for (size_t i = 0; i < 2; i++)
    {
        pushSingleSampleWithMeta();
        wait(1);
    }

    ASSERT_TRUE (data_ready_flagged_);
    auto data = retrieveData();

    ASSERT_EQ(data.size(), triggerTimePushedMeta_.size());

    nTriggersChecked_ = 0;
    
    for( ;nTriggersChecked_ !=  triggerTimePushedMeta_.size(); nTriggersChecked_++ )
    {
        auto dataIndex = nTriggersChecked_;
        ASSERT_EQ(ClientNotificationType::TRIGGERED, data[dataIndex].notificationType_);

        CircularBufferManager::FakeMetaInfo meta = triggerTimePushedMeta_[nTriggersChecked_];
        auto window_iterator = data[dataIndex].window_;
        ASSERT_EQ(data[dataIndex].context_->getEventNumber(),
            device_data_buffer_->eventName2eventID(device_data_buffer_->extractEventName(meta.fesaConcreteEventName)));

        std::size_t result_size = 100000, n_data_written = 0;
        float result1[result_size];
        buffer_manager_->copyDataWindow(&window_iterator, &result1[0], result_size, n_data_written);

        ASSERT_EQ(window_iterator.windowSize(), n_data_written);
        ASSERT_EQ(window_iterator.getWindowStatus(), (unsigned int)0);
        auto ref_meta = window_iterator.getRefMeta();
        ASSERT_FALSE(!ref_meta);

        // Triggerers should be alligned to the correct trigger position (First post trigger sample)
        ASSERT_STREQ( device_data_buffer_->extractEventName(meta.fesaConcreteEventName).c_str(), device_data_buffer_->eventID2eventName(ref_meta->getTimingContext()->getEventNumber()).c_str()) << " nTriggersChecked_: " << nTriggersChecked_;
        ASSERT_EQ( meta.context->getTimeStamp(), ref_meta->getTimingContext()->getTimeStamp())<< "TestError: Wrong WR stamp at nTriggersChecked_: " << nTriggersChecked_;
        ASSERT_EQ( meta.context->getTimeStamp(), ref_meta->getRelatedSampleStamp())<< "TestError: Wrong relatedSample stamp at nTriggersChecked_: " << nTriggersChecked_;

        auto timeSinceRefMeta = window_iterator.getTimeSinceRefMetaForEachSample();
        
        ASSERT_EQ (window_iterator.timeSinceRefMetaSanityCheck(timeSinceRefMeta, 0, true),0);
        ASSERT_EQ(timeSinceRefMeta.size(), n_data_written);

        auto firstSampleStamp = ref_meta->getRelatedSampleStamp() - static_cast<int64_t>(preTriggerTime_);
        auto lastSampleStamp = ref_meta->getRelatedSampleStamp() + static_cast<int64_t>(postTriggerTime_);

        auto& metaCol = window_iterator.getMetaDataCol();
        ASSERT_GE(metaCol.back()->getRelatedSampleStamp(), firstSampleStamp);
        ASSERT_LE(metaCol.front()->getRelatedSampleStamp(), lastSampleStamp);
        
        if(DEBUG_ENABLED)
        	std::cout << window_iterator;
        
        for (std::size_t i=0,j = triggerTimeSamplesPushedBeforeEachEvent_[nTriggersChecked_];i< timeSinceRefMeta.size();i++,j++ )
        {
            if(DEBUG_ENABLED)
            {
                std::cout << "fakeSampleStamps_[j] : " << fakeSampleStamps_[j] << std::endl;
                std::cout << "meta.trigger_stamp : " << meta.trigger_stamp << std::endl;
                std::cout << "measured timeSinceRefMeta["<<i<<"]      : " << timeSinceRefMeta[i] << std::endl;
                std::cout << "expected timeSinceRefMeta["<<j<<"]      : " << (fakeSampleStamps_[j] - meta.trigger_stamp)/1000000000. << std::endl;
            }
            ASSERT_NEAR((fakeSampleStamps_[j] - meta.trigger_stamp)/1000000000., timeSinceRefMeta[i], FLOAT_COMPARISON_TOLERANCE ) << "Wrong timeSinceRefMeta at index: " << i;
            ASSERT_EQ(result1[i], j);
        }
    }
    
    triggerTimePushedMeta_.clear();
    triggerTimeSamplesPushedBeforeEachEvent_.clear();
}

TEST_P(CircularBufferManagerContMultiStreaming, digitizer_streaming)
{
    float sample_rate = std::get<0>(GetParam());
    std::size_t  size_buffer  = sample_rate * 10; // 10 seconds --> 10 simulated sequences
    std::size_t  meta_size    = 100;
    std::size_t  tracker_size = 100;
    dataPushMode_ = DataPushMode::CONTINOUS_MULTI_SAMPLES;

    createCircularBufferManager(sample_rate, size_buffer,meta_size, tracker_size, ClientNotificationType::STREAMING, dataPushMode_);
    MaxClientUpdateFrequencyType update_frequency_hz = std::get<1>(GetParam());

    // The first sequence will not create a window yet (window only created for clock > 1sec) ... so we do not check it
    injectFakeSequence1_streaming(dataPushMode_);

    // another 12 sequences should generate a buffer-rollover
    for (uint32_t i = 0; i < 12; i ++)
        checkSequenceStreaming(update_frequency_hz, dataPushMode_);

    checkSequenceStreaming(update_frequency_hz, dataPushMode_);
    ASSERT_TRUE (buffer_manager_->n_data_pushed_total() > size_buffer); // make sure we did at least one roundtrip on the circular buffer
}

TEST_P(CircularBufferManagerContMultiStreamingTiming, digitizer_streaming_timing_stops)
{
    float sample_rate = std::get<0>(GetParam());
    std::size_t  size_buffer  = sample_rate * 10; // 10 seconds --> 10 simulated sequences
    std::size_t  meta_size    = 100;
    std::size_t  tracker_size = 100;
    dataPushMode_ = DataPushMode::CONTINOUS_MULTI_SAMPLES;

    createCircularBufferManager(sample_rate, size_buffer,meta_size, tracker_size, ClientNotificationType::STREAMING, dataPushMode_);
    MaxClientUpdateFrequencyType update_frequency_hz = std::get<1>(GetParam());

    injectFakeSequence1_streaming(dataPushMode_);

    // 22 sequences should generate a buffer-rollover (twice)... with timing
    for (uint32_t i = 0; i < 22; i ++)
        checkSequenceStreaming(update_frequency_hz, dataPushMode_);

    // Another 33 sequences without timing .. we expect the latest ref-trigger to be used
    // We disable the timeSinceRefMetaSanityCheck during the transition, since it is expected that both timings are not in sync.
    // All the time the previous ref trigger remains.
    for (uint32_t i = 0; i < 33; i ++)
        checkSequenceStreaming_noTiming(update_frequency_hz, true, false);

    // Inject one sequence with timing without validating the ref trigger to get a full valid timing sequence ready.
    checkSequenceStreaming(update_frequency_hz, dataPushMode_, false);

    // Resume timing
    for (uint32_t i = 0; i < 10; i ++)
        checkSequenceStreaming(update_frequency_hz, dataPushMode_);
}

TEST_P(CircularBufferManagerContMultiStreamingTiming, digitizer_streaming_no_timing_at_all)
{
    float        sample_rate  = std::get<0>(GetParam());
    std::size_t  size_buffer  = sample_rate * 10; //10 seconds --> 10 simulated sequences
    std::size_t  meta_size    = 100;
    std::size_t  tracker_size = 200;
    dataPushMode_ = DataPushMode::CONTINOUS_MULTI_SAMPLES;

    createCircularBufferManager(sample_rate, size_buffer,meta_size, tracker_size, ClientNotificationType::STREAMING, dataPushMode_);
    MaxClientUpdateFrequencyType update_frequency_hz = std::get<1>(GetParam());

    // The first sequence will not create a window yet (window only created for clock > 1sec) ... so we do not check it
    waitWithPush(1000);

    // Another 20 sequences without timing
    for (uint32_t seq_index = 0; seq_index < 12; seq_index ++)
        checkSequenceStreaming_noTiming(update_frequency_hz, false, true);
}

TEST_P(CircularBufferManagerContMultiFullSeq, digitizer_full_sequence)
{
    float        sample_rate  = GetParam();
    std::size_t  size_buffer  = sample_rate * 10; //10 seconds --> 10 simulated sequences
    std::size_t  meta_size    = 100;
    std::size_t  tracker_size = 200;
    dataPushMode_ = DataPushMode::CONTINOUS_MULTI_SAMPLES;

    createCircularBufferManager(sample_rate, size_buffer, meta_size, tracker_size, ClientNotificationType::FULL_SEQUENCE, dataPushMode_);

    injectFakeSequence1_streaming(dataPushMode_);
    injectFakeSequence1_streaming(dataPushMode_);

    checkSequenceFullSeq(dataPushMode_);
    checkSequenceFullSeq(dataPushMode_);
    checkSequenceFullSeq(dataPushMode_);
}

TEST_F(CircularBufferManagerBaseTestFixture, full_sequence_1Hz)
{
    float        sample_rate  = samp_rate_1Hz;
    std::size_t  size_buffer  = sample_rate * 10; //10 seconds --> 10 simulated sequences
    std::size_t  meta_size    = 100;
    std::size_t  tracker_size = 200;
    dataPushMode_ = DataPushMode::CONTINOUS_MULTI_SAMPLES;

    createCircularBufferManager(sample_rate, size_buffer, meta_size, tracker_size, ClientNotificationType::FULL_SEQUENCE, dataPushMode_);

    uint32_t numSequencesToCheck = 5;

    // Due to only one sample being pushed to the buffer per sequence (per second), we need 3 sequences to get the notification for the first notification. Then each new notification is produced every second sequence.
    // 1. sequence, the actual sample is pushed.
    // 2. sequence, the updateFullSequenceWindow function is called (caused by the push of the second sample), and the window is moved from UNINITIALIZED to UNDER_CONSTRUCTION
    // 3. sequence, the updateFullSequenceWindow is called again (caused by the push of the third sample), and the window is finished.
    // 4. sequence, the updateFullSequenceWindow is called again (caused by the push of the forth sample), and the window is moved from UNINITIALIZED to UNDER_CONSTRUCTION
    // 5. sequence, the updateFullSequenceWindow is called again (caused by the push of the fifth sample), and the window is finished.
    // For this reason we will inject 2 * numSequencesToCheck + 1 sequences.
    
    injectFakeSequence1_streaming(dataPushMode_, true);

    for (uint32_t i = 0; i < numSequencesToCheck; ++i)
    {
        injectFakeSequence1_streaming(dataPushMode_, true);
        injectFakeSequence1_streaming(dataPushMode_, true);
    
        std::vector<float> timeSinceRefMeta;
        std::shared_ptr<const SampleMetadata<float>> ref_meta;

        ASSERT_TRUE(data_ready_flagged_);
        auto data = retrieveData();

        ASSERT_EQ(1u, data.size());
        ASSERT_EQ(ClientNotificationType::FULL_SEQUENCE, data.back().notificationType_);
        CircularWindowIterator<float>& window_iterator = data.back().window_;

        std::size_t result_size = 100000, n_data_written = 0;
        float result1[result_size];
        buffer_manager_->copyDataWindow(&window_iterator, &result1[0], result_size, n_data_written);

        std::size_t expectedSamples = 0;
        validateExpectedSamples(window_iterator, expectedSamples, n_data_written);

        ASSERT_EQ((unsigned int)0, window_iterator.getWindowStatus());
        ref_meta = window_iterator.getRefMeta();
        ASSERT_FALSE(!ref_meta);

        // Get the ref-trigger which occured last
        auto lastFakeRefMeta = fakeRefMetaCol_.end();
        lastFakeRefMeta--; // now we have the last list element
        
        // Due to the fact that 2 sequences are required to produce the next notification, the
        // fakeRefMetaCol_ is growing faster and we need to go back one more refMeta each iterator we take.
        for (uint32_t j = 0; j < 2 + i; ++j)
        {
            lastFakeRefMeta--;
        }

        ASSERT_STREQ(lastFakeRefMeta->second.c_str(), device_data_buffer_->eventID2eventName(ref_meta->getTimingContext()->getEventNumber()).c_str());
        ASSERT_EQ(lastFakeRefMeta->first, ref_meta->getTimingContext()->getTimeStamp())<< "TestError: RefMeta Skipped";

        timeSinceRefMeta = window_iterator.getTimeSinceRefMetaForEachSample();
        ASSERT_EQ (window_iterator.timeSinceRefMetaSanityCheck(timeSinceRefMeta, 0, true),0);

        ASSERT_EQ(expectedSamples, timeSinceRefMeta.size());

        auto& metaCollection = window_iterator.getMetaDataCol();
        auto& meta = metaCollection.front();
        ASSERT_TRUE(DeviceDataBufferBase::isSequenceEndEvent(meta->getTimingContext()->getEventNumber()));
    }
}

TEST_F(CircularBufferManagerBaseTestFixture, full_sequence_1Hz_no_end_event)
{
    float        sample_rate  = samp_rate_1Hz;
    std::size_t  size_buffer  = sample_rate * 10; //10 seconds --> 10 simulated sequences
    std::size_t  meta_size    = 100;
    std::size_t  tracker_size = 200;
    dataPushMode_ = DataPushMode::CONTINOUS_MULTI_SAMPLES;

    createCircularBufferManager(sample_rate, size_buffer, meta_size, tracker_size, ClientNotificationType::FULL_SEQUENCE, dataPushMode_);

    uint32_t numSequencesToCheck = 5;

    // Due to only one sample being pushed to the buffer per sequence (per second), we need 3 sequences to get the notification for the first notification. Then each new notification is produced every sequence.
    // 1. sequence, the actual sample is pushed.
    // 2. sequence, the updateFullSequenceWindow function is called (caused by the push of the second sample), and the window is moved from UNINITIALIZED to UNDER_CONSTRUCTION
    // 3. sequence, the updateFullSequenceWindow is called again (caused by the push of the third sample), and the window is finished. The next window is UNDER_CONSTRUCTION (since the sequence was ended by the next sequence start event)
    // 4. sequence, the updateFullSequenceWindow is called again (caused by the push of the forth sample), and the window is finished. The next window is UNDER_CONSTRUCTION (since the sequence was ended by the next sequence start event)
    // For this reason we will inject numSequencesToCheck + 2 sequences.
    
    injectFakeSequence1_streaming(dataPushMode_, false);
    injectFakeSequence1_streaming(dataPushMode_, false);

    for (uint32_t i = 0; i < numSequencesToCheck; ++i)
    {
        injectFakeSequence1_streaming(dataPushMode_, false);
    
        std::vector<float> timeSinceRefMeta;
        std::shared_ptr<const SampleMetadata<float>> ref_meta;

        ASSERT_TRUE(data_ready_flagged_);
        auto data = retrieveData();

        ASSERT_EQ(1u, data.size());
        ASSERT_EQ(ClientNotificationType::FULL_SEQUENCE, data.back().notificationType_);
        CircularWindowIterator<float>& window_iterator = data.back().window_;

        std::size_t result_size = 100000, n_data_written = 0;
        float result1[result_size];
        buffer_manager_->copyDataWindow(&window_iterator, &result1[0], result_size, n_data_written);

        ASSERT_EQ((unsigned int)0, window_iterator.getWindowStatus());
        ref_meta = window_iterator.getRefMeta();
        ASSERT_FALSE(!ref_meta);

        // Get the ref-trigger which occured last
        auto lastFakeRefMeta = fakeRefMetaCol_.end();
        lastFakeRefMeta--; // now we have the last list element
        
        // Due to the fact that on each second sequence the next notification is produced, we need to go back 2 refMetas.
        for (uint32_t j = 0; j < 2; ++j)
        {
            lastFakeRefMeta--;
        }

        ASSERT_STREQ(lastFakeRefMeta->second.c_str(), device_data_buffer_->eventID2eventName(ref_meta->getTimingContext()->getEventNumber()).c_str());
        ASSERT_EQ(lastFakeRefMeta->first, ref_meta->getTimingContext()->getTimeStamp())<< "TestError: RefMeta Skipped";

        timeSinceRefMeta = window_iterator.getTimeSinceRefMetaForEachSample();
        ASSERT_EQ (window_iterator.timeSinceRefMetaSanityCheck(timeSinceRefMeta, 0, true),0);

        std::size_t expectedSamples = 1;
        ASSERT_EQ(expectedSamples, timeSinceRefMeta.size());
    }
}

TEST_P(CircularBufferManagerContMultiFullSeq, full_sequence_with_overwrite)
{
    float        sample_rate  = GetParam();
    std::size_t  size_buffer  = sample_rate * 5.4;
    std::size_t  meta_size    = 100;
    std::size_t  tracker_size = 200;
    dataPushMode_ = DataPushMode::CONTINOUS_MULTI_SAMPLES;

    createCircularBufferManager(sample_rate, size_buffer, meta_size, tracker_size, ClientNotificationType::FULL_SEQUENCE, dataPushMode_);

    // Inject numSequences sequences. With the second injection we should get the first window notification.
    // In total we should get numSequences notifications (since each sequence is ended with gap event).
    // The last maxSequencesInBuffer of which should still be valid.

    std::size_t numSequences = 15;
    std::size_t maxSequencesInBuffer = size_buffer / static_cast<std::size_t>(sample_rate);
    ASSERT_GT(numSequences, maxSequencesInBuffer);
    for (std::size_t i = 0; i < numSequences; i++)
    {
        injectFakeSequence1_streaming(dataPushMode_);
    }
    
    ASSERT_TRUE(data_ready_flagged_);
    auto data = retrieveData();

    if (sample_rate == samp_rate_10Hz)
    {
        // For 10Hz sample rate we don't get the last full sequence notification due to how the unit tests are written.
        numSequences--;
        maxSequencesInBuffer--;
    }

    ASSERT_EQ(numSequences, data.size());

    std::size_t expectedValidDataIndex = data.size() - maxSequencesInBuffer;

    for (std::size_t i = 0; i < numSequences; ++i)
    {
        ASSERT_EQ(ClientNotificationType::FULL_SEQUENCE, data[i].notificationType_);
        CircularWindowIterator<float>& window_iterator = data[i].window_;

        std::size_t result_size = window_iterator.windowSize(), n_data_written = 0;
        float result1[result_size];
        if (i >= expectedValidDataIndex)
        {
            ASSERT_NO_THROW(buffer_manager_->copyDataWindow(&window_iterator, &result1[0], result_size, n_data_written));
        }
        else
        {
            ASSERT_THROW(
                try
                {
                    buffer_manager_->copyDataWindow(&window_iterator, &result1[0], result_size, n_data_written);
                }
                catch( const std::runtime_error& e )
                {
                    std::string expected = "Readout data was overwritten. Data is invalid.";
                    std::string error = e.what();
                    ASSERT_TRUE(error.find(expected) != std::string::npos);
                    throw;
                }, std::runtime_error);
        }
    }
}

TEST_P(CircularBufferManagerTriggered, triggered_disjunct_multi)
{
    float        sample_rate  = GetParam();
    std::size_t  size_buffer  = 1000; //~9 triggers
    std::size_t  meta_size    = 100;
    std::size_t  tracker_size = 200;
    dataPushMode_ = DataPushMode::DISJUNCT_MULTI_SAMPLES;
    preTriggerSamples_        = 10;
    postTriggerSamples_       = 100;

    createCircularBufferManager(sample_rate, size_buffer,meta_size, tracker_size, ClientNotificationType::TRIGGERED, dataPushMode_);
    buffer_manager_->setTriggerSamples(preTriggerSamples_, postTriggerSamples_);

    checkSequenceTriggered();
    checkSequenceTriggered();
    ASSERT_GT(buffer_manager_->n_data_pushed_total(), size_buffer); // make sure we did at least one roundtrip on the circular buffer
}

TEST_P(CircularBufferManagerTriggered, triggered_disjunct_single)
{
    float        sample_rate  = GetParam();
    std::size_t  size_buffer  = sample_rate / 10; //~9 triggers
    std::size_t  meta_size    = 1000;
    std::size_t  tracker_size = 200;
    dataPushMode_ = DataPushMode::DISJUNCT_SINGLE_SAMPLES;

    // Trigger time in double nanoseconds.
    preTriggerTime_ = 5000000.0;
    postTriggerTime_ = 2000000.0;

    createCircularBufferManager(sample_rate, size_buffer,meta_size, tracker_size, ClientNotificationType::TRIGGERED, dataPushMode_);
    buffer_manager_->setTriggerTime(preTriggerTime_, postTriggerTime_);

    // Add some samples before the first SEQ_START so we will get the first triggered window.
    for (size_t i = 0; i < 2; i++)
    {
        pushSingleSampleWithMeta();
        wait(1);
    }
    checkSequenceTriggeredTime();
    checkSequenceTriggeredTime();
    ASSERT_GT(buffer_manager_->n_data_pushed_total(), size_buffer); // make sure we did at least one roundtrip on the circular buffer
}

TEST_P(CircularBufferManagerTriggered, triggered_continuous_multi)
{
    float        sample_rate  = GetParam();
    std::size_t  size_buffer  = 1000; //~9 triggers
    std::size_t  meta_size    = 100;
    std::size_t  tracker_size = 200;
    dataPushMode_ = DataPushMode::CONTINOUS_MULTI_SAMPLES;
    preTriggerSamples_        = 10;
    postTriggerSamples_       = 100;

    createCircularBufferManager(sample_rate, size_buffer,meta_size, tracker_size, ClientNotificationType::TRIGGERED, dataPushMode_);
    buffer_manager_->setTriggerSamples(preTriggerSamples_, postTriggerSamples_);

    checkSequenceTriggered();
    checkSequenceTriggered();
    ASSERT_TRUE (buffer_manager_->n_data_pushed_total() > size_buffer); // make sure we did at least one roundtrip on the circular buffer
}

TEST_P(CircularBufferManagerTriggered, digitizer_triggered_no_pre_samples)
{
    float        sample_rate  = GetParam();
    std::size_t  size_buffer  = 1000;
    std::size_t  meta_size    = 100;
    std::size_t  tracker_size = 200;
    dataPushMode_ = DataPushMode::DISJUNCT_MULTI_SAMPLES;
    preTriggerSamples_ = 0;
    postTriggerSamples_ = 100;

    createCircularBufferManager(sample_rate, size_buffer,meta_size, tracker_size, ClientNotificationType::TRIGGERED, dataPushMode_);
    buffer_manager_->setTriggerSamples(preTriggerSamples_, postTriggerSamples_);

    checkSequenceTriggered();
    checkSequenceTriggered();
    ASSERT_TRUE (buffer_manager_->n_data_pushed_total() > size_buffer); // make sure we did at least one roundtrip on the circular buffer
}

TEST_P(CircularBufferManagerDisjSingleStreaming, power_supply_MIL_Streaming)
{
    float        sample_rate  = std::get<0>(GetParam());
    std::size_t  size_buffer  = 100;
    std::size_t  meta_size    = 100;
    std::size_t  tracker_size = 200;
    dataPushMode_ = DataPushMode::DISJUNCT_SINGLE_SAMPLES;

    createCircularBufferManager(sample_rate, size_buffer, meta_size, tracker_size, ClientNotificationType::STREAMING, dataPushMode_);
    MaxClientUpdateFrequencyType update_frequency_hz = std::get<1>(GetParam());

    // The first sequence will not create a window yet (window only created for clock > 1sec) ... so we do not check it
    injectFakeSequence1_powersupply_streaming();

    for (uint32_t seq_index = 0; seq_index < 10; seq_index ++)
        checkSequenceStreaming(update_frequency_hz, dataPushMode_);
    ASSERT_TRUE (buffer_manager_->n_data_pushed_total() > size_buffer); // make sure we did at least one roundtrip on the circular buffer
}

TEST_P(CircularBufferManagerDisjSingleStreaming, power_supply_MIL_Streaming_no_timing_at_all)
{
    float        sample_rate  = std::get<0>(GetParam());
    std::size_t  size_buffer  = sample_rate * 10;
    std::size_t  meta_size    = 100;
    std::size_t  tracker_size = 200;
    dataPushMode_ = DataPushMode::DISJUNCT_SINGLE_SAMPLES;

    createCircularBufferManager(sample_rate, size_buffer,meta_size, tracker_size, ClientNotificationType::STREAMING, dataPushMode_);
    MaxClientUpdateFrequencyType update_frequency_hz = std::get<1>(GetParam());

    // The first sequence will not create a window yet (window only created for clock > 1sec) ... so we do not check it
    waitWithPush(1000);

    // TODO: Currently checkSequenceStreaming_noTiming uses 'waitWithPush' which makes use of sample_to_sample_distance --> No good testcase for MIL !
    // On the other hand, MIL Power Supplies usually do measure on timing events .. is it even possible to measure data when there is no timing played ?

    // Another 20 sequences without timing
    for (uint32_t seq_index = 0; seq_index < 12; seq_index ++)
        checkSequenceStreaming_noTiming(update_frequency_hz, false, true);
}

TEST_P(CircularBufferManagerDisjSingleFullSeq, power_supply_full_sequence)
{
   float        sample_rate  = GetParam();
   std::size_t  size_buffer  = 40;
   std::size_t  meta_size    = 100;
   std::size_t  tracker_size = 200;
   dataPushMode_ = DataPushMode::DISJUNCT_SINGLE_SAMPLES;

   createCircularBufferManager(sample_rate, size_buffer,meta_size, tracker_size, ClientNotificationType::FULL_SEQUENCE, dataPushMode_);

   // Run a couple of sequences first so that in total we will write at least one full buffer.
   injectFakeSequence1_streaming(dataPushMode_);
   injectFakeSequence1_streaming(dataPushMode_);
   injectFakeSequence1_streaming(dataPushMode_);

   checkSequenceFullSeq(dataPushMode_);
   checkSequenceFullSeq(dataPushMode_);
   checkSequenceFullSeq(dataPushMode_);

   ASSERT_TRUE (buffer_manager_->n_data_pushed_total() > size_buffer); // make sure we did at least one roundtrip on the circular buffer

   // Now lets just add two new meta items without adding new samples
   // That should not lead to a new sequence window (this scenario actually happens during operation, e.g. in the gap)
   wait(530);
   injectFakeEvent("CMD_SEQ_START#257", 1);
   wait(1);
   injectFakeEvent("CMD_SEQ_START#257", 2);
   ASSERT_FALSE(data_ready_flagged_);
}

INSTANTIATE_TEST_CASE_P(
    cont_multi_streaming_tests,
    CircularBufferManagerContMultiStreaming,
    ::testing::Values(
                std::make_tuple(samp_rate_1Hz, 1), // TODO: While the test passes. Make sure it actually works ok.
                std::make_tuple(samp_rate_10Hz, 1),
                std::make_tuple(samp_rate_100Hz, 1),
                std::make_tuple(samp_rate_1kHz, 1),
                std::make_tuple(samp_rate_10kHz, 1),
                std::make_tuple(samp_rate_1Hz, 10), // TODO: While the test passes. Make sure it actually works ok.
                // TODO: Fails currently. For update rates which are >= sample rate it must be
                // checked wether the tests are wrong or the code is not working ok.
                // std::make_tuple(samp_rate_10Hz, 10),
                std::make_tuple(samp_rate_100Hz, 10),
                std::make_tuple(samp_rate_1kHz, 10),
                std::make_tuple(samp_rate_10kHz, 10),
                std::make_tuple(samp_rate_1Hz, 25), // TODO: While the test passes. Make sure it actually works ok.
                // std::make_tuple(samp_rate_10Hz, 25),
                std::make_tuple(samp_rate_100Hz, 25),
                std::make_tuple(samp_rate_1kHz, 25),
                std::make_tuple(samp_rate_10kHz, 25)
                ));

INSTANTIATE_TEST_CASE_P(
    cont_multi_streaming_tests,
    CircularBufferManagerContMultiStreamingTiming,
    ::testing::Values(
                std::make_tuple(samp_rate_1Hz, 1),
                std::make_tuple(samp_rate_10Hz, 1),
                std::make_tuple(samp_rate_100Hz, 1),
                std::make_tuple(samp_rate_1kHz, 1),
                std::make_tuple(samp_rate_10kHz, 1),
                // TODO: Fails currently. At the point of checking the timeSinceRefMeta.
                std::make_tuple(samp_rate_1Hz, 10),
                // std::make_tuple(samp_rate_10Hz, 10),
                // std::make_tuple(samp_rate_100Hz, 10),
                // std::make_tuple(samp_rate_1kHz, 10),
                // std::make_tuple(samp_rate_10kHz, 10),
                std::make_tuple(samp_rate_1Hz, 25)
                // std::make_tuple(samp_rate_10Hz, 25),
                // std::make_tuple(samp_rate_100Hz, 25),
                // std::make_tuple(samp_rate_1kHz, 25),
                // std::make_tuple(samp_rate_10kHz, 25)
                ));

INSTANTIATE_TEST_CASE_P(
    full_sequence_tests,
    CircularBufferManagerContMultiFullSeq,
    ::testing::Values(
                // For 1Hz there are separate tests full_sequence_1Hz and full_sequence_1Hz_no_end_event.
                samp_rate_10Hz,
                samp_rate_100Hz,
                samp_rate_1kHz,
                samp_rate_10kHz
                ));

INSTANTIATE_TEST_CASE_P(
    triggered_tests,
    CircularBufferManagerTriggered,
    ::testing::Values(
                // TODO: Triggered tests were written with higher sampling frequences in mind and as such
                // pass for 1k and 10kHz. Most of the test for 100Hz pass if FLOAT_COMPARISON_TOLERANCE
                // is increased to 1.5e-7, while lower frequencies tests all fail.
                samp_rate_1kHz,
                samp_rate_10kHz
                ));

// These tests all pass, however the DISJUNCT_SINGLE_SAMPLES tests are written to produce 132
// samples per sequence/second regardless of the samp_rate since this mode doesn't have a strict "sample rate".
INSTANTIATE_TEST_CASE_P(
    disjunct_single_tests,
    CircularBufferManagerDisjSingleStreaming,
    ::testing::Values(
                std::make_tuple(samp_rate_1Hz, 1),
                std::make_tuple(samp_rate_10Hz, 1),
                std::make_tuple(samp_rate_100Hz, 1),
                std::make_tuple(samp_rate_1kHz, 1),
                std::make_tuple(samp_rate_10kHz, 1),
                std::make_tuple(samp_rate_1Hz, 10),
                std::make_tuple(samp_rate_10Hz, 10),
                std::make_tuple(samp_rate_100Hz, 10),
                std::make_tuple(samp_rate_1kHz, 10),
                std::make_tuple(samp_rate_10kHz, 10),
                std::make_tuple(samp_rate_1Hz, 25),
                std::make_tuple(samp_rate_10Hz, 25),
                std::make_tuple(samp_rate_100Hz, 25),
                std::make_tuple(samp_rate_1kHz, 25),
                std::make_tuple(samp_rate_10kHz, 25)
                ));

// These tests all pass, however the DISJUNCT_SINGLE_SAMPLES tests are written to produce 132
// samples per sequence/second regardless of the samp_rate since this mode doesn't have a strict "sample rate".
INSTANTIATE_TEST_CASE_P(
    disjunct_single_tests,
    CircularBufferManagerDisjSingleFullSeq,
    ::testing::Values(
                samp_rate_1Hz,
                samp_rate_10Hz,
                samp_rate_100Hz,
                samp_rate_1kHz,
                samp_rate_10kHz
                ));

// TODO: Test as well smaller methods instead of testing "EVERYTHING"

} // end namespace

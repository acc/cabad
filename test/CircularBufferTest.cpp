/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <test/CircularBufferTestFixture.h>

#include <cabad/CircularBuffer.h>

namespace cabad
{

std::size_t CircularBufferTestFixture::bufferSize_ = 10;
CircularBuffer<int> CircularBufferTestFixture::buffer_(bufferSize_);

TEST_F(CircularBufferTestFixture, Single_Iterator_BeginEnd)
{
    CircularBuffer<int> buffer(10);
    ASSERT_NO_THROW(
            ASSERT_EQ((long unsigned)10, buffer.end().index()); // needed for container.end() (points outside of container )
            ASSERT_EQ((long unsigned)0, buffer.begin().index());
    );
}

TEST_F(CircularBufferTestFixture, Single_Iterator_Pointers)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);

    for (std::size_t i= 0; i<circular_size;i++)
    {
        buffer.push(i);
    }

    CircularBuffer<int>::iterator iter = buffer.begin();
    for (int i= 0; i<(int)circular_size;i++)
    {
        ASSERT_EQ(i, *iter);
        ++iter;
    }
}

TEST_F(CircularBufferTestFixture, Single_Iterator_assigment)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);

    CircularBuffer<int>::iterator iter1 = buffer.begin();
    CircularBuffer<int>::iterator iter2 = iter1;

    ++iter1;
    ++iter2;

    buffer.push(1);
    buffer.push(4);
    
    ASSERT_EQ(4, *iter2);
    ++iter1;
    ASSERT_EQ(4, *iter2);
}

TEST_F(CircularBufferTestFixture, moveIterToLatest)
{
    int someValue = 23;
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    buffer.push(someValue);
    CircularBuffer<int>::iterator iter;
    buffer.moveIterToLatest(iter);

    ASSERT_EQ(someValue, *iter);
}

TEST_F(CircularBufferTestFixture, Single_push_scalar)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    auto iter1 = buffer.getReadIterator(0);
    auto iter2 = buffer.getReadIterator(1);
    auto iter3 = buffer.getReadIterator(9);
    for (int i= 0; i<30;i++)
        buffer.push(i);
    for (std::size_t i= 0; i<circular_size;i++)
        ASSERT_EQ(int(i+20), buffer[i]);

    buffer.moveIterToLatest(iter1);
    buffer.moveIterToLatest(iter2);
    buffer.moveIterToLatest(iter3);

    ++iter2;
    ++iter3;
    ++iter3;

    int someValue = 123;
    buffer.push(someValue);
}

TEST_F(CircularBufferTestFixture, Single_push_array)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);

    // Not possible to push arrays bigger than the buffer itself
    ASSERT_THROW(
            int data[circular_size + 1];
            buffer.push(data, circular_size + 1);
            ,std::exception);

    { // regular copy

        int data[5];
        for (int i= 0; i<5;i++)
            data[i] = i;
        buffer.push(data, 5);
        for (std::size_t i= 0; i<5;i++)
            ASSERT_EQ((int)i, buffer[i]);
    }

    { // buffer end reached. Will copy twice
        std::size_t circular_size = 12;
        CircularBuffer<int> buffer(circular_size);
        int data[8];
        for (int i= 0; i<8;i++)
            data[i] = i;
        buffer.push(data, 8);// 0-7 populated now
        buffer.push(data, 8);// 8-11 populated now as well, 0-3 overwritten, write iter now at 4
        for (std::size_t i= 0; i<4;i++) // check the overwritten elements
            ASSERT_EQ((int)i+4, buffer[i]);
    }
}

TEST_F(CircularBufferTestFixture, push_vector)
{
	std::size_t circular_size = 10;
	CircularBuffer<int> buffer(circular_size);

	std::vector<int> data;
	for (std::size_t i= 0; i<circular_size;i++)
		data.push_back(i);

	buffer.push(data);
	for (std::size_t i= 0; i<circular_size;i++)
		ASSERT_EQ((int)i, buffer[i]);

    // Anything else is already tested in the C-array test
}


TEST_F(CircularBufferTestFixture, copyDataWindow)
{
    int data[bufferSize_];
    for (std::size_t i= 0; i<bufferSize_;i++)
        data[i] = (int)i;
    CircularBuffer<int>::iterator start;
    CircularBuffer<int>::iterator end;
    buffer_.push(data, bufferSize_);

    { // window of single element. Write iterator is 0.
        std::size_t result_size = 1, expected_n_data_written = 0;
        int result[result_size];
        start = buffer_.getReadIterator(2);
        end = buffer_.getReadIterator(2);
        auto validationCheckpoint = buffer_.calculateValidationCheckpoint(start, end);
        buffer_.copyDataWindow(start, end, validationCheckpoint, &result[0], result_size, expected_n_data_written);
        ASSERT_EQ(result_size, expected_n_data_written);
        ASSERT_EQ(2, result[0]);
    }

    { // window does not cross buffer_.end(). Write iterator is 0.
        std::size_t result_size = 5, expected_n_data_written = 0;
        int read_offset = 1;
        int result[result_size];
        start = buffer_.getReadIterator(read_offset);
        end = buffer_.getReadIterator(read_offset + result_size - 1);  // -1 since end() is included in the data.
        auto validationCheckpoint = buffer_.calculateValidationCheckpoint(start, end);
		buffer_.copyDataWindow(start, end, validationCheckpoint, &result[0], result_size, expected_n_data_written);
        ASSERT_EQ(result_size, expected_n_data_written);
        for (std::size_t i = 0; i < result_size; i++)
            ASSERT_EQ((int)i + read_offset, result[i]);
    }

    { // window size == buffer size - 1. Write iterator is 0.
      // We are reading the maximum we can which is every sample but the one where the write iterator is.
        std::size_t result_size = bufferSize_ - 1, expected_n_data_written = 0;
        int result[result_size];
        int read_offset = 1;
        start = buffer_.getReadIterator(read_offset);
        end = buffer_.getReadIterator(read_offset + result_size - 1);  // -1 since end() is included in the data.
        auto validationCheckpoint = buffer_.calculateValidationCheckpoint(start, end);
        buffer_.copyDataWindow(start, end, validationCheckpoint, &result[0], result_size, expected_n_data_written);
        ASSERT_EQ(result_size, expected_n_data_written);
        for (std::size_t i= 0; i<result_size;i++)
            ASSERT_EQ((int)i + read_offset, result[i]);
    }

    { // window crosses buffer_.end().
        buffer_.push(data, 2);
        // Write iterator is now 2.
        std::size_t result_size = 7, expected_n_data_written = 0;
        int result[result_size];
        start = buffer_.getReadIterator(5);
        end = buffer_.getReadIterator(1);
        auto validationCheckpoint = buffer_.calculateValidationCheckpoint(start, end);
        buffer_.copyDataWindow(start, end, validationCheckpoint, &result[0], result_size, expected_n_data_written);
        ASSERT_EQ(result_size, expected_n_data_written);
        ASSERT_EQ(5, result[0]);
        ASSERT_EQ(6, result[1]);
        ASSERT_EQ(7, result[2]);
        ASSERT_EQ(8, result[3]);
        ASSERT_EQ(9, result[4]);
        ASSERT_EQ(0, result[5]);
        ASSERT_EQ(1, result[6]);
    }

    { // window crosses buffer_.end() + window size == buffer size - 1.
      // We are reading the maximum we can which is every sample but the one where the write iterator is.
        buffer_.push(&data[2], 2);
        // Write iterator is now 4.
        std::size_t result_size = bufferSize_ - 1, expected_n_data_written = 0;
        int result[result_size];
        start = buffer_.getReadIterator(5);
        end = buffer_.getReadIterator(3);
        auto validationCheckpoint = buffer_.calculateValidationCheckpoint(start, end);
        buffer_.copyDataWindow(start, end, validationCheckpoint, &result[0], result_size, expected_n_data_written);
        ASSERT_EQ(result_size, expected_n_data_written);
        ASSERT_EQ(5, result[0]);
        ASSERT_EQ(6, result[1]);
        ASSERT_EQ(7, result[2]);
        ASSERT_EQ(8, result[3]);
        ASSERT_EQ(9, result[4]);
        ASSERT_EQ(0, result[5]);
        ASSERT_EQ(1, result[6]);
        ASSERT_EQ(2, result[7]);
        ASSERT_EQ(3, result[8]);
    }

    { // reserved buffer to small. Write iterator is 4.
        std::size_t result_size = 1, expected_n_data_written = 0;
        int result[result_size];
        start = buffer_.getReadIterator(2);
        end = buffer_.getReadIterator(3);
        auto validationCheckpoint = buffer_.calculateValidationCheckpoint(start, end);
        ASSERT_THROW(
            buffer_.copyDataWindow(start, end, validationCheckpoint, &result[0], result_size, expected_n_data_written);
        ,std::exception);
        ASSERT_EQ((long unsigned)0, expected_n_data_written);
    }
}

TEST_F(CircularBufferTestFixture, dataValidation)
{
    // Use a bigger buffer.
    std::size_t bufferSize = 1555;
    CircularBuffer<int> buffer(bufferSize);
    
    std::size_t numPushed = 16719;
    for (std::size_t i = 0; i < numPushed; ++i)
    {
        int data = static_cast<int>(i);
        buffer.push(data);
    }

    ASSERT_EQ(buffer.n_data_pushed_total(), numPushed);

    auto writeIter = numPushed % bufferSize;
    ASSERT_EQ(buffer.getWriteIterIndex(), writeIter);

    // Valid data should be from writeIter + 1 until writeIter + bufferSize -1
    auto start = buffer.getReadIterator(writeIter + 1);
    auto end = buffer.getReadIterator(writeIter - 1);
    auto checkpoint = buffer.calculateValidationCheckpoint(start, end);

    ASSERT_EQ(numPushed + 1, checkpoint);

    std::vector<int> readoutBuffer(bufferSize - 1);
    std::size_t written;
    buffer.copyDataWindow(start, end, checkpoint, readoutBuffer.data(), readoutBuffer.size(), written);

    ASSERT_EQ(readoutBuffer.size(), written);

    int expectedLastSampleValue = numPushed - 1;
    ASSERT_EQ(expectedLastSampleValue, readoutBuffer.back());

    // First sample value should be last sample value - readoutBuffer.size() + 1
    int expectedValue = expectedLastSampleValue - readoutBuffer.size() + 1;
    for (auto readoutValue : readoutBuffer)
    {
        ASSERT_EQ(expectedValue, readoutValue);
        ++expectedValue;
    }

    // Pushing 1 data point invalidates our window.
    int data = static_cast<int>(numPushed);
    buffer.push(data);

    ASSERT_THROW(
        try
        {
            buffer.copyDataWindow(start, end, checkpoint, readoutBuffer.data(), readoutBuffer.size(), written);
        }
        catch( const std::runtime_error& e )
        {
            std::string expected = "Readout data was overwritten. Data is invalid.";
            std::string error = e.what();
            ASSERT_TRUE(error.find(expected) != std::string::npos);
            throw;
        }, std::runtime_error);
}

TEST_F(CircularBufferTestFixture, checkpointCalculation)
{
    // Use a bigger buffer.
    constexpr std::size_t bufferSize = 1555;
    CircularBuffer<int> buffer(bufferSize);
    
    constexpr std::size_t numPushed = 16719;
    for (std::size_t i = 0; i < numPushed; ++i)
    {
        int data = static_cast<int>(i);
        buffer.push(data);
    }

    ASSERT_EQ(buffer.n_data_pushed_total(), numPushed);

    auto writeIter = numPushed % bufferSize;
    ASSERT_EQ(buffer.getWriteIterIndex(), writeIter);

    auto tryRead = [&buffer](
            const CircularBuffer<int>::iterator& start,
            const CircularBuffer<int>::iterator& end,
            uint64_t checkpoint,
            bool expectThrow)
    {
        std::vector<int> readoutBuffer(bufferSize);
        std::size_t written;
        if (expectThrow)
        {
            ASSERT_THROW(
                try
                {
                    buffer.copyDataWindow(start, end, checkpoint, readoutBuffer.data(), readoutBuffer.size(), written);
                }
                catch( const std::runtime_error& e )
                {
                    std::string expected = "Readout data was overwritten. Data is invalid.";
                    std::string error = e.what();
                    ASSERT_TRUE(error.find(expected) != std::string::npos);
                    throw;
                }, std::runtime_error);
        }
        else
        {
            ASSERT_NO_THROW(buffer.copyDataWindow(start, end, checkpoint, readoutBuffer.data(), readoutBuffer.size(), written));
        }
    };

    {
        // When the window contains the write iterator, the checkpoint should be invalid right away.
        auto start = buffer.getReadIterator(writeIter);
        auto end = buffer.getReadIterator(writeIter + 10);
        auto checkpoint = buffer.calculateValidationCheckpoint(start, end);
        ASSERT_EQ(numPushed, checkpoint);
        tryRead(start, end, checkpoint, true);
    }
    {
        // When the window contains the write iterator, the checkpoint should be invalid right away.
        auto start = buffer.getReadIterator(writeIter - 5);
        auto end = buffer.getReadIterator(writeIter);
        auto checkpoint = buffer.calculateValidationCheckpoint(start, end);
        ASSERT_EQ(numPushed - 5, checkpoint);
        tryRead(start, end, checkpoint, true);
    }
    {
        // When the window contains only the write iterator, the checkpoint should be invalid right away.
        auto start = buffer.getReadIterator(writeIter);
        auto end = buffer.getReadIterator(writeIter);
        auto checkpoint = buffer.calculateValidationCheckpoint(start, end);
        ASSERT_EQ(numPushed, checkpoint);
        tryRead(start, end, checkpoint, true);
    }

    auto wrapCounter = numPushed / bufferSize;
    for (size_t i = 200; i < bufferSize; i += 200)
    {
        // When the window spans before the write iterator, the checkpoint should reflect start (in units of total pushed) + buffer size.
        auto start = buffer.getReadIterator(writeIter - i);
        auto end = buffer.getReadIterator(writeIter - i / 2);
        auto checkpoint = buffer.calculateValidationCheckpoint(start, end);

        if (start.index() < writeIter)
        {
            // In case the start is still in the current buffer wrap.
            auto startCheckpoint = start.index() + (wrapCounter * bufferSize) + bufferSize;
            ASSERT_EQ(startCheckpoint, checkpoint);
        }
        else
        {
            // In case the start is in the previous buffer wrap.
            auto startCheckpoint = start.index() + ((wrapCounter - 1) * bufferSize) + bufferSize;
            ASSERT_EQ(startCheckpoint, checkpoint);
        }
        tryRead(start, end, checkpoint, false);
    }

    // Test also the method without dataEnd.
    {
        // When the window start equals the write iterator, the checkpoint should be invalid right away.
        auto start = buffer.getReadIterator(writeIter);
        auto checkpoint = buffer.calculateValidationCheckpoint(start);
        ASSERT_EQ(numPushed, checkpoint);
        auto end = buffer.getReadIterator(writeIter + 10);
        tryRead(start, end, checkpoint, true);
    }
    {
        // When the window start differs from the write iterator, the checkpoint is valid.
        auto start = buffer.getReadIterator(writeIter - 5);
        auto checkpoint = buffer.calculateValidationCheckpoint(start);
        ASSERT_EQ(numPushed + bufferSize - 5, checkpoint);
        auto end = buffer.getReadIterator(writeIter);
        tryRead(start, end, checkpoint, false);
    }
}

TEST_F(CircularBufferTestFixture, BeginEnd)
{
    CircularBuffer<int> buffer(10);
    ASSERT_NO_THROW(
            ASSERT_EQ((long unsigned)10, buffer.pEnd().index()); // needed for container.end() (points outside of container )
            ASSERT_EQ((long unsigned)0, buffer.pBegin().index());
    );
}

TEST_F(CircularBufferTestFixture, reset)
{
    CircularBuffer<int> buffer(10);
    for(int i=0;i<10;i++)
        buffer.push(i);
    auto iter = buffer.getReadIterator(8);
    iter.reset_to(4);
    ASSERT_EQ(iter.index(), (long unsigned)4);
    iter.reset_to(12);
    ASSERT_EQ(iter.index(), (long unsigned)2);
    iter.reset_to(0);
    ASSERT_EQ(iter.index(), (long unsigned)0);
    iter.reset_to(-1);
    ASSERT_EQ(iter.index(), (long unsigned)9);
    iter.reset_to(-2);
    ASSERT_EQ(iter.index(), (long unsigned)8);
    iter.reset_to(-9);
    ASSERT_EQ(iter.index(), (long unsigned)1);
    iter.reset_to(-10);
    ASSERT_EQ(iter.index(), (long unsigned)0);
    iter.reset_to(-11);
    ASSERT_EQ(iter.index(), (long unsigned)9);
}

TEST_F(CircularBufferTestFixture, OperatorIncrement)
{
    CircularBuffer<int> buffer(10);
    for(int i=0;i<10;i++)
        buffer.push(i);
    CircularBuffer<int>::iterator iter = buffer.begin();
    for(int i=0;i<10;i++)
    {
        ASSERT_EQ(iter.index(), (long unsigned)i);
        ASSERT_EQ(*iter, i);
        ++iter;
    }
    ASSERT_EQ(iter.index(), (long unsigned)0);
    ASSERT_EQ(*iter, 0);
}

TEST_F(CircularBufferTestFixture, OperatorDecrement)
{
    CircularBuffer<int> buffer(10);
    for(int i=0;i<10;i++)
        buffer.push(i);
    CircularBuffer<int>::iterator iter = buffer.begin();

    ASSERT_EQ(iter.index(), (long unsigned)0);
    ASSERT_EQ(*iter, 0);
    --iter;
    for(int i=9;i>0;i--)
    {
        ASSERT_EQ(iter.index(), (long unsigned)i);
        ASSERT_EQ(*iter, i);
        --iter;
    }
}

TEST_F(CircularBufferTestFixture, OperatorIncrementEqual)
{
    CircularBuffer<int> buffer(10);
    auto iter = buffer.getReadIterator(8);
    iter += 8;
    ASSERT_EQ(iter.index(), (long unsigned)6);
    iter += 16;
    ASSERT_EQ(iter.index(), (long unsigned)2);
    iter += 8;
    ASSERT_EQ(iter.index(), (long unsigned)0);
}

TEST_F(CircularBufferTestFixture, OperatorDecrementEqual)
{
    CircularBuffer<int> buffer(10);
    auto iter = buffer.getReadIterator(2);
    iter -= 6;
    ASSERT_EQ(iter.index(), (long unsigned)6);
    iter -= 16;
    ASSERT_EQ(iter.index(), (long unsigned)0);
    iter -=8;
    ASSERT_EQ(iter.index(), (long unsigned)2);
}

TEST_F(CircularBufferTestFixture, assigment)
{
    CircularBuffer<int> buffer(10);
    auto iter1 = buffer.getReadIterator(8);
    auto iter2 = buffer.getReadIterator(5);
    iter2 = iter1;

    ASSERT_TRUE(iter1.index() == iter2.index());
    ASSERT_TRUE(iter1.getCircularSize() == iter2.getCircularSize());
}

TEST_F(CircularBufferTestFixture, comparison)
{
    CircularBuffer<int> buffer1(10);
    auto iter1 = buffer1.getReadIterator(8);
    auto iter2 = buffer1.getReadIterator(8);
    auto iter3 = buffer1.getReadIterator(7);
    CircularBuffer<int> buffer2(15);
    auto iter4 = buffer2.getReadIterator(8);

    ASSERT_TRUE(iter1 == iter2);
    ASSERT_FALSE(iter1 == iter3);
    ASSERT_FALSE(iter1 == iter4);

    ASSERT_FALSE(iter1 != iter2);
    ASSERT_TRUE(iter1 != iter3);
    ASSERT_TRUE(iter1 != iter4);

    ASSERT_TRUE(iter1 == iter2);
}

TEST_F(CircularBufferTestFixture, distanceTillEnd)
{
    CircularBuffer<int> buffer(10);
    auto iter1 = buffer.getReadIterator(8);
    auto iter2 = buffer.getReadIterator(9);
    auto iter3 = buffer.getReadIterator(0);
    ASSERT_EQ(iter1.distanceTillEnd(),(long unsigned)1);
    ASSERT_EQ(iter2.distanceTillEnd(),(long unsigned)0);
    ASSERT_EQ(iter3.distanceTillEnd(),(long unsigned)9);
}

TEST_F(CircularBufferTestFixture, isBetween)
{
    CircularBuffer<int> bufferSmall(10);
    auto iterSmall = bufferSmall.getReadIterator(5);
    {
		auto iter0 = bufferSmall.getReadIterator(0);
		auto iter5 = bufferSmall.getReadIterator(5);
		auto iter8 = bufferSmall.getReadIterator(8);
		ASSERT_TRUE(iter8.isBetween(iter5, iter0));
    }

    CircularBuffer<int> buffer(100);
    auto iter1 = buffer.getReadIterator(40);
    auto iter2 = buffer.getReadIterator(39);
    auto iter3 = buffer.getReadIterator(41);
    auto iter4 = buffer.getReadIterator(42);

    ASSERT_FALSE(iter1.isBetween(iterSmall, iter2)); // 40 is not between  5 and 39
    ASSERT_TRUE(iter1.isBetween(iter2, iter3));      // 40 is between     39 and 41
    ASSERT_TRUE(iter1.isBetween(iter2, iter1));      // 40 is between     39 and 40
    ASSERT_FALSE(iter1.isBetween(iter3, iter2));     // 40 is not between 41 and 39 (rollover)
    ASSERT_TRUE(iter4.isBetween(iter3, iter2));      // 42 is between     41 and 39 (rollover)
    ASSERT_FALSE(iter4.isBetween(iter2, iter3));     // 42 is not between 39 and 41
    ASSERT_TRUE(iter1.isBetween(iter1, iter1));      // 40 is between     40 and 40
    ASSERT_FALSE(iter4.isBetween(iter1, iter1));      // 42 is not between  40 and 40
}

TEST_F(CircularBufferTestFixture, distance_to)
{
    {
        CircularBuffer<int> buffer(10);
        auto iter1 = buffer.getReadIterator(1);
        auto iter2 = buffer.getReadIterator(1);
        ASSERT_EQ(iter1.distance_to(iter2), (long unsigned)0);
    }
    {
        CircularBuffer<int> buffer(10);
        auto iter1 = buffer.getReadIterator(1);
        auto iter2 = buffer.getReadIterator(3);
        ASSERT_EQ(iter1.distance_to(iter2), (long unsigned)2);
    }
    {
        CircularBuffer<int> buffer(10);
        auto iter1 = buffer.getReadIterator(1);
        auto iter2 = buffer.getReadIterator(0);
        ASSERT_EQ(iter1.distance_to(iter2), (long unsigned)9);
    }
}

TEST_F(CircularBufferTestFixture, prev_index)
{
    CircularBuffer<int> buffer(10);
    auto iter1 = buffer.getReadIterator(0);
    auto iter2 = buffer.getReadIterator(5);
    auto iter3 = buffer.getReadIterator(9);
    ASSERT_EQ(iter1.prev_index(), (long unsigned)9);
    ASSERT_EQ(iter2.prev_index(), (long unsigned)4);
    ASSERT_EQ(iter3.prev_index(), (long unsigned)8);
}

TEST_F(CircularBufferTestFixture, next)
{
    CircularBuffer<int> buffer(10);
    auto iter1 = buffer.getReadIterator(0);
    auto iter2 = buffer.getReadIterator(5);
    auto iter3 = buffer.getReadIterator(9);
    iter1 = buffer.next(iter1);
    iter2 = buffer.next(iter2);
    iter3 = buffer.next(iter3);
    ASSERT_EQ(iter1.index(), (long unsigned)1);
    ASSERT_EQ(iter2.index(), (long unsigned)6);
    ASSERT_EQ(iter3.index(), (long unsigned)0);
}

TEST_F(CircularBufferTestFixture, prev)
{
    CircularBuffer<int> buffer(10);
    auto iter1 = buffer.getReadIterator(0);
    auto iter2 = buffer.getReadIterator(5);
    auto iter3 = buffer.getReadIterator(9);
    iter1 = buffer.prev(iter1);
    iter2 = buffer.prev(iter2);
    iter3 = buffer.prev(iter3);
    ASSERT_EQ(iter1.index(), (long unsigned)9);
    ASSERT_EQ(iter2.index(), (long unsigned)4);
    ASSERT_EQ(iter3.index(), (long unsigned)8);
}

TEST_F(CircularBufferTestFixture, getCopyOfWriteIterator)
{
    int someValue = 42;
    CircularBuffer<int> buffer(10);
    buffer.push(someValue);
    buffer.push(someValue);
    auto writeIter1 = buffer.getCopyOfWriteIterator();
    auto writeIter2 = buffer.getCopyOfWriteIterator();
    ASSERT_EQ(writeIter1.index(), (long unsigned)2);
    ASSERT_TRUE(&writeIter1 != &writeIter2);
}

TEST_F(CircularBufferTestFixture, valid_distance_between)
{
    int someValue = 42;
    CircularBuffer<int> buffer(10);
    buffer.push(someValue);
    buffer.push(someValue);

    auto iter0 = buffer.getReadIterator(0);
    auto iter1 = buffer.getReadIterator(1);

    // here is the write iterator (lets validate that)
    ASSERT_EQ( buffer.getCopyOfWriteIterator().index(), (unsigned long)2);

    auto iter3 = buffer.getReadIterator(3);
    auto iter8 = buffer.getReadIterator(8);

    ASSERT_EQ(0,  buffer.valid_distance_between(iter0, iter0));
    ASSERT_EQ(1,  buffer.valid_distance_between(iter0, iter1));
    ASSERT_EQ(-8, buffer.valid_distance_between(iter1, iter3));
    ASSERT_EQ(5,  buffer.valid_distance_between(iter3, iter8));
    ASSERT_EQ(-5, buffer.valid_distance_between(iter8, iter3));
    ASSERT_EQ(3,  buffer.valid_distance_between(iter8, iter1));
    ASSERT_EQ(-3, buffer.valid_distance_between(iter1, iter8));
}

TEST_F(CircularBufferTestFixture, getLatest)
{
    int someValue = 42;
    CircularBuffer<int> buffer(3);

    {
        auto iter = buffer.getLatest();
        ASSERT_EQ(iter.index(), (long unsigned)2);
        buffer.push(someValue);
    }
    {
        auto iter = buffer.getLatest();
        ASSERT_EQ(iter.index(), (long unsigned)0);
        buffer.push(someValue);
    }
    {
        auto iter = buffer.getLatest();
        ASSERT_EQ(iter.index(), (long unsigned)1);
        buffer.push(someValue);
    }
    {
        auto iter = buffer.getLatest();
        ASSERT_EQ(iter.index(), (long unsigned)2);
        buffer.push(someValue);
    }
}

} // end namespace

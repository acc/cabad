/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <test/CircularWindowIteratorTestFixture.h>
#include <test/TimingContextMock.h>

#include <cabad/CircularWindowIterator.h>
#include <cabad/CircularBuffer.h>
#include <cabad/SampleMetaData.h>

#include <math.h>

namespace cabad
{

TEST_F(CircularWindowIteratorTestFixture, windowSize)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);

    MetaDataBuffer<int> metabuffer(10, &(*device_data_buffer_));
    CircularWindowIterator<int> window("test",
              &buffer,
              &metabuffer,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              double(0.01));

    window.updateDataRange(buffer.getReadIterator(0), buffer.getReadIterator(4));

    ASSERT_EQ(window.windowSize(), (std::size_t)5);
}

TEST_F(CircularWindowIteratorTestFixture, distanceRefMetaToWindow)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    auto refMetaSample = buffer.getReadIterator(0);

    MetaDataBuffer<int> metabuffer(10, &(*device_data_buffer_));
    std::shared_ptr<SampleMetadata<int>> new_meta( new SampleMetadata<int>(refMetaSample, 0, 1234, 0));
    metabuffer.push(new_meta);

    CircularWindowIterator<int> window("test",
              &buffer,
              &metabuffer,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              double(0.01));

    window.updateDataRange(buffer.getReadIterator(0), buffer.getReadIterator(4));

    ASSERT_THROW(
            window.distanceRefMetaToWindow(); // no ref trigger defined yet
            ,std::exception);
    window.setRefMeta(new_meta);

    auto incrementRefMetaSample = [&]()
    {
        ++refMetaSample;
        new_meta = std::make_shared<SampleMetadata<int>>(refMetaSample, 0, 1234, 0);
        window.setRefMeta(new_meta);
    };

    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(0));
    incrementRefMetaSample();
    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(-1));
    incrementRefMetaSample();
    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(-2));
    incrementRefMetaSample();
    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(-3));
    incrementRefMetaSample();
    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(-4));
    incrementRefMetaSample();
    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(5)); // ref trigger left window
    incrementRefMetaSample();
    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(4));
    incrementRefMetaSample();
    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(3));
    incrementRefMetaSample();
    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(2));
    incrementRefMetaSample();
    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(1));
    incrementRefMetaSample();
    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(0));
}

TEST_F(CircularWindowIteratorTestFixture, updateMetaWindowAccordingToSampleWindow)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    std::shared_ptr<const TimingContext> inject(new TimingContextMock(2, 283));
    auto meta1_sample = buffer.getReadIterator(0);
    auto meta2_sample = buffer.getReadIterator(5);
    auto meta3_sample = buffer.getReadIterator(8);
    auto meta4_sample = buffer.getReadIterator(9);

    MetaDataBuffer<int> metabuffer(10, &(*device_data_buffer_));
    std::shared_ptr<SampleMetadata<int>> meta1( new SampleMetadata<int>(meta1_sample, 0, 1, 0));
    std::shared_ptr<SampleMetadata<int>> meta2_inject_event( new SampleMetadata<int>(meta2_sample, 0, inject, 2, 0));
    std::shared_ptr<SampleMetadata<int>> meta3( new SampleMetadata<int>(meta3_sample, 0, 3, 0));
    std::shared_ptr<SampleMetadata<int>> meta4( new SampleMetadata<int>(meta4_sample, 0, 4, 0));
    metabuffer.push(meta1);
    metabuffer.push(meta2_inject_event);
    metabuffer.push(meta3);
    metabuffer.push(meta4);

    {   // Meta before/after data-window are not used
        CircularWindowIterator<int> window("test",
                  &buffer,
                  &metabuffer,
                  false,
                  DataPushMode::CONTINOUS_MULTI_SAMPLES,
                  double(0.01));

        window.updateDataRange(buffer.getReadIterator(2), buffer.getReadIterator(8));

        ASSERT_NO_THROW(
                window.updateMetaWindowAccordingToSampleWindow(true);
        );

        ASSERT_EQ(meta2_inject_event, window.getMetaStart());
        ASSERT_FALSE(window.getMetaDataCol().empty());
        ASSERT_EQ(meta3, window.getMetaDataCol().front());
        ASSERT_EQ(meta2_inject_event, window.getRefMeta());

        // Window is empty if no meta was found in the window
        window.updateDataRange(buffer.getReadIterator(6), buffer.getReadIterator(7));

        // Previous end should be used to find a reference trigger
        ASSERT_NO_THROW(
                window.updateMetaWindowAccordingToSampleWindow(true);
        );

        ASSERT_TRUE(window.getMetaStart() == nullptr);
        ASSERT_TRUE(window.getMetaDataCol().empty());
        ASSERT_EQ(meta2_inject_event, window.getRefMeta());

        // If no previous end was defined, just the previous reference-trigger should be reused
        ASSERT_NO_THROW(
                window.updateMetaWindowAccordingToSampleWindow(true);
        );

        ASSERT_TRUE(window.getMetaStart() == nullptr);
        ASSERT_TRUE(window.getMetaDataCol().empty());
        ASSERT_EQ(meta2_inject_event, window.getRefMeta());
    }

}

TEST_F(CircularWindowIteratorTestFixture, moveDataWindowTo)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);

    MetaDataBuffer<int> metabuffer(10, &(*device_data_buffer_));
    CircularWindowIterator<int> window("test",
              &buffer,
              &metabuffer,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              double(0.01));

    auto iter4 = buffer.getReadIterator(4);
    window.moveDataWindowTo(5, iter4);
    ASSERT_EQ((unsigned long)0, window.getDataStart().index());
    ASSERT_EQ((unsigned long)4, window.getDataEnd().index());

    auto iter9 = buffer.getReadIterator(9);
    window.moveDataWindowTo(5, iter9);
    ASSERT_EQ((unsigned long)5, window.getDataStart().index());
    ASSERT_EQ((unsigned long)9, window.getDataEnd().index());

    auto iter0 = buffer.getReadIterator(0);
    window.moveDataWindowTo(1, iter0);
    ASSERT_EQ((unsigned long)0, window.getDataStart().index());
    ASSERT_EQ((unsigned long)0, window.getDataEnd().index());

    window.moveDataWindowTo(4, iter4);
    ASSERT_EQ((unsigned long)1, window.getDataStart().index());
    ASSERT_EQ((unsigned long)4, window.getDataEnd().index());

    window.moveDataWindowTo(0, iter4);
    ASSERT_EQ((unsigned long)1, window.getDataStart().index());
    ASSERT_EQ((unsigned long)4, window.getDataEnd().index());
}

TEST_F(CircularWindowIteratorTestFixture, copyConstructor)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    auto iter4 = buffer.getReadIterator(4);
    auto iter9 = buffer.getReadIterator(9);

    MetaDataBuffer<int> metabuffer1(10, &(*device_data_buffer_));
    CircularWindowIterator<int> window1("test",
              &buffer,
              &metabuffer1,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              double(0.01));

    window1.moveDataWindowTo(5, iter4); // window1: [0..4]
    MetaDataBuffer<int> metabuffer2(10, &(*device_data_buffer_));
    CircularWindowIterator<int> window2(&metabuffer2);
    window2 = window1; // window1: [0..4], window2: [0..4]
    ASSERT_EQ((unsigned long)0, window2.getDataStart().index());
    ASSERT_EQ((unsigned long)4, window2.getDataEnd().index());

    window1.moveDataWindowTo(5, iter9); // window1: [5..9], window2: [0..4]
    ASSERT_EQ((unsigned long)5, window1.getDataStart().index());
    ASSERT_EQ((unsigned long)9, window1.getDataEnd().index());
    ASSERT_EQ((unsigned long)0, window2.getDataStart().index());
    ASSERT_EQ((unsigned long)4, window2.getDataEnd().index());
}

TEST_F(CircularWindowIteratorTestFixture, validation)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    auto iter4 = buffer.getReadIterator(4);
    auto iter8 = buffer.getReadIterator(8);

    MetaDataBuffer<int> metabuffer(10, &(*device_data_buffer_));
    CircularWindowIterator<int> window("test",
              &buffer,
              &metabuffer,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              double(0.01));

    int some_data = 123;

    auto tryBufferRead = [&buffer](const CircularWindowIterator<int>& window)
    {
        std::vector<int> data(window.windowSize());
        std::size_t written;
        buffer.copyDataWindow(window.getDataStart(), window.getDataEnd(), window.getWindowValidationCheckpoint(), data.data(), data.size(), written);
    };

    // First write, to make previous write iter differ from current write iter (otherwise the whole buffer will be invalid)
    buffer.push(some_data);

    window.moveDataWindowTo(3, iter4); // window has 3 samples: #2, #3 and #4
    ASSERT_NO_THROW(tryBufferRead(window););

    // write iter entered window
    buffer.push(some_data); // write iter now at #2
    ASSERT_THROW(tryBufferRead(window), std::runtime_error);

    // write iter left window
    int data_array1[3] = { 3,4,5 };
    buffer.push(data_array1, 3); // write iter now at #5
    ASSERT_THROW(tryBufferRead(window), std::runtime_error);

    window.moveDataWindowTo(3, iter8); // window has 3 samples: #6 #7 #8
    ASSERT_NO_THROW(tryBufferRead(window););

    // write itrator writes cross complete window
    int data_array2[5]  = { 6,7,8,9,0 };
    buffer.push(data_array2, 5); // write iter now at #0
    ASSERT_THROW(tryBufferRead(window), std::runtime_error);
}

TEST_F(CircularWindowIteratorTestFixture, isInWindow)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    auto iter1 = buffer.getReadIterator(1);
    auto iter2 = buffer.getReadIterator(2);
    auto iter3 = buffer.getReadIterator(3);
    auto iter4 = buffer.getReadIterator(4);
    auto iter5 = buffer.getReadIterator(5);

    MetaDataBuffer<int> metabuffer(10, &(*device_data_buffer_));
    CircularWindowIterator<int> window("test",
              &buffer,
              &metabuffer,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              double(0.01));

    window.moveDataWindowTo(3, iter4); // window has 3 samples: #2, #3 and #4
    ASSERT_FALSE(window.isInWindow(iter1));
    ASSERT_TRUE (window.isInWindow(iter2));
    ASSERT_TRUE (window.isInWindow(iter3));
    ASSERT_TRUE (window.isInWindow(iter4));
    ASSERT_FALSE(window.isInWindow(iter5));

    window.moveDataWindowTo(1, iter4); // window has 1 samples: #4
    ASSERT_FALSE(window.isInWindow(iter3));
    ASSERT_TRUE (window.isInWindow(iter4));
    ASSERT_FALSE(window.isInWindow(iter5));
}

TEST_F(CircularWindowIteratorTestFixture, not_used_yet)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    auto iter1 = buffer.getReadIterator(1);

    MetaDataBuffer<int> metabuffer(10, &(*device_data_buffer_));
    CircularWindowIterator<int> window("test",
              &buffer,
              &metabuffer,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              double(0.01));

    ASSERT_TRUE(window.not_used_yet());
    window.moveDataWindowTo(1, iter1);
    ASSERT_FALSE(window.not_used_yet());
}

TEST_F(CircularWindowIteratorTestFixture, timeSinceRefMetaSanityCheck)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    auto refMetaSample = buffer.getReadIterator(0);
    std::vector<float> timeSinceRefMetaForEachSample;

    MetaDataBuffer<int> metabuffer(10, &(*device_data_buffer_));
    int64_t refMetaStamp = 1234;
    std::shared_ptr<const TimingContext> timing_context(new TimingContextMock(refMetaStamp, 42));

    std::shared_ptr<SampleMetadata<int>> new_meta(new SampleMetadata<int>(refMetaSample, 0, timing_context, 0));
    metabuffer.push(new_meta);
    std::shared_ptr<SampleMetadata<int>> refMeta = metabuffer.back();

    CircularWindowIterator<int> window("test",
              &buffer,
              &metabuffer,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              double(0.01));

    window.updateDataRange(buffer.getReadIterator(0), buffer.getReadIterator(4));
    window.setRefMeta(refMeta);

    float *channeltimeSinceRefMetas = new float[window.windowSize()];
    window.setRefMeta(new_meta);

    ASSERT_THROW( // Throw if passed array is too short (instead of crash)
            window.fillTimeSinceRefMetaForEachSample(channeltimeSinceRefMetas, 2);
            ,std::exception);

    window.fillTimeSinceRefMetaForEachSample(channeltimeSinceRefMetas, window.windowSize());
    ASSERT_EQ (0, window.timeSinceRefMetaSanityCheck(timeSinceRefMetaForEachSample));

    delete []channeltimeSinceRefMetas;
}

TEST_F(CircularWindowIteratorTestFixture, timeSinceRefMetaSanityCheck_injected_ref_meta)
{
    // TODO
}

TEST_F(CircularWindowIteratorTestFixture, getTimeStampOfLastSample_before_window)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    auto refMetaSample = buffer.getReadIterator(2);
    std::vector<float> timeSinceRefMetaForEachSample;

    MetaDataBuffer<int> metabuffer(10, &(*device_data_buffer_));
    int64_t refMetaStamp = 12340000000;
    double sample_sample_distance_sec = 0.01; // == 10000000ns
    std::shared_ptr<const TimingContext> timing_context(new TimingContextMock(refMetaStamp, 42));

    std::shared_ptr<SampleMetadata<int>> new_meta(new SampleMetadata<int>(refMetaSample, 0, timing_context, refMetaStamp));
    metabuffer.push(new_meta);
    std::shared_ptr<SampleMetadata<int>> refMeta = metabuffer.back();

    CircularWindowIterator<int> window("test",
              &buffer,
              &metabuffer,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              sample_sample_distance_sec);

    window.updateDataRange(buffer.getReadIterator(4), buffer.getReadIterator(7));
    window.setRefMeta(refMeta);

    float *channeltimeSinceRefMetas = new float[window.windowSize()];
    window.fillTimeSinceRefMetaForEachSample(channeltimeSinceRefMetas, window.windowSize());
//    for(std::size_t i= 0; i< window.windowSize();i++)
//    {
//        std::cout << "channeltimeSinceRefMetas["<< i <<"]:                       " << channeltimeSinceRefMetas[i] << std::endl;
//    }
    int64_t lastSampleStampNano = refMetaStamp + int64_t(round(channeltimeSinceRefMetas[window.windowSize()-1] * 1000000000));
    //std::cout << "lastSampleStampNano: " << lastSampleStampNano << std::endl;
    ASSERT_EQ (lastSampleStampNano, window.getTimeStampOfLastSample());

    delete []channeltimeSinceRefMetas;
}

TEST_F(CircularWindowIteratorTestFixture, getTimeStampOfLastSample_on_first_sample)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    auto refMetaSample = buffer.getReadIterator(0);
    std::vector<float> timeSinceRefMetaForEachSample;

    MetaDataBuffer<int> metabuffer(10, &(*device_data_buffer_));
    int64_t refMetaStamp = 12340000000;
    double sample_sample_distance_sec = 0.01; // == 10000000ns
    std::shared_ptr<const TimingContext> timing_context(new TimingContextMock(refMetaStamp, 42));

    std::shared_ptr<SampleMetadata<int>> new_meta(new SampleMetadata<int>(refMetaSample, 0, timing_context, refMetaStamp));
    metabuffer.push(new_meta);
    std::shared_ptr<SampleMetadata<int>> refMeta = metabuffer.back();

    CircularWindowIterator<int> window("test",
              &buffer,
              &metabuffer,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              sample_sample_distance_sec);

    window.updateDataRange(buffer.getReadIterator(0), buffer.getReadIterator(4));
    window.setRefMeta(refMeta);

    float *channeltimeSinceRefMetas = new float[window.windowSize()];
    window.fillTimeSinceRefMetaForEachSample(channeltimeSinceRefMetas, window.windowSize());
//    for(std::size_t i= 0; i< window.windowSize();i++)
//    {
//        std::cout << "channeltimeSinceRefMetas["<< i <<"]:                       " << channeltimeSinceRefMetas[i] << std::endl;
//    }
    int64_t lastSampleStampNano = refMetaStamp + int64_t(round(channeltimeSinceRefMetas[window.windowSize()-1] * 1000000000));
    //std::cout << "lastSampleStampNano: " << lastSampleStampNano << std::endl;
    ASSERT_EQ (lastSampleStampNano, window.getTimeStampOfLastSample());

    delete []channeltimeSinceRefMetas;
}

TEST_F(CircularWindowIteratorTestFixture, getTimeStampOfLastSample_in_window)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    auto refMetaSample = buffer.getReadIterator(3);
    std::vector<float> timeSinceRefMetaForEachSample;

    MetaDataBuffer<int> metabuffer(10, &(*device_data_buffer_));
    int64_t refMetaStamp = 12340000000;
    double sample_sample_distance_sec = 0.01; // == 10000000ns
    std::shared_ptr<const TimingContext> timing_context(new TimingContextMock(refMetaStamp, 42));

    std::shared_ptr<SampleMetadata<int>> new_meta(new SampleMetadata<int>(refMetaSample, 0, timing_context, refMetaStamp));
    metabuffer.push(new_meta);
    std::shared_ptr<SampleMetadata<int>> refMeta = metabuffer.back();

    CircularWindowIterator<int> window("test",
              &buffer,
              &metabuffer,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              sample_sample_distance_sec);

    window.updateDataRange(buffer.getReadIterator(0), buffer.getReadIterator(4));
    window.setRefMeta(refMeta);

    float *channeltimeSinceRefMetas = new float[window.windowSize()];
    window.fillTimeSinceRefMetaForEachSample(channeltimeSinceRefMetas, window.windowSize());
//    for(std::size_t i= 0; i< window.windowSize();i++)
//    {
//        std::cout << "channeltimeSinceRefMetas["<< i <<"]:                       " << channeltimeSinceRefMetas[i] << std::endl;
//    }
    int64_t lastSampleStampNano = refMetaStamp + int64_t(round(channeltimeSinceRefMetas[window.windowSize()-1] * 1000000000));
    //std::cout << "lastSampleStampNano: " << lastSampleStampNano << std::endl;
    ASSERT_EQ (lastSampleStampNano, window.getTimeStampOfLastSample());

    delete []channeltimeSinceRefMetas;
}
} // end namespace

/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/ContextTracker.h>
#include <gtest/gtest.h>

#include <test/Util.h>

#include <string>
#include <iostream>

namespace cabad
{

class ContextTrackerFixture : public ::testing::Test
{

public:

	ContextTrackerFixture()
    {

    }

    // Is called once for all tests of this fixture
    static void SetUpTestCase();

    //called on each test
    void SetUp();
    void TearDown();

};

void ContextTrackerFixture::SetUpTestCase()
{
    setLogFunction(fake_log_function);
}
void ContextTrackerFixture::SetUp()
{

}

void ContextTrackerFixture::TearDown()
{

}
} // end namespace

#pragma once

#include <cabad/TimingContext.h>

//#include <gmock/gmock.h>
//TODO: USe GMock (update version, see https://google.github.io/googletest/gmock_cook_book.html !!
// Or is there std::mock ?

class TimingContextMock : public cabad::TimingContext
{
 public:

    TimingContextMock(int64_t timeStamp, unsigned int eventNumber): timeStamp_(timeStamp),eventNumber_(eventNumber),id_(0)
     {

     }

    TimingContextMock(int64_t timeStamp, unsigned int eventNumber, int64_t id): timeStamp_(timeStamp),eventNumber_(eventNumber),id_(id)
     {

     }

    int64_t getTimeStamp() const
    {
        return timeStamp_;
    }

    unsigned int getEventNumber() const
    {
        return eventNumber_;
    }

    int64_t getID() const
    {
        return id_;
    }

    int64_t timeStamp_;
    unsigned int eventNumber_;
    int64_t id_;

};

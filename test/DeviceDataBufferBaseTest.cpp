/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <test/DeviceDataBufferBaseFixture.h>
#include <test/TimingContextMock.h>

#include <cabad/DeviceDataBufferBase.h>

namespace cabad
{

TEST_F(DeviceDataBufferBaseFixture, extractEventName)
{
    DeviceDataBufferBase buffer(fesaConcreteEventNames_, fesaDeviceName_);

    ASSERT_NO_THROW(
            std::string eventName1 = buffer.extractEventName(fesaInternalPrefix_ + inject_event_);
            std::string eventName2 = buffer.extractEventName(inject_event_);
            ASSERT_STREQ(eventName1.c_str(), eventName2.c_str());
    );

    ASSERT_NO_THROW(
            std::string eventName = buffer.extractEventName(inject_event_);
            unsigned int id = buffer.eventName2eventID(eventName);
            ASSERT_TRUE(buffer.isTriggerEventEnabled(id));
            buffer.setTriggerEventEnableState(id, false);
            ASSERT_FALSE(buffer.isTriggerEventEnabled(id));
    );

    ASSERT_THROW(
            buffer.extractEventName("missingSeparator");
            ,std::exception);
    ASSERT_THROW(
            buffer.extractEventName("#SeparatorAtTheStart");
            ,std::exception);
}

TEST_F(DeviceDataBufferBaseFixture, extractEventID)
{
    DeviceDataBufferBase buffer(fesaConcreteEventNames_, fesaDeviceName_);

    ASSERT_NO_THROW(
            unsigned int eventID1 = buffer.extractEventID(fesaInternalPrefix_ + inject_event_);
            unsigned int eventID2 = buffer.extractEventID(inject_event_);
            ASSERT_EQ(eventID1, eventID2);
    );

    ASSERT_THROW(
            buffer.extractEventID("missingSeparator");
            ,std::exception);
}

TEST_F(DeviceDataBufferBaseFixture, Constructor)
{
    ASSERT_NO_THROW(
        DeviceDataBufferBase buffer(fesaConcreteEventNames_, fesaDeviceName_);
    );

    ASSERT_NO_THROW(
        DeviceDataBufferBase buffer(fesaConcreteEventNames_, fesaDeviceName_, inject_event_);
    );

    ASSERT_NO_THROW(
        DeviceDataBufferBase buffer(fesaConcreteEventNames_, fesaDeviceName_, inject_event_, seq_start_event_);
    );

    ASSERT_NO_THROW(
        DeviceDataBufferBase buffer(fesaConcreteEventNames_, fesaDeviceName_, inject_event_, seq_start_event_, command_event_);
    );
}

} // end namespace

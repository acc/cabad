/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <cabad/Definitions.h>

#include <gtest/gtest.h>

int main(int argc, char **argv)
{
    cabad::Events::initEvents(257, 258);

    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

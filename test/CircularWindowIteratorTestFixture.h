/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/DeviceDataBufferBase.h>

#include <gtest/gtest.h>

#include <string>
#include <iostream>
#include <memory>

namespace cabad
{

class CircularWindowIteratorTestFixture : public ::testing::Test
{

public:

    CircularWindowIteratorTestFixture()
    {
        trigger_events_.push_back("CMD_SEQ_START#257");
        trigger_events_.push_back("CMD_BEAM_INJECTION#283");
        trigger_events_.push_back("EVT_COMMAND#255");
        device_data_buffer_.reset(new DeviceDataBufferBase(trigger_events_, "FakeDevice", "CMD_BEAM_INJECTION#283", "CMD_SEQ_START#257", "EVT_COMMAND#255"));
    }

    // Is called once for all tests of this fixture
    static void SetUpTestCase();

    std::vector<std::string> trigger_events_;
    std::shared_ptr<DeviceDataBufferBase> device_data_buffer_;
};

void CircularWindowIteratorTestFixture::SetUpTestCase()
{

}


} // end namespace

/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/Util.h>
#include <cabad/Definitions.h>

#include <string>
#include <functional>

// global simulated UTC clock
extern uint64_t fakeClock;
extern bool DEBUG_ENABLED;

// fake clock is usualy started with 1 second on the clock for better stamp comparison
#define START_TIME 1000000000

namespace cabad
{

uint64_t fake_clock_get_utc_now();

void fake_log_function(LogLevel severity, const std::string& file, int line, const std::string& message, const std::string& topic);

} // end namespace

/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <test/DataReadyQueueFixture.h>

namespace cabad
{

TEST_F(DataReadyQueueFixture, push_request)
{
    uint32_t index1 = 0, index2 = 0, index3 = 0;
    ClientNotificationData<float> data;

    ASSERT_NO_THROW(

    DataReadyQueue<float> queue(100);
    ASSERT_FALSE(queue.requestData(index1, data));
    index1 = queue.push(dummy_sinkName_, dummy_context_, dummy_notificationType_, dummy_updateFrequency_, dummy_window_);

    // Data Remains on Q
    ASSERT_TRUE(queue.requestData(index1, data));
    ASSERT_TRUE(queue.requestData(index1, data));
    ASSERT_EQ(data.notificationType_,dummy_notificationType_);
    ASSERT_EQ(data.updateFrequency_,dummy_updateFrequency_);
    ASSERT_EQ(data.sinkName_, dummy_sinkName_);

    index2 = queue.push(dummy_sinkName_, dummy_context_, ClientNotificationType::TRIGGERED, dummy_updateFrequency_, dummy_window_);
    index3 = queue.push(dummy_sinkName_, dummy_context_, ClientNotificationType::STREAMING, dummy_updateFrequency_, dummy_window_);
    ASSERT_TRUE(queue.requestData(index1, data));
    ASSERT_EQ(data.notificationType_,dummy_notificationType_);
    ASSERT_TRUE(queue.requestData(index2, data));
    ASSERT_EQ(data.notificationType_,ClientNotificationType::TRIGGERED);
    ASSERT_TRUE(queue.requestData(index3, data));
    ASSERT_EQ(data.notificationType_,ClientNotificationType::STREAMING);
    );
}

TEST_F(DataReadyQueueFixture, rollover)
{
    uint32_t index1, index2, index3;
    ClientNotificationData<float> data;

    DataReadyQueue<float> queue(2);
    index1 = queue.push(dummy_sinkName_, dummy_context_, dummy_notificationType_, dummy_updateFrequency_, dummy_window_);
    ASSERT_TRUE(queue.requestData(index1, data));
    index2 = queue.push(dummy_sinkName_, dummy_context_, dummy_notificationType_, dummy_updateFrequency_, dummy_window_);
    index3 = queue.push(dummy_sinkName_, dummy_context_, dummy_notificationType_, dummy_updateFrequency_, dummy_window_);

    ASSERT_FALSE(queue.requestData(index1, data));
    ASSERT_TRUE(queue.requestData(index2, data));
    ASSERT_TRUE(queue.requestData(index3, data));
}

} // end namespace

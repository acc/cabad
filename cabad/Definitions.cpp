/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <cabad/Definitions.h>

namespace cabad
{
bool Events::initialized_ = false;
EventNumber Events::startEvent_ = 0;
EventNumber Events::endEvent_ = 0;

bool operator &(ClientNotificationType lhs, ClientNotificationType rhs)
{
    return static_cast<std::underlying_type<ClientNotificationType>::type>(lhs) &
           static_cast<std::underlying_type<ClientNotificationType>::type>(rhs);
}

ClientNotificationType operator |(ClientNotificationType lhs, ClientNotificationType rhs)
{
    return static_cast<ClientNotificationType> (
        static_cast<std::underlying_type<ClientNotificationType>::type>(lhs) |
        static_cast<std::underlying_type<ClientNotificationType>::type>(rhs)
    );
}

void Events::initEvents(EventNumber startEvent, EventNumber endEvent)
{
    if (initialized_)
    {
        throw RUNTIME_ERROR("Event configuration may only be initialized once.");
    }
    
    if (startEvent == 0 || endEvent == 0)
    {
        throw RUNTIME_ERROR("Provided start / end events must be not be zero.");
    }

    initialized_ = true;
    startEvent_ = startEvent;
    endEvent_ = endEvent;
}

EventNumber Events::getSequenceStartEvent()
{
    if (!initialized_)
    {
        throw RUNTIME_ERROR("Both start and end event must be configured before the library can be used.");
    }
    return startEvent_;
}

EventNumber Events::getSequenceEndEvent()
{
    if (!initialized_)
    {
        throw RUNTIME_ERROR("Both start and end event must be configured before the library can be used.");
    }
    return endEvent_;
}

} /* namespace */

/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/CircularBuffer.h>
#include <cabad/SampleMetaData.h>
#include <cabad/Definitions.h>

#include <vector>

namespace cabad
{

/* Keeps track on a concrete data window and alll containing metadata in that window */
template<typename T>
class CircularWindowIterator
{

public:

    enum class State
    {
        UNINITIALIZED,
        UNDER_CONSTRUCTION,
        READY
    };

    CircularWindowIterator(const std::string& name,
              CircularBuffer<T>* circularBuffer,
              MetaDataBuffer<T>* metadataBuffer,
              bool considerForStartupFinishedCount,
              DataPushMode dataPushMode,
              double sample_to_sample_distance);

    // TODO: Drop this Ctor as soon as the Fesa-Class does not require it any more !
    CircularWindowIterator()
        : window_state_(State::UNINITIALIZED),
          circularBuffer_(nullptr),
          metadataBuffer_(nullptr),
          startup_finished_(false),
          dataPushMode_(DataPushMode::DISJUNCT_SINGLE_SAMPLES),
          name_(""),
          sample_to_sample_distance_(0.0),
          sample_to_sample_distance_nano_(0.0),
          stamp_last_sample_prev_(0),
          windowValidationCheckpoint_(std::numeric_limits<uint64_t>::max())
    {
    }

    CircularWindowIterator(MetaDataBuffer<T>* metadataBuffer);

    virtual ~CircularWindowIterator() = default;

    std::size_t windowSize() const;

    void reset();

    std::string getName() const;

    void startupFinished();

    // Moves the datawindow up to the specified data_end
    // The new window will have the length n_samles
    bool moveDataWindowTo(std::size_t n_samples, const typename CircularBuffer<T>::iterator& data_end);

    void setRefMeta(const std::shared_ptr<const SampleMetadata<T>>& refMeta)
    {
        ref_meta_ = refMeta;
    }

    std::shared_ptr<const SampleMetadata<T>> getRefMeta() const
    {
        return ref_meta_;
    }

    void resetRefMeta()
    {
        ref_meta_ = std::shared_ptr<SampleMetadata<T>>(nullptr);
    }

    // distance between ref trigger and first sample of the window
    // Positive if ref trigger happened before window
    // Is 0 if ref trigger is on first sample
    // can be negative, if ref-trigger is inside window
    int64_t distanceRefMetaToWindow() const;

    bool isInWindow(const typename CircularBuffer<T>::iterator& sample) const;

    // Check if a meta item with the given timing context is in this window
    bool isInWindow(const std::shared_ptr<const TimingContext>& timingContext) const;

    // Time in seconds from each sample to the ref triigger of this window (can be negative)
    // optionally add foreign ref trigger stamp (e.g. if there is no timing)
    // Note: Use "fillTimeSinceRefMetaForEachSample" for better performance if you work with C-Arrays
    std::vector<float> getTimeSinceRefMetaForEachSample(uint64_t foreign_ref_meta_stamp = 0) const;

     // Time in seconds from each sample to the ref triigger of this window (can be negative)
     // optionally add foreign ref trigger stamp (e.g. if there is no timing)
    void fillTimeSinceRefMetaForEachSample(float*      timesSinceRefMeta,
                                           std::size_t timesSinceRefMeta_size,
                                           uint64_t    foreign_ref_meta_stamp = 0) const;

    int64_t getTimeStampOfLastSample();

    const std::vector<std::shared_ptr<SampleMetadata<T>>>& getMetaDataCol() const;
    void prefillMetadata(std::shared_ptr<SampleMetadata<T>> metadata);
    std::shared_ptr<SampleMetadata<T>> getMetaStart() const;

    State getWindowState() const;

    uint32_t getWindowStatus() const;

    // Check if timeSinceRefMeta on each sample have increasing order, as well across multiple updates
    // returns 0 if check passed, an error code otherwise (check the code for the meaning)
    int timeSinceRefMetaSanityCheck(std::vector<float>& timeSinceRefMetaForEachSample,
                                    uint64_t            foreign_ref_meta_stamp = 0,
                                    bool                enableLogging = false);

    // Find and set the metadata (and optionally reference trigger), according to the current samples
    void updateMetaWindowAccordingToSampleWindow(bool update_ref_meta);

    /**
     * Should be called when the window is moved/completed and new data start and end iterators are
     * known. This function also calculates the invalidation checkpoint used to detect the data
     * overwrite.
     */
    void updateDataRange(
            const typename CircularBuffer<T>::iterator& start,
            const typename CircularBuffer<T>::iterator& end);

    // TRUE If that window was not used so far
    bool not_used_yet()
    {
    	return data_start_ == circularBuffer_->pEnd() || data_end_ == circularBuffer_->pEnd();
    }

    uint64_t getWindowValidationCheckpoint() const;

    const typename CircularBuffer<T>::iterator& getDataStart() const;
    const typename CircularBuffer<T>::iterator& getDataEnd() const;

    // Attach conent of this window to a stream
    template<typename U>
    friend std::ostream& operator<<(std::ostream& os, const CircularWindowIterator<U>& window);

private:

    void calculateWindowValidationCheckpoint();

    typename CircularBuffer<T>::iterator data_start_;
    typename CircularBuffer<T>::iterator data_end_;

    State window_state_;

    // All meta tags in the related data-window
    // if there are no new meta tags in this window, both will point to the last valid metadata
    std::vector< std::shared_ptr<SampleMetadata<T>>> window_metadata_;

    // The CircularBuffer on which this window iter operates
    CircularBuffer<T>* circularBuffer_;

    // The metadataBuffer on which this window iter operates
    MetaDataBuffer<T>* metadataBuffer_;

    // Reference meta which will be used to calculate relative sample times (can be a trigger meta or a timestamp meta)
    std::shared_ptr<const SampleMetadata<T>> ref_meta_;

    bool startup_finished_;

    static int startup_finished_count_;

    DataPushMode dataPushMode_;

    // E.g. "signalName::triggered" for triggered window iterators ... useful for debugging
    std::string name_;

    // Distance between two samples in seconds
    double sample_to_sample_distance_;

    // Distance between two samples in nanoseconds
    double sample_to_sample_distance_nano_;

    // stamp of the last sample on the latest window update
    // only used for timeSinceRefMetaSanityCheck
    int64_t stamp_last_sample_prev_;

    // Represents the position in the data buffer at which point this window data will be invalid.
    // The position is represented in the units of CircularBuffer<T>::n_data_pushed_total_.
    // I.e. Represents the value of n_data_pushed_total_ when the window will become invalid.
    uint64_t windowValidationCheckpoint_;
};

} /* namespace cabad */

#include <cabad/CircularWindowIteratorImpl.h>
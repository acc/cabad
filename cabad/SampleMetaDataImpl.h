/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

namespace cabad
{

    // This constructor can be used when the meta item belongs to a WREvent
    template<typename T>
    SampleMetadata<T>::SampleMetadata(typename CircularBuffer<T>::iterator related_sample,
                                   uint64_t                                related_sample_absolute_offset,
                                   std::shared_ptr<const TimingContext>    timingContext,
                                   uint64_t                                related_sample_stamp,
                                   uint32_t                                status,
                                   std::shared_ptr<MetaPayload>            payload):
                                                              related_sample_(std::move(related_sample)),
                                                              related_sample_stamp_(related_sample_stamp),
                                                              related_sample_absolute_offset_(related_sample_absolute_offset),
                                                              timingContext_(std::move(timingContext)),
                                                              status_(status),
                                                              payload_(std::move(payload))

    {
    }

    // This constructor can be used when the meta item does not belong to a WREvent (idependant WR stamp from wr timing receiver)
    template<typename T>
    SampleMetadata<T>::SampleMetadata(typename CircularBuffer<T>::iterator related_sample,
                                      uint64_t                             related_sample_absolute_offset,
                                      uint64_t                             related_sample_stamp,
                                      uint32_t                             status,
                                      std::shared_ptr<MetaPayload>         payload) :
                                                              related_sample_(std::move(related_sample)),
                                                              related_sample_stamp_(related_sample_stamp),
                                                              related_sample_absolute_offset_(related_sample_absolute_offset),
                                                              status_(status),
                                                              payload_(std::move(payload))
    {
    }

    template<typename T>
    std::ostream& operator<<(std::ostream& os, const SampleMetadata<T>& meta)
    {
        os << "Related Sample index: " << meta.getRelatedSample().index() << " - Timestamp: " << meta.getRelatedSampleStamp();
        if(meta.hasTimingContext())
            os << " - EventNumber: " << meta.getTimingContext()->getEventNumber() << " EventStamp: " << meta.getTimingContextStamp();
        if(meta.getStatus() != 0)
        {
        	if (meta.getStatus() == AcquisitionStatus::TRIGGER_CONTEXT_MATCHING_NO_MATCH)
                os << " - status: TRIGGER_CONTEXT_MATCHING_NO_MATCH";
        	else if (meta.getStatus() == AcquisitionStatus::TRIGGER_CONTEXT_MATCHING_MULTIPLE_MATCHES)
        	    os << " - status: TRIGGER_CONTEXT_MATCHING_MULTIPLE_MATCHES";
        	else
        		os << " - status: Unknown";
        }
        os << std::endl;
        return os;
    }

// ############## MetaDataBuffer ##############

    template<typename T>
    MetaDataBuffer<T>::MetaDataBuffer(std::size_t           max_size,
                                   DeviceDataBufferBase* device_data_buffer) :
            max_size_(max_size),
            device_data_buffer_(device_data_buffer)
    {
        assert(max_size > 0);
    }

    template<typename T>
    void MetaDataBuffer<T>::drop_oldest_element()
    {
        if (buffer_.empty())
        {
            return;
        }

        buffer_.pop_back();
    }

    template<typename T>
    int MetaDataBuffer<T>::push(const std::shared_ptr<SampleMetadata<T>>& metadata)
    {
        if (!metadata)
        {
           throw RUNTIME_ERROR("Attempt to push nothing");
        }
        std::lock_guard<std::mutex> lock(mutex_);

        if(isRefMetaTriggerHelper(metadata))
            most_recent_ref_trigger_meta_ = metadata;


        if (buffer_.size() == 0)
        {
            buffer_.push_front(metadata);
            return 0;
        }

        // Find the most recent meta which has a timing context available.
        auto it = std::find_if(buffer_.begin(), buffer_.end(), [](const std::shared_ptr<SampleMetadata<T>>& meta)
        {
            return meta->hasTimingContext();
        });

        if (metadata->hasTimingContext() && it != buffer_.end())
        {
            // Only stricly increasing timestamps are permitted
            if(metadata->getTimingContext()->getTimeStamp() <= (*it)->getTimingContext()->getTimeStamp())
            {
                return -1; //TODO: Log Warn / Error ?
            }
        }
        
        buffer_.push_front(metadata);

        if (buffer_.size() > max_size_)
        {
            drop_oldest_element();
        }
        return 0;
    }

    template<typename T>
    std::shared_ptr<SampleMetadata<T>> MetaDataBuffer<T>::getNextTriggerMeta(const std::shared_ptr<const SampleMetadata<T>>& current_trigger_meta)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if (buffer_.empty())
        {
            throw RUNTIME_ERROR("logical error.");
        }

        if (current_trigger_meta == buffer_.front())
        {
            return std::shared_ptr<SampleMetadata<T>>(nullptr);
        }

        auto it = std::find(buffer_.rbegin(), buffer_.rend(), current_trigger_meta);
        if (current_trigger_meta != nullptr && it != buffer_.rend())
        {
            // Found current meta trigger. Increment to get the next.
            it++;
        }
        else
        {
            // If no current trigger meta is specified, start search by oldest known meta
            it = buffer_.rbegin();
        }

        for (; it != buffer_.rend(); ++it)
        {
            auto meta = *it;
            if (meta == nullptr)
            {
                throw RUNTIME_ERROR("Unexpected nullptr in MetaDataBuffer::getNextTriggerMeta.");
            }
            
            if (meta->hasTimingContext())
            {
                unsigned int eventNumber = meta->getTimingContext()->getEventNumber();
                if (device_data_buffer_->isTriggerEvent(eventNumber))
                {
                    return meta;
                }
            }
        }

        //nothing found
        return std::shared_ptr<SampleMetadata<T>>(nullptr);
    }

    template<typename T>
    std::shared_ptr<SampleMetadata<T>> MetaDataBuffer<T>::findTriggerMeta(int64_t trigger_stamp)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if (buffer_.empty())
        {
            return std::shared_ptr<SampleMetadata<T>>(nullptr);
        }

        for (const auto& meta : buffer_)
        {
            if (meta == nullptr)
            {
                throw RUNTIME_ERROR("Unexpected nullptr in MetaDataBuffer::findTriggerMeta.");
            }
            if (meta->hasTimingContext())
             {
                unsigned int eventNumber = meta->getTimingContext()->getEventNumber();
                if (device_data_buffer_->isTriggerEvent(eventNumber) && meta->getTimingContextStamp() == trigger_stamp)
                {
                    return meta;
                }
             }
        }

        //nothing found
        return std::shared_ptr<SampleMetadata<T>>(nullptr);
    }

    // searches for the oldest ref trigger in the given window
    // WIll continue to serach outside the window, if no ref trigger was found in the window
    template<typename T>
    std::shared_ptr<const SampleMetadata<T>> MetaDataBuffer<T>::findRefMeta(const std::shared_ptr<const SampleMetadata<T>>& window_start,
                                                                      const std::shared_ptr<const SampleMetadata<T>>& window_end)
    {
        if (window_start == nullptr || window_end == nullptr)
        {
            throw RUNTIME_ERROR("Invalid argument passed");
        }
        std::lock_guard<std::mutex> lock(mutex_);

        if (buffer_.empty())
        {
           throw RUNTIME_ERROR("The buffer is empty");
        }

        auto startIt = std::find(buffer_.begin(), buffer_.end(), window_start);
        auto endIt = std::find(buffer_.begin(), buffer_.end(), window_end);
        if (startIt == buffer_.end() || endIt == buffer_.end())
        {
            throw RUNTIME_ERROR("Failed to find window start/end in MetaDataBuffer.");
        }

        bool start_passed = false;
        std::shared_ptr<const SampleMetadata<T>> match;
        for (; endIt != buffer_.end(); ++endIt)
        {
            auto meta = *endIt;
            if (meta == nullptr)
            {
                throw RUNTIME_ERROR("Unexpected nullptr in MetaDataBuffer::findRefMeta.");
            }
            
            if (isRefMetaTriggerHelper(meta))
            {
                match = meta;
                // If we searched the whole window, we will continue to search older TimingContexts
                // And return the first refMeta we can find
                if (start_passed)
                {
                    return match;
                }
            }

            // If we find a ref-trigger inside the window, we use that ref-trigger.
            if (match != nullptr && endIt == startIt)
            {
                return match;
            }
            else if (endIt == startIt)
            {
                start_passed = true;
            }
        }
        //nothing found
        return std::shared_ptr<SampleMetadata<T>>(nullptr);
    }

    template<typename T>
    std::shared_ptr<SampleMetadata<T>> MetaDataBuffer<T>::getNextSequenceMarker(const std::shared_ptr<const SampleMetadata<T>>& last_sequence_marker)
    {
        std::lock_guard<std::mutex> lock(mutex_);

        if (buffer_.empty())
        {
            throw RUNTIME_ERROR("The buffer is empty");
        }

        auto it = std::find(buffer_.rbegin(), buffer_.rend(), last_sequence_marker);
        if (last_sequence_marker != nullptr && it != buffer_.rend())
        {
            it++;
        }
        else
        {
            it = buffer_.rbegin();
        }

        for (; it != buffer_.rend(); ++it)
        {
            auto meta = *it;
            if (meta == nullptr)
            {
                throw RUNTIME_ERROR("Unexpected nullptr in MetaDataBuffer::getNextSequenceMarker.");
            }

            if (meta->hasTimingContext())
            {
                if(DeviceDataBufferBase::isSequenceStartEvent(meta->getTimingContext()->getEventNumber()) ||
                    DeviceDataBufferBase::isSequenceEndEvent(meta->getTimingContext()->getEventNumber()))
                {
                    return meta;
                }
            }
        }

        // Not found.
        return std::shared_ptr<SampleMetadata<T>>(nullptr);;
    }

    template<typename T>
    std::shared_ptr<SampleMetadata<T>> MetaDataBuffer<T>::findMetaWithDataSampleOlderThan(
        const std::shared_ptr<const SampleMetadata<T>>& startMeta,
        int64_t timestamp)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if (startMeta == nullptr)
        {
            throw RUNTIME_ERROR("startMeta must not be nullptr");
        }
        auto it = std::find(buffer_.begin(), buffer_.end(), startMeta);
        if (it == buffer_.end())
        {
            throw RUNTIME_ERROR("startMeta not found in metadata buffer");
        }
        if (timestamp == startMeta->getRelatedSampleStamp())
        {
            it++;
        }
        for (; it != buffer_.end(); ++it)
        {
            auto meta = *it;
            if (meta == nullptr)
            {
                throw RUNTIME_ERROR("Unexpected nullptr in MetaDataBuffer::findMetaWithDataSampleOlderThan.");
            }
            if (meta->getRelatedSampleStamp() <= timestamp)
            {
                return meta;
            }
        }
        return {nullptr};
    }

    template<typename T>
    std::shared_ptr<SampleMetadata<T>> MetaDataBuffer<T>::findMetaWithDataSampleNewerThan(
        const std::shared_ptr<const SampleMetadata<T>>& startMeta,
        int64_t timestamp)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if (startMeta == nullptr)
        {
            throw RUNTIME_ERROR("startMeta must not be nullptr");
        }
        auto it = std::find(buffer_.rbegin(), buffer_.rend(), startMeta);
        if (it == buffer_.rend())
        {
            throw RUNTIME_ERROR("startMeta not found in metadata buffer");
        }
        if (timestamp == startMeta->getRelatedSampleStamp())
        {
            it++;
        }
        
        for (; it != buffer_.rend(); ++it)
        {
            auto meta = *it;
            if (meta == nullptr)
            {
                throw RUNTIME_ERROR("Unexpected nullptr in MetaDataBuffer::findMetaWithDataSampleOlderThan.");
            }
            if (meta->getRelatedSampleStamp() >= timestamp)
            {
                return meta;
            }
        }
        return {nullptr};
    }

    // get the oldest metadata which is available
    template<typename T>
    std::shared_ptr<SampleMetadata<T>> MetaDataBuffer<T>::back()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if(buffer_.empty())
           throw RUNTIME_ERROR("The buffer is empty");
        if (buffer_.back() == nullptr)
        {
            throw RUNTIME_ERROR("Back element of buffer is nullptr.");
        }
        return buffer_.back();
    }

    // get the most recent metadata which is available
    template<typename T>
    std::shared_ptr<SampleMetadata<T>> MetaDataBuffer<T>::front()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if(buffer_.empty())
           throw RUNTIME_ERROR("The buffer is empty");

        if (buffer_.front() == nullptr)
        {
            throw RUNTIME_ERROR("Front element of buffer is nullptr.");
        }
        return buffer_.front();
    }

template<typename T>
bool MetaDataBuffer<T>::isCycledOut(const std::shared_ptr<const SampleMetadata<T>>& metadata)
{
    std::lock_guard<std::mutex> lock(mutex_);
    if(buffer_.empty())
    {
        return true;
    }

    if (metadata == nullptr)
    {
        throw RUNTIME_ERROR("Provided metadata must not be nullptr.");
    }
    
    auto& back = buffer_.back();
    if (back == nullptr)
    {
        throw RUNTIME_ERROR("Back element of metadata buffer is nullptr.");
    }
    
    return back->related_sample_stamp_ > metadata->related_sample_stamp_;
}

template<typename T>
std::ostream& operator<<(std::ostream& os, const MetaDataBuffer<T>& buffer)
{
    os << "### MetaDataBuffer ### - max_size: " << buffer.max_size_ << std::endl;
    // for(auto& iter : buffer.buffer_)
    //     os << *iter;
    os << "####################################" << std::endl;
    return os;
}

template<typename T>
bool MetaDataBuffer<T>::empty()
{
    std::lock_guard<std::mutex> lock(mutex_);
    return buffer_.empty();
}

template<typename T>
bool MetaDataBuffer<T>::isRefMetaTriggerEvent(EventNumber eventID) const
{
    std::lock_guard<std::mutex> lock(mutex_);
    return isRefMetaTriggerEventHelper(eventID);
}

template<typename T>
bool MetaDataBuffer<T>::isRefMetaTriggerEventHelper(EventNumber eventID) const
{
    // We dont have any reference meta? Aceppt all suppored ID's
    if(!most_recent_ref_trigger_meta_)
    {
        if( eventID == device_data_buffer_->getRefMetaTriggerEventID()          ||
            eventID == device_data_buffer_->getRefMetaTriggerEventIDFallback1() ||
            eventID == device_data_buffer_->getRefMetaTriggerEventIDFallback2()    )
            return true;
        return false;
    }

    // Only use Fallback events if the correct event was never seen
    if(eventID == device_data_buffer_->getRefMetaTriggerEventID())
        return true;
    else if(eventID == device_data_buffer_->getRefMetaTriggerEventIDFallback1())
    {
        if(most_recent_ref_trigger_meta_->getTimingContext()->getEventNumber() == device_data_buffer_->getRefMetaTriggerEventIDFallback1()) // only fallback1 so far
            return true;
        if(most_recent_ref_trigger_meta_->getTimingContext()->getEventNumber() == device_data_buffer_->getRefMetaTriggerEventIDFallback2()) // only fallback2 so far
            return true;
    }
    else if(eventID == device_data_buffer_->getRefMetaTriggerEventIDFallback2())
    {
        if(most_recent_ref_trigger_meta_->getTimingContext()->getEventNumber() == device_data_buffer_->getRefMetaTriggerEventIDFallback2()) // only fallback2 so far
            return true;
    }
    return false;
}

template<typename T>
bool MetaDataBuffer<T>::isRefMetaTrigger(const std::shared_ptr<const SampleMetadata<T>>& meta) const
{
    std::lock_guard<std::mutex> lock(mutex_);
    return isRefMetaTriggerHelper(meta);
}

template<typename T>
bool MetaDataBuffer<T>::isRefMetaTriggerHelper(const std::shared_ptr<const SampleMetadata<T>>& meta) const
{
    if(!meta->hasTimingContext())
        return false;

    return isRefMetaTriggerEventHelper(meta->getTimingContext()->getEventNumber());
}

} /* namespace cabad */

/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/Definitions.h>
#include <cabad/Util.h>

#include <cstring>
#include <iostream>
#include <cassert>
#include <vector>
#include <memory>
#include <atomic>

namespace cabad
{

/* class to describe a circular buffer for a single type */
template <typename T>
class CircularBuffer
{
public:

    CircularBuffer(std::size_t size)
        : size_(size),
          numSamplesLastPush_(0),
          n_data_pushed_total_(0)
    {
        data_ = new T[size_];

        write_iterator_ = iterator(data_ , size_, 0);
    }

    ~CircularBuffer()
    {
        delete[] data_;
    }

    class iterator
    {
        public:
            iterator() : iterator(nullptr, 0, 0) {}
            iterator(T* data, std::size_t size, std::size_t index = 0):
                circularSize_(size),
				data_(data)
            {
                // Explicitly allow this case to support buffer.end(), which is at index "circularSize_"
                if(index == circularSize_)
                {
                    index_ = circularSize_;
                }
                else
                {
                    index_ = index % circularSize_;
                }
            }
            iterator(const iterator& other)
                : circularSize_(other.circularSize_),
                  index_(other.index_),
                  data_(other.data_)
            {

            }

            iterator& operator= (const iterator& other)
            {
                data_ = other.data_;
                index_ = other.index_;
                circularSize_ = other.circularSize_;
                return *this;
            }
            iterator& operator+= (std::size_t length)
            {
                index_ = (length + index_) % circularSize_;
                return *this;
            }
            iterator& operator-= (std::size_t length)
            {
                length = length % circularSize_;
                if(length > index_)
                {
                    index_ = circularSize_ - (length - index_);
                }
                else
                {
                    index_ -= length;
                }
                return *this;
            }
            iterator& operator++ ()
            {
                index_ = (index_ + 1) % circularSize_;
                return *this;
            }
            iterator& operator-- ()
            {
                if( index_ == 0 )
                {
                    index_ = circularSize_-1;
                    return *this;
                }
                index_ = (index_ - 1) % circularSize_;
                return *this;
            }
            bool operator==(const iterator& rhs) const
            {
                return index_ == rhs.index_ && data_ == rhs.data_ && index_ == rhs.index_;
            }
            bool operator!=(const iterator& rhs) const
            {
                return !(*this == rhs);
            }

            const T& operator*() const
            {
                // Due to iterator::setToEnd it's possible that index_ will be
                // equal to size of buffer. In that case we need to throw.
                if (index_ >= circularSize_)
                {
                    throw RUNTIME_ERROR("Error: iterator is out of bound.");
                }
                return data_[index_];
            }
            const T* operator->() const
            {
                // Due to iterator::setToEnd it's possible that index_ will be
                // equal to size of buffer. In that case we need to throw.
                if (index_ >= circularSize_)
                {
                    throw RUNTIME_ERROR("Error: iterator is out of bound.");
                }
                return data_ + index_;
            }

            void reset_to(int32_t index)
            {
                while(index < 0)
                {
                    index += circularSize_;
                }
                if(index == 0)
                {
                    index_ = 0;
                }
                else
                {
                    index_ = index % circularSize_;
                }
            }
            void setToEnd()
            {
                index_ = circularSize_;
            }

            std::size_t index() const
            {
                return index_;
            }
            std::size_t prev_index() const
            {
                if(index_ == 0)
                {
                    return circularSize_ - 1;
                }
                return index_ - 1;
            }

            // TRUE if the iterator index is between, or directly on start or end
            bool isBetween(const iterator& start, const iterator& end) const
            {
                if(start.circularSize_ != circularSize_ || end.circularSize_ != circularSize_)
                {
                    return false;
                }

                if(start.index() < end.index())
                {
                    return index_ >= start.index() && index_ <= end.index();
                }
                else if(start.index() == end.index()) // Single sample 
                {
                    return index_ == start.index();
                }
                else // rollover
                {
                    return index_ <=  end.index() || index_ >= start.index();
                }
                return false;
            }

            // The result will always be positive.
            // Number of samples to step, until the index of other is found
            std::size_t distance_to(const iterator& other) const
            {
                if (other.index_ >= this->index_)
                {
                    return other.index_ - this->index_;
                }
                else // rollover
                {
                    return this->distanceTillEnd() + other.index_ + 1;
                }
            }
            std::size_t distanceTillEnd() const
            {
                return circularSize_ - index_ - 1;
            }
            std::size_t getCircularSize() const
            {
                return circularSize_;
            }

        private:
            iterator& operator++(int unused); // postfix not supported (prefix anyhow is faster)
            iterator& operator--(int unused); // postfix not supported (prefix anyhow is faster)
            
            std::size_t circularSize_;
            std::size_t index_;
            T* data_;
    };

    const T& operator[](std::size_t index) const
    {
        assert(index < size_);
        return data_[index];
    }

    iterator begin() const
    {
        return {data_, size_, 0};
    }

    iterator end() const
    {
        return {data_, size_, size_};
    }

    void push(const T* data, std::size_t data_size)
    {
        if(data_size > size_)
            throw RUNTIME_ERROR("Error: Cannot push chunks which exceed the size of the circular buffer.");

        n_data_pushed_total_ += data_size;

        // Copy the data twice if end of the rolling buffer is reached
        std::size_t remainingBufferSize =  write_iterator_.distanceTillEnd() + 1; // +1 to including write_iterator_ itself
        if( remainingBufferSize < data_size ) // copy twice
        {
            std::size_t rolloverSize = data_size - remainingBufferSize;
            memcpy(&data_[write_iterator_.index()], data, remainingBufferSize * sizeof(T));
            memcpy(&data_[0], &data[remainingBufferSize], rolloverSize * sizeof(T));
        }
        else // single copy
        {
            memcpy(&data_[write_iterator_.index()], data, data_size * sizeof(T));
        }

        write_iterator_ += data_size;
        numSamplesLastPush_ = data_size;
    }

    void push(const std::vector<T>& data)
    {
    	push(data.data(), data.size());
    }

    void push(const T& data)
    {
        n_data_pushed_total_++;
        data_[write_iterator_.index()] = data;
        ++write_iterator_;
        numSamplesLastPush_ = 1;
    }

    void copyDataViaCallback(const iterator& start, const iterator& end, uint64_t windowValidationCheckpoint, std::function<void(T* firstData, std::size_t firstDataSize, T* secondData, std::size_t secondDataSize)> copyFunc)
    {
        auto sizeToRead = start.distance_to(end) + 1; // +1 to read the end itself as well
        if (start.index() <= end.index())
        {
            copyFunc(&data_[start.index()], sizeToRead, nullptr, 0);
        }
        else
        {
            auto remainingSize = start.distanceTillEnd() + 1;
            auto rolloverSize = sizeToRead - remainingSize;
            copyFunc(&data_[start.index()], remainingSize, &data_[0], rolloverSize);
        }

        // After we finished reading out the data validate that the data was not overwritten.
        if (n_data_pushed_total_ >= windowValidationCheckpoint)
        {
            throw RUNTIME_ERROR("Readout data was overwritten. Data is invalid.");
        }
    }

    // copies data from buffer into array
    bool copyDataWindow(const iterator& start, const iterator& end, uint64_t windowValidationCheckpoint, T* buffer, const std::size_t& buffer_size, std::size_t& data_written)
    {
        data_written = start.distance_to(end) + 1; // +1 to read the end itself as well
        if(data_written > buffer_size)
        {
            std::string message;
            message = "Error: Write aborted. data to write ("+ std::to_string(data_written) +" elements) would exceed available buffer size (" + std::to_string(buffer_size) + " elements)";
            data_written = 0;
            throw RUNTIME_ERROR(message);
        }
        if( start.index() <= end.index())
        {
            memcpy(&buffer[0], &data_[start.index()], data_written * sizeof(T));
        }
        else
        {
            std::size_t size_remaining = start.distanceTillEnd() + 1; // +1 to including data_start_ itself
            std::size_t size_rollover = data_written - size_remaining;
            memcpy(&buffer[0], &data_[start.index()], size_remaining * sizeof(T));
            memcpy(&buffer[size_remaining], &data_[0], size_rollover * sizeof(T));
        }

        // After we finished reading out the data validate that the data was not overwritten.
        if (n_data_pushed_total_ >= windowValidationCheckpoint)
        {
            throw RUNTIME_ERROR("Readout data was overwritten. Data is invalid.");
        }

        // For the sake of backward compatibility
        return true;
    }

    std::size_t size() const
    {
        return size_;
    }

    iterator pBegin()
    {
        return getReadIterator(0);
    }

    iterator pEnd()
    {
        iterator iter = getReadIterator(size_);
        iter.setToEnd();
        return iter;
    }

    std::size_t getWriteIterIndex() const
    {
        return write_iterator_.index();
    }

    void moveIterToLatest(iterator& iter) const
    {
        iter = write_iterator_;
        --iter;
    }

    bool isWriteIter(const iterator& iter) const
    {
        return write_iterator_ == iter;
    }

    /*
     * distance_to_write_iter
     * @iter: Iterator to check the distance
     *
     * Returns the distance from the passed iterator to the write iterator.
     * E.g. if @iter is on samle 2 and write iter is on sample 5, than 3 will be returned.
     * If both iterators point to the same sample, 0 will be returned.
     */
    std::size_t distance_to_write_iter(const iterator& iter) const
    {
        return iter.distance_to(write_iterator_);
    }

    /*
     * valid_distance_between
     * @iter1: First Iterator to check
     * @iter2: Second Iterator to check
     *
     * Returns the valid distance from the first to the second iterator.
     * The distance is considered 'valid' if there is no write iterator in between.
     * Note that in contrast to "distance_to", this method will return a negative distance, if the positive distance is not valid !
     */
	int32_t valid_distance_between(const iterator& iter1, const iterator& iter2) const
    {
        std::size_t distance = iter1.distance_to(iter2);
        std::size_t distance_to_write_iter = this->distance_to_write_iter(iter1);
        if(distance == distance_to_write_iter)
        {
            LOG_ERROR("There is no valid distance between this iterator and the write iterator.");
            return distance;
        }
        else if(distance > distance_to_write_iter)
        {
            return -1 * iter2.distance_to (iter1);
        }
        else
        {
            return distance;
        }
    }

    iterator getReadIterator(int32_t index = 0)
    {
        while(index < 0)
        {
            index += size_;
        }
        if(index != 0)
        {
           index = index % size_;
        }
        iterator iter = iterator(data_, size_, index);
        return iter;
    }

    iterator getLatest()
    {
       return getReadIterator(int32_t(write_iterator_.index()) - 1);
    }

    iterator getCopyOfWriteIterator()
    {
        return getReadIterator(write_iterator_.index());
    }

    // creates a new iterator, on the next slot and returns it
    iterator next(const iterator& iter)
    {
        return getReadIterator(iter.index() + 1);
    }

    // creates a new iterator, on the previous slot and returns it
    iterator prev(const iterator& iter)
    {
        return getReadIterator(iter.index() - 1);
    }

    uint64_t n_data_pushed_total() const
    {
    	return n_data_pushed_total_;
    }

    /**
     * Calculates the value of n_data_pushed_total_ at which the data will become invalid
     * (overwritten) assuming dataStart + buffer size as the point of invalidation. This function
     * is useful when the end of the data is not yet known.
     */
    uint64_t calculateValidationCheckpoint(
            const iterator& dataStart) const
    {
        auto distance = dataStart.distance_to(write_iterator_);
        if (distance == 0)
        {
            // Write iterator points to data start.
            // Window is already invalid.
            return n_data_pushed_total_;
        }
        return n_data_pushed_total_ + size_ - distance;
    }
    /**
     * Calculates the value of n_data_pushed_total_ at which the data within the iterators
     * will become invalid (overwritten). Assuming dataStart + buffer size as the point of
     * invalidation. DataEnd used to determine if the window is invalid already at the point of
     * this calculation.
     */
    uint64_t calculateValidationCheckpoint(
            const iterator& dataStart,
            const iterator& dataEnd) const
    {
        auto distance = dataStart.distance_to(write_iterator_);
        if (write_iterator_.isBetween(dataStart, dataEnd))
        {
            // Window is already invalid.
            return n_data_pushed_total_ - distance;
        }
        return n_data_pushed_total_ + size_ - distance;
    }

    std::size_t getNumLastPushed() const
    {
        return numSamplesLastPush_;
    }

private:

    std::size_t size_;

    std::size_t numSamplesLastPush_;

    // Current write iterator. Must only be accesed from the writer thread (with the member functions).
    iterator write_iterator_;

    // Number of data items pushed to the buffer in total. This value together with the functions
    // calculateValidationCheckpoint() is used to determine whether the data in the buffer is still
    // valid. When the readout window is constructed and completed (in CircularBufferManagerBase)
    // it must obtain the invalidation point by calling calculateValidationCheckpoint(). The
    // calculated value and the value of n_data_pushed_total_ is then used after the readout to
    // determine the validity of data.
    std::atomic<uint64_t> n_data_pushed_total_;

    T* data_;
};

} /* namespace cabad */

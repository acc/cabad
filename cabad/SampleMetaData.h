/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/CircularBuffer.h>
#include <cabad/DeviceDataBufferBase.h>
#include <cabad/TimingContext.h>

#include <algorithm>
#include <deque>
#include <memory>
#include <mutex>

namespace cabad
{

namespace AcquisitionStatus
{
enum AcquisitionStatus
{
       // A Trigger meta could not be matched to its related timing context within the given tolerance window
       TRIGGER_CONTEXT_MATCHING_NO_MATCH          = 0x01,

       // Multiple timing context matches were found for a single trigger event
       TRIGGER_CONTEXT_MATCHING_MULTIPLE_MATCHES  = 0x02,
};
}

// An abstract base class for adding user provided data to the SampleMetadata. When the user
// requires additional metadata stored with the SampleMetadata, they should derive from this class
// and provide an instante of the child class to the CircularBufferManager's add metadata functions.
class MetaPayload
{
public:
    virtual ~MetaPayload() = 0;
};

inline MetaPayload::~MetaPayload()
{
}

template<typename T> class MetaDataBuffer;

template<typename T>
class SampleMetadata
{
public:
    // This constructor can be used when the meta item belongs to a WREvent
    SampleMetadata(typename CircularBuffer<T>::iterator related_sample,
                   uint64_t                             related_sample_absolute_offset,
                   std::shared_ptr<const TimingContext> timingContext,
                   uint64_t                             related_sample_stamp,
                   uint32_t                             status = 0,
                   std::shared_ptr<MetaPayload>         payload = nullptr);

    // This constructor can be used when the meta item does not belong to a WREvent (idependant WR stamp from wr timing receiver)
    SampleMetadata(typename CircularBuffer<T>::iterator related_sample,
                   uint64_t                             related_sample_absolute_offset,
                   uint64_t                             related_sample_stamp,
                   uint32_t                             status = 0,
                   std::shared_ptr<MetaPayload>         payload = nullptr);

    //Constructors needed for unit-testing
    SampleMetadata(const std::shared_ptr<const TimingContext>& timingContext):
            related_sample_stamp_(timingContext->getTimeStamp()),
            related_sample_absolute_offset_(0),
            timingContext_(timingContext),
            status_(0)
    {

    }

    SampleMetadata(const typename CircularBuffer<T>::iterator& related_sample,
                   const std::shared_ptr<const TimingContext>& timingContext):
            related_sample_(related_sample),
            related_sample_stamp_(timingContext->getTimeStamp()),
            related_sample_absolute_offset_(0),
            timingContext_(timingContext),
            status_(0)
    {

    }

    SampleMetadata(const typename CircularBuffer<T>::iterator& related_sample, uint64_t related_sample_stamp = 0):
            related_sample_(related_sample),
            related_sample_stamp_(related_sample_stamp),
            related_sample_absolute_offset_(0),
            status_(0)
    {

    }

    SampleMetadata(uint64_t related_sample_stamp = 0):
            related_sample_stamp_(related_sample_stamp),
            related_sample_absolute_offset_(0),
            status_(0)
    {

    }

    bool hasTimingContext() const
    {
        return timingContext_ != nullptr;
    }

    std::shared_ptr<const TimingContext> getTimingContext() const
    {
        if(!timingContext_)
            throw RUNTIME_ERROR("This SampleMetadata has no Timing context");
        return timingContext_;
    }

    int64_t getTimingContextStamp() const
    {
        if(!timingContext_)
            throw RUNTIME_ERROR("This SampleMetadata has no Timing context");
        return timingContext_->getTimeStamp();
    }

    // Concrete stamp of the related sample
    int64_t getRelatedSampleStamp() const
    {
        return related_sample_stamp_;
    }

    uint64_t getRelatedSampleAbsoluteOffset() const
    {
        return related_sample_absolute_offset_;
    }

    typename CircularBuffer<T>::iterator getRelatedSample() const
    {
        return related_sample_;
    }

    uint32_t getStatus() const
    {
        return status_;
    }

    void setStatus(uint32_t status)
    {
        status_ = status;
    }

    std::shared_ptr<MetaPayload> getPayload() const
    {
        return payload_;
    }

    // Attach any relevant printable info to the stream
    template<typename U>
    friend std::ostream& operator<<(std::ostream& os, const SampleMetadata<U>& meta);
private:

    typename CircularBuffer<T>::iterator          related_sample_;
    int64_t                                       related_sample_stamp_;
    uint64_t                                      related_sample_absolute_offset_;    // (number of items read from input stream in total)

    // Timing context which is very close to the related sample
    std::shared_ptr<const TimingContext> timingContext_;

	// Internal Status - bit enum, see "AcquisitionStatus"
    uint32_t status_;

    std::shared_ptr<MetaPayload> payload_;

    friend class MetaDataBuffer<T>;
};

// Linked list of SampleMetadata
template<typename T>
class MetaDataBuffer
{

public:

    MetaDataBuffer(std::size_t max_size, DeviceDataBufferBase* device_data_buffer);

    /*
     * push:
     * @metadata : New metadata to be pushed into the buffer
     *
     * The new metadata will be pushed into the buffer, if it does not exist yet (checked by comparing TimingContext)
     * The new metadata will be inserted at the proper location, so that all metadata in the buffer will always be sorted chronologically
     *
     * Return value: 0 on sucess, -1 if a metadata with the same TImingContext already exists
     */
    int push(const std::shared_ptr<SampleMetadata<T>>& metadata);

    //empty if nothing is found
    std::shared_ptr<SampleMetadata<T>> getNextTriggerMeta(const std::shared_ptr<const SampleMetadata<T>>& current_trigger_meta);

    //empty if nothing is found
    std::shared_ptr<SampleMetadata<T>> findTriggerMeta(int64_t trigger_stamp);

    // searches for the oldest ref trigger in the given window
    // Will continue to serach outside the window, if no ref trigger was found in the window
    // empty, if not found
    std::shared_ptr<const SampleMetadata<T>> findRefMeta(const std::shared_ptr<const SampleMetadata<T>>& window_start,
                                                         const std::shared_ptr<const SampleMetadata<T>>& window_end);

    std::shared_ptr<SampleMetadata<T>> getNextSequenceMarker(const std::shared_ptr<const SampleMetadata<T>>& last_sequence_marker);

    /*
     * Search the buffer for the first metadata which has a related data sample with timestamp that
     * is older or equal to the provided timestamp. If no meta is found nullptr is returned.
     */
    std::shared_ptr<SampleMetadata<T>> findMetaWithDataSampleOlderThan(const std::shared_ptr<const SampleMetadata<T>>& startMeta, int64_t timestamp);

    /*
     * Search the buffer for the first metadata which has a related data sample with timestamp that
     * is newer or equal to the provided timestamp. If no meta is found nullptr is returned.
     */
    std::shared_ptr<SampleMetadata<T>> findMetaWithDataSampleNewerThan(const std::shared_ptr<const SampleMetadata<T>>& startMeta, int64_t timestamp);

    // get the oldest metadata which is available
    std::shared_ptr<SampleMetadata<T>> back();

    // get the most recent metadata which is available
    std::shared_ptr<SampleMetadata<T>> front();

    // True if the timestamp of the passed metadata is older than the oldest element in the buffer
    bool isCycledOut(const std::shared_ptr<const SampleMetadata<T>>& metadata);

    bool empty();

    template <class Callable>
    void for_each_meta_data(Callable callable)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if(buffer_.empty())
            return;

        for(auto iter = buffer_.begin(); iter != buffer_.end(); iter++)
        {
            if (callable(*iter) == false)
                return;
        }
    }


    bool isRefMetaTriggerEvent(EventNumber eventID) const;

    bool isRefMetaTrigger(const std::shared_ptr<const SampleMetadata<T>>& meta) const;

    // Attach any relevant printable info to the stream
    template<typename U>
    friend std::ostream& operator<<(std::ostream& os, const MetaDataBuffer<U>& buffer);
private:

    bool isRefMetaTriggerHelper(const std::shared_ptr<const SampleMetadata<T>>& meta) const;
    bool isRefMetaTriggerEventHelper(EventNumber eventID) const;

    // drops the oldest element of the buffer
    void drop_oldest_element();

    // Based on the current usage of the metadata buffer, the mutex isn't strictly required.
    // However to guarantee that the buffer would be working also in multiple threads, we leave it in.
    mutable std::mutex mutex_;
    std::size_t max_size_;
    std::deque<std::shared_ptr<SampleMetadata<T>>> buffer_;
    DeviceDataBufferBase* device_data_buffer_;

    // MetaData of the most recent ref-trigger data we received
    std::shared_ptr<SampleMetadata<T>> most_recent_ref_trigger_meta_;
};

} /* namespace cabad */

#include <cabad/SampleMetaDataImpl.h>
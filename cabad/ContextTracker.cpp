/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <cabad/ContextTracker.h>
#include <cabad/Definitions.h>
#include <cabad/Util.h>

namespace cabad
{

ContextTracker::ContextTracker(std::size_t buffer_capacity):
        max_size_(buffer_capacity)
{
    std::cout << "Creating ContextTracker of buffer_capacity:" << buffer_capacity << std::endl;
}

void ContextTracker::addContext(const std::shared_ptr<const TimingContext>& context)
{
    std::lock_guard<std::mutex> lock(mutex_);

    buffer_.push_front(context);
    if(buffer_.size() > max_size_)
        buffer_.pop_back();

    LOG_TRACE("New WR context added. Event: " + std::to_string(context->getEventNumber()) + " Timestamp: " + std::to_string(context.get()->getTimeStamp()) );
    //std::cout << "ContextTracker::addWRContext of event: " << context->getEventNumber() <<  " Timestamp: " << context.get()->getTimeStamp() << std::endl;
}

std::deque < std::shared_ptr<const TimingContext>  > ContextTracker::getContextRange(int64_t rangeStartStamp, int64_t rangeEndStamp)
{
    if( rangeStartStamp > rangeEndStamp)
        throw RUNTIME_ERROR("Logical error: rangeStartStamp > rangeEndStamp");

    std::lock_guard<std::mutex> lock(mutex_);

    std::deque < std::shared_ptr<const TimingContext> > result;
    for(auto& iter : buffer_)
    {
        if(rangeStartStamp <= iter->getTimeStamp() && iter->getTimeStamp() <= rangeEndStamp)
            result.push_front(iter);
    }

    return result;
}

std::shared_ptr<const TimingContext> ContextTracker::getNextTriggerContext(int64_t searchStartStamp, const DeviceDataBufferBase& deviceDataBuffer)
{
    std::lock_guard<std::mutex> lock(mutex_);

    bool start_found = false;

    // We need to iterate reverse, in order to get increasing stamps
    for (auto iter = buffer_.rbegin(); iter != buffer_.rend(); ++iter)
    {
    	if(!start_found && (*iter)->getTimeStamp() >= searchStartStamp)
    		start_found = true;

    	if(start_found)
    	{
    		//std::cout << "deviceDataBuffer.isTriggerEvent(iter->getEventNumber()): " << deviceDataBuffer.isTriggerEvent(iter->getEventNumber()) << std::endl;
    		if(deviceDataBuffer.isTriggerEvent((*iter)->getEventNumber()) && deviceDataBuffer.isTriggerEventEnabled((*iter)->getEventNumber()))
				return *iter;
    	}
    }

    //nothing found
    return std::shared_ptr<const TimingContext>(nullptr);
}

std::shared_ptr<const TimingContext> ContextTracker::getContextByID(int64_t id)
{
    std::lock_guard<std::mutex> lock(mutex_);

    for(auto& iter : buffer_)
    {
        if(id == iter->getID())
            return iter;
    }

    //nothing found
    return std::shared_ptr<const TimingContext>(nullptr);
}
} /* namespace cabad */

/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <vector>
#include <cmath>
#include <sstream>
#include <limits>

// TODO: Remove magic number, allow to configure by constructor
#define N_SEQUENCE_WINDOWS 10

#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))

namespace cabad
{

static int OK    = 0;
static int ERROR = 1;

template<typename T>
CircularBufferManagerBase<T>::CircularBufferManagerBase(std::string signal_name,
                                                      float sample_rate_hz,
                                                      std::vector<MaxClientUpdateFrequencyType> maxClientUpdateFrequencies,
                                                      ContextTracker* contextTracker,
                                                      DeviceDataBufferBase* deviceDataBuffer,
                                                      ClientNotificationType clientNotificationType,
                                                      DataPushMode dataPushMode,
                                                      std::size_t circularBufferSize,
                                                      std::size_t metadataBufferSize,
                                                      std::size_t dataQueueSize,
                                                      std::function<void(uint32_t data_id, CircularBufferManagerBase*)> dataReadyCallback):
                                                circularBuffer_(circularBufferSize),
                                                metadata_buffer_(metadataBufferSize, deviceDataBuffer),
                                                contextTracker_(contextTracker),
                                                signal_name_(signal_name),
                                                deviceDataBuffer_(deviceDataBuffer),
                                                dataReadyQueue_(dataQueueSize),
                                                dataReadyCallback_(std::move(dataReadyCallback)),
                                                last_context_search_end_stamp_(0),
                                                pre_trigger_time_(0),
                                                post_trigger_time_(0),
                                                clientNotificationType_(clientNotificationType),
                                                new_sequence_marker_detected_(false),
                                                new_trigger_detected_(false),
                                                dataPushMode_(dataPushMode),
                                                sample_rate_hz_(sample_rate_hz),
                                                n_matched_triggers_(100), // will always check the 100 latest triggers
                                                alligment_offset_data_timestamp_WRStamp_(10)
{
    LOG_TRACE("Creating new circular buffer '" + signal_name_ + " sample_rate_hz: " + std::to_string(sample_rate_hz) );

    sample_to_sample_distance_ =  double(1.) / sample_rate_hz;
    sample_to_sample_distance_nano_ = sample_to_sample_distance_ * 1000000000.;

    // Mainly required for unit-testing to initialize to now instead of '0'
    // In order to make sure that the first update only happens after min_client_update_delay_ns_, not emmediatly
    auto lastStreamingUpdate = get_timestamp_nano_utc();

    if( clientNotificationType_ & ClientNotificationType::STREAMING )
    {
        for (auto& maxClientUpdateFrequency : maxClientUpdateFrequencies )
        {
            readDataWindowColStreaming_.insert(std::make_pair(maxClientUpdateFrequency,  new CircularWindowIterator<T>(signal_name_,
                                                                                                                    &circularBuffer_,
                                                                                                                    &metadata_buffer_,
                                                                                                                    true,
                                                                                                                    dataPushMode_,
                                                                                                                    sample_to_sample_distance_)));

            log_levels_per_maxClientUpdateFrequency_[maxClientUpdateFrequency] = LogLevel::WARNING;
            lastStreamingUpdate_.insert(std::make_pair(maxClientUpdateFrequency, lastStreamingUpdate));
        }
    }

    numberOfFullSequenceWindows_ = N_SEQUENCE_WINDOWS; // TODO: Remove magic number
    if( clientNotificationType_ & ClientNotificationType::FULL_SEQUENCE )
    {
        for(std::size_t i = 0; i < numberOfFullSequenceWindows_; i++)
        {
            readDataWindowColFullSequence_.push_back(new CircularWindowIterator<T>(signal_name + " - FULL_SEQUENCE",
                                                                                &circularBuffer_,
                                                                                &metadata_buffer_,
                                                                                false,
                                                                                dataPushMode_,
                                                                                sample_to_sample_distance_));
        }

        currentFullSequenceWindow_ = readDataWindowColFullSequence_.begin();
    }

    if( clientNotificationType_ & ClientNotificationType::TRIGGERED )
    {
        for(auto& triggerEvent : deviceDataBuffer_->getTriggerEvents())
        {
            readDataWindowColTriggered_.insert(std::make_pair(triggerEvent.first,
                new CircularWindowIterator<T>(signal_name + " - TRIGGERED",
                                           &circularBuffer_,
                                           &metadata_buffer_,
                                           false,
                                           dataPushMode_,
                                           sample_to_sample_distance_)));
        }
    }

    triggerMatchingTolerance_ns_ = 1000000; // 1ms is the default

    // Set default log-level
    log_levels_per_type_[ClientNotificationType::STREAMING]     =  LogLevel::WARNING;
    log_levels_per_type_[ClientNotificationType::FULL_SEQUENCE] =  LogLevel::WARNING;
    log_levels_per_type_[ClientNotificationType::TRIGGERED]     =  LogLevel::WARNING;
}

template<typename T>
CircularBufferManagerBase<T>::~CircularBufferManagerBase()
{
    if( clientNotificationType_ & ClientNotificationType::STREAMING )
    {
        for (auto window : readDataWindowColStreaming_)
            delete window.second;
    }
    if( clientNotificationType_ & ClientNotificationType::FULL_SEQUENCE )
    {
        for(auto window : readDataWindowColFullSequence_)
            delete window;
    }
    if( clientNotificationType_ & ClientNotificationType::TRIGGERED )
    {
        for (auto eventWindowPair : readDataWindowColTriggered_)
        {
            delete eventWindowPair.second;
        }
    }
    //TODO: delete  stuff
}

template<typename T>
void CircularBufferManagerBase<T>::log_error_if(ClientNotificationType type, const std::string& message)
{
    if(log_levels_per_type_[type] <= LogLevel::ERROR)
        LOG_ERROR_FOR_TOPIC (message, signal_name_);
}
template<typename T>
void CircularBufferManagerBase<T>::log_trace_if(ClientNotificationType type, const std::string& message)
{
    if(log_levels_per_type_[type] <= LogLevel::TRACE)
        LOG_TRACE_FOR_TOPIC (message, signal_name_);
}
template<typename T>
void CircularBufferManagerBase<T>::log_trace_if(ClientNotificationType type, MaxClientUpdateFrequencyType maxClientUpdateFrequency, const std::string& message)
{
    if(log_levels_per_type_[type] <= LogLevel::TRACE && log_levels_per_maxClientUpdateFrequency_[maxClientUpdateFrequency] <= LogLevel::TRACE)
        LOG_TRACE_FOR_TOPIC (message, signal_name_);
}

template<typename T>
bool CircularBufferManagerBase<T>::is_log_enabled_for(ClientNotificationType type, LogLevel log_level)
{
    return log_levels_per_type_[type] <= log_level;
}
template<typename T>
bool CircularBufferManagerBase<T>::is_log_enabled_for(ClientNotificationType type, MaxClientUpdateFrequencyType maxClientUpdateFrequency, LogLevel log_level)
{
    return log_levels_per_type_[type] <= log_level && log_levels_per_maxClientUpdateFrequency_[maxClientUpdateFrequency] <= log_level;
}

template<typename T>
bool CircularBufferManagerBase<T>::getDataByID(uint32_t id, ClientNotificationData<T>& data) const
{
    return dataReadyQueue_.requestData(id, data);
}

template<typename T>
uint64_t CircularBufferManagerBase<T>::n_data_pushed_total() const
{
    return circularBuffer_.n_data_pushed_total();
}

template<typename T>
const MetaDataBuffer<T>& CircularBufferManagerBase<T>::getMetaBuffer() const
{
    return metadata_buffer_;
}

template<typename T>
int CircularBufferManagerBase<T>::findSampleWindowMarkers (const typename CircularBuffer<T>::iterator& firstSample,
                                                        const typename CircularBuffer<T>::iterator& lastSample,
                                                        int64_t& firstSampleStamp,
                                                        int64_t& firstContextSearchStamp,
                                                        int64_t& lastContextSearchStamp,
                                                        int64_t& first_sample_abs_offset)
{
    SampleMetadata<T>* meta_start = NULL;
    int64_t distanceMetaToFirstSample = 0;
    int64_t distanceMetaToLastSample  = 0;

    if( dataPushMode_ == DataPushMode::DISJUNCT_SINGLE_SAMPLES || dataPushMode_ == DataPushMode::CONTINOUS_MULTI_SAMPLES )
    {
        // Not possible to allign any MultiplexingContextData, if we dont even have a data timestamp
        if(!most_recent_meta_)
            return OK;

        // The standard use-case, we added multiplexing info before
        if( mux_contexts_info_added_till_here_)
        {
            meta_start = &(*mux_contexts_info_added_till_here_);
            //std::cout << "mux_contexts_info_added_till_here_ used"  << std::endl;
        }
        else // first time we add a context, or mux_contexts_info_added_till_here_ has cycled out of the buffer
        {
            meta_start = &(*most_recent_meta_);
            //std::cout << "most_recent_meta_ used"  << std::endl;
        }
    }

   // std::cout << "meta_start: "; meta_start->print();
//    std::cout << "firstSample->index()                    " << firstSample->index()<< std::endl;
//    std::cout << "lastSample->index()                     " << lastSample->index() << std::endl;


    if ( dataPushMode_ == DataPushMode::DISJUNCT_SINGLE_SAMPLES )
    {
        // If we have a meta-item on each sample, we dont need to calculate any sample_to_sample stuff
        firstSampleStamp = metadata_buffer_.front()->getRelatedSampleStamp();

        if(mux_contexts_info_added_till_here_)
        {
            // We already added some context info. Start search for new contexts right after the latest added mux-context stamp
            // In order to prevent adding the same meta twice
            firstContextSearchStamp = mux_contexts_info_added_till_here_->getRelatedSampleStamp() + 1;
            if(mux_contexts_info_added_till_here_ == metadata_buffer_.back())
                LOG_ERROR(signal_name_ + ": Logical Error, findSampleWindowMarkers called without adding new metadata");
        }
        else
        {
            // First time we search for contexts .. just search the whole metadata_buffer
            firstContextSearchStamp = metadata_buffer_.back()->getRelatedSampleStamp();
        }

        // Always search up to the last timestamp we added
        lastContextSearchStamp = metadata_buffer_.front()->getRelatedSampleStamp();

        distanceMetaToFirstSample = firstSampleStamp - firstContextSearchStamp;
        first_sample_abs_offset = meta_start->getRelatedSampleAbsoluteOffset() + distanceMetaToFirstSample;
    }
    else if ( dataPushMode_ == DataPushMode::CONTINOUS_MULTI_SAMPLES )
    {
        //std::cout << "sample_to_sample_distance_nano " << sample_to_sample_distance_nano << std::endl;
        // we refer to a meta which occured before the first sample.
        distanceMetaToFirstSample = circularBuffer_.valid_distance_between(meta_start->getRelatedSample(), firstSample);
        distanceMetaToLastSample  = circularBuffer_.valid_distance_between(meta_start->getRelatedSample(), lastSample);

//        std::cout << "distanceMetaToFirstSample :" << distanceMetaToFirstSample << std::endl;
//        std::cout << "distanceMetaToLastSample  :" << distanceMetaToLastSample << std::endl;

        firstSampleStamp = meta_start->getRelatedSampleStamp() + (distanceMetaToFirstSample * sample_to_sample_distance_nano_);
        first_sample_abs_offset = meta_start->getRelatedSampleAbsoluteOffset() + distanceMetaToFirstSample;

        if( last_context_search_end_stamp_ == 0) // First time we search ? Than start by the first sample stamp
            firstContextSearchStamp = firstSampleStamp;
        else                                     // We already searched beafore ? Start directly after tha last search-stamp (we dont want to add the same event twice)
            firstContextSearchStamp = last_context_search_end_stamp_ + 1;

        lastContextSearchStamp  = meta_start->getRelatedSampleStamp() + floor(distanceMetaToLastSample * sample_to_sample_distance_nano_);

        // To make sure the values are sane
        if(firstContextSearchStamp > lastContextSearchStamp)
            firstContextSearchStamp = lastContextSearchStamp;

//        std::cout << "firstSampleStamp " << firstSampleStamp << std::endl;
//        std::cout << "firstContextSearchStamp :" << firstContextSearchStamp << std::endl;
//        std::cout << "lastContextSearchStamp  :" << lastContextSearchStamp << std::endl;
        //std::cout << "search window ns " << lastContextSearchStamp - firstContextSearchStamp<< std::endl;
        last_context_search_end_stamp_ = lastContextSearchStamp;
    }
    else if ( dataPushMode_ == DataPushMode::DISJUNCT_MULTI_SAMPLES )
    {
        // We know that the metaitem will be on or after the first sample
        int64_t distanceFirstSampleToMeta = firstSample.distance_to(metadata_buffer_.front()->getRelatedSample());
        int64_t distanceMetaToLastSample  = metadata_buffer_.front()->getRelatedSample().distance_to(lastSample);

        firstSampleStamp = metadata_buffer_.front()->getRelatedSampleStamp() - floor(distanceFirstSampleToMeta * sample_to_sample_distance_nano_);
        firstContextSearchStamp = firstSampleStamp;
        lastContextSearchStamp  = metadata_buffer_.front()->getRelatedSampleStamp() + floor(distanceMetaToLastSample * sample_to_sample_distance_nano_);
        first_sample_abs_offset = metadata_buffer_.front()->getRelatedSampleAbsoluteOffset() + distanceMetaToFirstSample;
//        std::cout << "firstSampleStamp " << firstSampleStamp << std::endl;
//        std::cout << "firstContextSearchStamp " << firstContextSearchStamp << std::endl;
//        std::cout << "lastContextSearchStamp " << lastContextSearchStamp << std::endl;
//        std::cout << "distanceFirstSampleToMeta " << distanceFirstSampleToMeta << std::endl;
//        std::cout << "distanceMetaToLastSample " << distanceMetaToLastSample << std::endl;
    }
    else
    {
        LOG_ERROR(signal_name_ + ": DataPushMode not yet supported");
    }
    return OK;
}

template<typename T>
int CircularBufferManagerBase<T>::addMultiplexingContextDataForRange(const typename CircularBuffer<T>::iterator& firstSample,
                                                                  const typename CircularBuffer<T>::iterator& lastSample)
{
    int64_t firstSampleStamp, firstContextSearchStamp, lastContextSearchStamp, first_sample_abs_offset;
    firstSampleStamp = firstContextSearchStamp = lastContextSearchStamp = first_sample_abs_offset = 0;
    findSampleWindowMarkers(firstSample, lastSample, firstSampleStamp, firstContextSearchStamp, lastContextSearchStamp, first_sample_abs_offset );

    //std::cout << "distanceMetaStartToFirstSample: " << distanceMetaStartToFirstSample << std::endl;
    //std::cout << "distanceMetaStartToLastSample:  " << distanceMetaStartToLastSample << std::endl;
    //std::cout << "addMultiplexingContextDataForRange 2 " << std::endl;
    if(firstContextSearchStamp > lastContextSearchStamp)
    {
        LOG_WARNING(signal_name_ + "addMultiplexingContextDataForRange - firstContextSearchStamp is bigger than lastContextSearchStamp.\n"
                + " firstContextSearchStamp: " + std::to_string(firstContextSearchStamp) + "\n"
                + " lastContextSearchStamp : " + std::to_string(lastContextSearchStamp)  + "\n");
        return ERROR;
    }
    //if( sample_rate_hz_ == 10.) // only check for 10Hz sinks
    //    std::cout << "seaching meta from:  " << firstContextSearchStamp << " till: " << lastContextSearchStamp << std::endl;
    auto contextRange = contextTracker_->getContextRange(firstContextSearchStamp, lastContextSearchStamp );

//    std::cout << "addMultiplexingContextDataForRange 3 " << std::endl;
//    std::cout << "#############################################################################" << std::endl;
//    std::cout << "contextRange.size(): " << contextRange.size() << std::endl;
    if( contextRange.size() == 0)
        return OK;

    //std::cout << "#############################################################################" << std::endl;
    int64_t sample_stamp = firstSampleStamp; // TODO: int64_t --> uint64_t
    int64_t sample_abs_offset = first_sample_abs_offset;
    //std::cout << "sample_abs_offset_iter_start:" << sample_abs_offset_iter << std::endl;
    for (auto& contextIter : contextRange)
    {
        //std::cout << "addMultiplexingContextDataForRange Found context at: " <<  contextIter->getTimeStamp() << std::endl;
        int64_t index_offset_first_sample_till_context = 0;
        if( dataPushMode_ == DataPushMode::DISJUNCT_SINGLE_SAMPLES ) // in this case, we always add the contexts to the current single sample
            index_offset_first_sample_till_context = 0;
        else if ( dataPushMode_ == DataPushMode::CONTINOUS_MULTI_SAMPLES || dataPushMode_ == DataPushMode::DISJUNCT_MULTI_SAMPLES )
        {
            int64_t time_first_sample_till_context_nano = contextIter->getTimeStamp() - firstSampleStamp;
            index_offset_first_sample_till_context = floor(time_first_sample_till_context_nano / sample_to_sample_distance_nano_);
        }
        else
        {
            LOG_ERROR(signal_name_ + ": DataPushMode not yet supported");
        }

        // Make sure we dont have a negative offset
        // FIXME: For low freq, e.g. 1Hz, we NEED to consider as well old contexts
        if(index_offset_first_sample_till_context < 0)
        {
            continue;
            //LOG_ERROR(signal_name_ + "Logical Error: index_offset_first_sample_till_context is negative");
            //return ERROR;
        }

        if(dataPushMode_ == DataPushMode::DISJUNCT_MULTI_SAMPLES || dataPushMode_ == DataPushMode::CONTINOUS_MULTI_SAMPLES) // in single sample mode, the stamp is always the same
            sample_stamp = firstSampleStamp + floor( index_offset_first_sample_till_context * sample_to_sample_distance_nano_);
        auto sample_iter = circularBuffer_.getReadIterator(firstSample.index() + index_offset_first_sample_till_context);
        sample_abs_offset = first_sample_abs_offset + index_offset_first_sample_till_context;

        // We did not measure any sample when this evtn occured
        // That might happen during startup. Just ignore the event
        if(sample_abs_offset <= 0)
            continue;

//        std::cout << "sample_abs_offset_iter:" << sample_abs_offset_iter << std::endl;
//        std::cout << "sample_iter->index(): " << sample_iter->index() << std::endl;
//        std::cout << "sample_iter_wr_stamp: " <<  sample_stamp_iter << std::endl;
//        std::cout << " creating trigger meta: " << contextIter->getEventNumber() << " : "<< contextIter->getTimeStamp() << std::endl;

        // Add a new meta item
        std::shared_ptr<SampleMetadata<T>> meta = std::make_shared<SampleMetadata<T>>(circularBuffer_.getReadIterator(sample_iter.index()), sample_abs_offset, contextIter, sample_stamp, 0);
        addMetaData(meta);
    }

    mux_contexts_info_added_till_here_ = most_recent_meta_;
    return OK;
}

// A new WR stamp to add to the data-stream
// Could be just a random WR-stamp, not correcalted to a WR-Event. Or the WR stamp which matches some specific WR event
// @related_sample_stamp
/*
 * addMeta:
 *
 * A new WR stamp to add to the data-stream.
 * Could be just a random WR-stamp, not correcalted to a WR-Event. Or the WR stamp which matches some specific WR event
 *
 * @related_sample_stamp :
 * @related_sample_absolute_offset:
 * @status:
 * @match_wr_stamp_to_wr_event: Wheather this WR stamp should be matched to a
 */
template<typename T>
std::shared_ptr<SampleMetadata<T>> CircularBufferManagerBase<T>::addMeta(int64_t meta_timestamp,
                                                                   uint64_t related_sample_absolute_offset,
                                                                   bool match_wr_stamp_to_wr_event,
                                                                   std::shared_ptr<MetaPayload> payload)
{
    int64_t offset_to_write_iter = circularBuffer_.n_data_pushed_total() - related_sample_absolute_offset;
    if( offset_to_write_iter < 0 )
    {
        LOG_ERROR(signal_name_ + ": Not allowed to add WrStampMetaData if the related sample is located after the write iterator");
        return std::shared_ptr<SampleMetadata<T>>(nullptr);
    }

    auto related_sample = circularBuffer_.getReadIterator(int32_t(circularBuffer_.getWriteIterIndex()) - offset_to_write_iter);
    uint32_t internal_status = 0;

    // For TriggerMeta we need find the related WREvent and stamp the sample on which the trigger happened accordingly
    // The old timestamp on that sample can be used to calculate the general time difference between sample stamp and wr stamp
    if(match_wr_stamp_to_wr_event)
    {
        std::shared_ptr<const TimingContext> logical_next_context;
        if(last_matching_trigger_context_)
        {
            //std::cout << "attempt to just get the next available trigger context" << signal_name_ << std::endl;
            logical_next_context = contextTracker_->getNextTriggerContext(last_matching_trigger_context_->getTimeStamp() + 1, *deviceDataBuffer_);
        }

        std::shared_ptr<const TimingContext> best_matching_context;

        // search for according MultiplexingContext
        auto contextRange = contextTracker_->getContextRange(meta_timestamp - triggerMatchingTolerance_ns_, meta_timestamp + triggerMatchingTolerance_ns_ );

        // offset between the utc stamp of data and timing (positive offset --> matched WR stamp is older than stamp on sample)
        int64_t alligment_offset = 0;
        int64_t smallest_alligment_offset_abs = std::numeric_limits<int64_t>::max();

        std::map<std::int64_t, std::string> matchingEvents;

        std::size_t n_matching_trigger_events = 0;
        for (auto& context : contextRange)
        {
            if (deviceDataBuffer_->isTriggerEvent(context->getEventNumber()) &&  deviceDataBuffer_->isTriggerEventEnabled(context->getEventNumber()))
            {
                n_matching_trigger_events++;
                matchingEvents.insert({context->getTimeStamp(), deviceDataBuffer_->eventID2eventName(context->getEventNumber())});
                int64_t offset = meta_timestamp - context->getTimeStamp();
                if (abs(offset) < smallest_alligment_offset_abs)
                {
                    smallest_alligment_offset_abs = abs(offset);
                    if(abs(offset) < abs(triggerMatchingTolerance_ns_) )
                    {
                        alligment_offset = offset;
                        best_matching_context = context;
                        //std::cout << "match! - alligment_offset micro:" <<  round(alligment_offset / 1000.)<< std::endl;
                    }
                }
            }
        }

        if (n_matching_trigger_events == 0)
        {
            if(last_matching_trigger_context_)
            {
                //std::cout << "attempt to just get the next available trigger context" << signal_name_ << std::endl;
                best_matching_context = logical_next_context;

                // Only do this trick once .. otherwise there is the risk of wrong matches
                last_matching_trigger_context_ = std::shared_ptr<const TimingContext>(nullptr);
            }
            else
            {
                std::cout << "No previous trigger context found" << signal_name_ << std::endl;
            }
        }
        else if(n_matching_trigger_events > 1)
        {
            internal_status |= AcquisitionStatus::TRIGGER_CONTEXT_MATCHING_MULTIPLE_MATCHES;

            std::stringstream ss;
            ss << signal_name_ << " The trigger-event with stamp " << meta_timestamp << "could not be aligned to a specific WR-Event, since multiple WR-Events do match:\n";
            for (auto& event : matchingEvents)
            {
                ss << "- Name: " << event.second << ", Stamp: " << event.first << "\n";
            }
            ss << "Try to decrease the matchingTolerance, increase the sample rate of your measuring device or only let your signal-source trigger by events which are more distinct from each other.";
            
            LOG_WARNING(ss.str());
        }
        else
        {
            // only if we have a single match, we update the average offset
            alligment_offset_data_timestamp_WRStamp_.push(alligment_offset);
        }
        //std::cout << "n_matching_trigger_events: " << n_matching_trigger_events <<  std::endl;

        if(best_matching_context) // only add these which actually do match
        {
            if(logical_next_context && logical_next_context->getTimeStamp() != best_matching_context->getTimeStamp())
            {
                LOG_ERROR(signal_name_ + ": URGH, order of matched triggers-events is corrupt !!");
                LOG_ERROR(signal_name_ + ": Found next via matching: " + std::to_string(best_matching_context->getTimeStamp()));
                LOG_ERROR(signal_name_ + ": But logically should be: " + std::to_string(logical_next_context->getTimeStamp()));
                n_matched_triggers_.push(0.);
                return std::shared_ptr<SampleMetadata<T>>(nullptr);
            }
            n_matched_triggers_.push(1.);
            last_matching_trigger_context_ = best_matching_context;
            new_trigger_detected_ = true;

            // Apply concrete time-offset to the related sample stamp
            int64_t related_sample_stamp = meta_timestamp - alligment_offset;
            std::shared_ptr<SampleMetadata<T>> meta = std::make_shared<SampleMetadata<T>>(related_sample, related_sample_absolute_offset, best_matching_context, related_sample_stamp, internal_status, std::move(payload));
            addMetaData(meta);
            return meta;
        }
        else
        {
            n_matched_triggers_.push(0.);
            internal_status |= AcquisitionStatus::TRIGGER_CONTEXT_MATCHING_NO_MATCH;
            //std::cout << "no matching context found for trigger in signal " << signal_name_ << std::endl;
        }
    }
    else
    {
        // Apply average offset to the related sample stamp
        int64_t related_sample_stamp = meta_timestamp - alligment_offset_data_timestamp_WRStamp_.get_average();
        std::shared_ptr<SampleMetadata<T>> meta = std::make_shared<SampleMetadata<T>>(related_sample, related_sample_absolute_offset, related_sample_stamp, internal_status, std::move(payload));
        addMetaData(meta);
        return meta;
    }

    //std::cout << "alligment_offset_ micro:" << round(alligment_offset / 1000.)<< std::endl;

	//std::cout << "delayWRStampToTriggerStamp_: " << delayWRStampToTriggerStamp_ << std::endl;
    //std::cout << "Add Meta for timeStamp: " << timeStamp << std::endl;


    return std::shared_ptr<SampleMetadata<T>>(nullptr);
}

template<typename T>
std::shared_ptr<SampleMetadata<T>>CircularBufferManagerBase<T>::addWrStampMetaData(
            int64_t wr_stamp,
            uint64_t related_sample_absolute_offset,
            std::shared_ptr<MetaPayload> payload)
{
    return addMeta(wr_stamp, related_sample_absolute_offset, false, std::move(payload));
}

template<typename T>
std::shared_ptr<SampleMetadata<T>> CircularBufferManagerBase<T>::addTriggerMetaData(
            int64_t  hardware_trigger_stamp,
            uint64_t related_sample_absolute_offset,
            std::shared_ptr<MetaPayload> payload)
{
    return addMeta(hardware_trigger_stamp, related_sample_absolute_offset, true, std::move(payload));
}

template<typename T>
std::shared_ptr<SampleMetadata<T>> CircularBufferManagerBase<T>::addTriggerMetaData(
            const std::shared_ptr<const TimingContext>& context,
            uint64_t related_sample_absolute_offset,
            int64_t related_sample_stamp,
            std::shared_ptr<MetaPayload> payload)
{
    int64_t offset_to_write_iter = circularBuffer_.n_data_pushed_total() - related_sample_absolute_offset;
    if( offset_to_write_iter < 0 )
    {
        LOG_ERROR(signal_name_ + ": Not allowed to add WrStampMetaData if the related sample is located after the write iterator");
        return std::shared_ptr<SampleMetadata<T>>(nullptr);
    }

    auto related_sample = circularBuffer_.getReadIterator(int32_t(circularBuffer_.getWriteIterIndex())-offset_to_write_iter);
    n_matched_triggers_.push(1.);
    new_trigger_detected_ = true;

    std::shared_ptr<SampleMetadata<T>> meta = std::make_shared<SampleMetadata<T>>(related_sample, related_sample_absolute_offset, context, related_sample_stamp, 0, std::move(payload));
    addMetaData(meta);
    return meta;
}

template<typename T>
void CircularBufferManagerBase<T>::setTriggerSamples(std::size_t pre_trigger_samples, std::size_t post_trigger_samples)
{
    switch (dataPushMode_)
    {
    case DataPushMode::CONTINOUS_SINGLE_SAMPLES:
    case DataPushMode::CONTINOUS_MULTI_SAMPLES:
    case DataPushMode::DISJUNCT_MULTI_SAMPLES:
        pre_trigger_time_ = pre_trigger_samples * sample_to_sample_distance_nano_;
        post_trigger_time_ = post_trigger_samples * sample_to_sample_distance_nano_;
        break;
    case DataPushMode::DISJUNCT_SINGLE_SAMPLES:
    default:
        throw RUNTIME_ERROR("The method 'setTriggerSamples' cannot be used for DISJUNCT_SINGLE_SAMPLES DataPushMode. "
                            "Please use 'setTriggerTime' instead");
    }
}

template<typename T>
void CircularBufferManagerBase<T>::setTriggerTime(double pre_trigger_time, double post_trigger_time)
{
    pre_trigger_time_ = pre_trigger_time;
    post_trigger_time_ = post_trigger_time;
}

template<typename T>
void CircularBufferManagerBase<T>::pushBackDataFinished()
{
    if (clientNotificationType_ & ClientNotificationType::TRIGGERED)
    {
        // Complete unfinished triggered windows 
        for (auto& eventWindowPair : readDataWindowColTriggered_)
        {
            if (eventWindowPair.second->getWindowState() != CircularWindowIterator<T>::State::UNDER_CONSTRUCTION)
            {
                continue;
            }

            auto refMeta = eventWindowPair.second->getRefMeta();
            if (refMeta == nullptr)
            {
                // There was something wrong with the trigger window. Reset it.
                eventWindowPair.second->reset();
                LOG_ERROR("Unexpected corrupted trigger window which was under construction.");
            }
            moveTriggeredWindow(refMeta->getTimingContextStamp(), eventWindowPair.first);
        }
    }
    
    if((clientNotificationType_ & ClientNotificationType::TRIGGERED) && new_trigger_detected_)
    {
        new_trigger_detected_ = false;

        // Will get the next trigger meta which occured
        // (or the first which ever happened if most_recent_trigger_meta_ is empty)
        std::shared_ptr<SampleMetadata<T>> new_trigger = metadata_buffer_.getNextTriggerMeta(most_recent_trigger_meta_);
        //std::cout << *metadata_buffer_;
        if (new_trigger)
        {
            //log_trace_if(signal_name_ + " request move for trigger: " << *new_trigger;
            EventNumber eventNumber = new_trigger->getTimingContext()->getEventNumber();
            moveTriggeredWindow(new_trigger->getTimingContextStamp(), eventNumber);
        }
        else
        {
// FIXME Due to unprecise timestamping of the digitizer in streaming mode, it may happen that timing-events of trigger windows are mismatched.
// So it is possible to receive "new_trigger_detected_", but the trigger happened before most_recent_trigger_meta_, so that getNextTriggerMeta will fail to find it.

//            std::ostringstream message;
//            if(most_recent_trigger_meta_)
//                message << signal_name_  << ": New trigger detected, but no next trigger event found while searching for:" << *most_recent_trigger_meta_;
//            else
//                message << signal_name_  << ": New trigger detected, but no first trigger event found on meta buffer:";
//            message << *metadata_buffer_;
//            LOG_ERROR(message.str());
        }
        most_recent_trigger_meta_ = new_trigger;
    }
    if(clientNotificationType_ & ClientNotificationType::STREAMING)
    {
        auto last_sample = circularBuffer_.getLatest();

        for(const auto& pair : readDataWindowColStreaming_ )
        {
            // In order to synchronize the number of samples which are updated (always minimum across all channels)
            int64_t last_sample_stamp = 0;
            std::size_t n_pending_samples;

            CircularWindowIterator<T>* previous_window = pair.second;
            if(previous_window->not_used_yet())
            	n_pending_samples = circularBuffer_.getWriteIterIndex();
            else
                n_pending_samples = previous_window->getDataEnd().distance_to(last_sample);

            // For DISJUNCT_SINGLE_SAMPLES, the latest meta stamp is used, if available
            if(dataPushMode_ == DataPushMode::DISJUNCT_SINGLE_SAMPLES)
            {
                if(most_recent_meta_)
                    last_sample_stamp = most_recent_meta_->getRelatedSampleStamp();
            }
            else if (dataPushMode_ == DataPushMode::DISJUNCT_MULTI_SAMPLES)
            {
                LOG_ERROR(signal_name_ + ": DataPushMode not yet supported");
            }
            else
            {
                // If available, use timestamp of the last sample to synchronize. 0 Otherwise
                if(!previous_window->not_used_yet())
                {
                    uint64_t last_stamp_previous_window = previous_window->getTimeStampOfLastSample();
                    if(last_stamp_previous_window)
                        last_sample_stamp = last_stamp_previous_window + floor(n_pending_samples * sample_to_sample_distance_nano_);
                }
            }

            if (n_pending_samples == 0)
            {
                continue;
            }
            uint64_t now = get_timestamp_nano_utc();

            MaxClientUpdateFrequencyType updateFrequency = pair.first;
            auto streamingUpdate = lastStreamingUpdate_[updateFrequency];
            // Check if it is already time to do a client update
            uint64_t min_client_update_delay_ns = 1000000000. / updateFrequency;
            if(now >= (streamingUpdate + min_client_update_delay_ns))
            {
                moveStreamingWindow(last_sample_stamp, n_pending_samples, updateFrequency);
                lastStreamingUpdate_[updateFrequency] = now;
            }
        }
    }
    if((clientNotificationType_ & ClientNotificationType::FULL_SEQUENCE) && new_sequence_marker_detected_)
    {
        new_sequence_marker_detected_ = false;
        if (most_recent_sequence_marker_ && most_recent_sequence_marker_->hasTimingContext())
        {
            moveFullSequenceWindow(most_recent_sequence_marker_->getTimingContextStamp());
        }
    }
}

template<typename T>
int CircularBufferManagerBase<T>::addMetaData(const std::shared_ptr<SampleMetadata<T>>& new_meta)
{
    if(metadata_buffer_.push(new_meta) != 0)
    {
        // Was not inserted, since already exists in meta-buffer
        return OK;
    }

    most_recent_meta_ = new_meta;

    //std::cout << "CircularBufferManagerBase::addMetaData: "; new_meta->print();

    if(!new_meta->hasTimingContext())
        return OK;

    most_recent_timing_event_meta_ = new_meta;

    if(deviceDataBuffer_->isSequenceStartEvent(new_meta->getTimingContext()->getEventNumber()) || deviceDataBuffer_->isSequenceEndEvent(new_meta->getTimingContext()->getEventNumber()))
    {
        if(most_recent_sequence_marker_)
        {
            if(most_recent_sequence_marker_->getTimingContextStamp() != new_meta->getTimingContextStamp())
            {
                most_recent_sequence_marker_ = new_meta;
                new_sequence_marker_detected_ = true;
            }
            else
            {
                log_trace_if (ClientNotificationType::FULL_SEQUENCE, "Eeek ..  attempt to add sequence-marker twice to metabuffer" + signal_name_);
            }
        }
        else
        {
            most_recent_sequence_marker_ = new_meta;
            new_sequence_marker_detected_ = true;
        }
    }

    return OK;
}

template<typename T>
CircularWindowIterator<T>* CircularBufferManagerBase<T>::updateTriggerWindowIter(
        int64_t trigger_stamp,
        EventNumber eventNumber)
{
    log_trace_if (ClientNotificationType::TRIGGERED, "-------------- updateTriggerWindowIter start for signal " + signal_name_);

    auto window = readDataWindowColTriggered_[eventNumber];

    if (window->getWindowState() == CircularWindowIterator<T>::State::READY)
    {
        window->reset();
        log_error_if (ClientNotificationType::TRIGGERED, "TRIG - we already processed this trigger .. resetting window.");
        return nullptr;
    }

    std::shared_ptr<SampleMetadata<T>> trigger_meta = metadata_buffer_.findTriggerMeta(trigger_stamp);
    if(!trigger_meta)
    {
        LOG_ERROR(signal_name_ + ": Trigger for stamp " + std::to_string(trigger_stamp) + " not found on the Metadata Buffer");
        return NULL;
    }

    window->setRefMeta(trigger_meta);

    typename CircularBuffer<T>::iterator startSample;
    typename CircularBuffer<T>::iterator endSample;
    auto triggerSample = trigger_meta->getRelatedSample();

    switch (dataPushMode_)
    {
    case DataPushMode::DISJUNCT_MULTI_SAMPLES:
    {
        auto pre_trigger_samples = static_cast<std::size_t>(pre_trigger_time_ / sample_to_sample_distance_nano_);
        auto post_trigger_samples = static_cast<std::size_t>(post_trigger_time_ / sample_to_sample_distance_nano_);
        startSample = circularBuffer_.getReadIterator(triggerSample.index() - pre_trigger_samples);
        endSample = circularBuffer_.getReadIterator(triggerSample.index() + post_trigger_samples - 1);
        auto lastPushed = circularBuffer_.getNumLastPushed();
        if (lastPushed < pre_trigger_samples + post_trigger_samples)
        {
            LOG_ERROR("Not enough samples pushed for DISJUNCT_MULTI_SAMPLES trigger mode. Skipping this event.");
            return nullptr;
        }
        auto writeHead = circularBuffer_.getCopyOfWriteIterator();
        auto start = circularBuffer_.getReadIterator(writeHead.index() - lastPushed);
        bool allSamplesInLastPush = startSample.isBetween(start, writeHead);
        if (!allSamplesInLastPush)
        {
            LOG_ERROR("Not enough samples pushed for DISJUNCT_MULTI_SAMPLES trigger mode. "
                "StartSample is not between the expected start and the write head. Skipping this event.");
        }
        
        break;
    }
    case DataPushMode::DISJUNCT_SINGLE_SAMPLES:
    {
        auto triggerStamp = trigger_meta->getRelatedSampleStamp();
        auto dataStartTimestamp = triggerStamp - static_cast<int64_t>(pre_trigger_time_);
        auto dataEndTimestamp = triggerStamp + static_cast<int64_t>(post_trigger_time_);
        auto startMeta = metadata_buffer_.findMetaWithDataSampleOlderThan(trigger_meta, dataStartTimestamp);
        auto endMeta = metadata_buffer_.findMetaWithDataSampleNewerThan(trigger_meta, dataEndTimestamp);
        
        if (startMeta == nullptr)
        {
            LOG_ERROR("Could not find start meta for triggered mode with DISJUNCT_SINGLE_SAMPLES.");
            return nullptr;
        }

        if(endMeta == nullptr)
        {
            // This trigger window cannot be finished yet, since not all post trigger
            // samples were added yet. It will be finished in a later iteration.
            window->prefillMetadata(trigger_meta);
            return nullptr;
        }

        auto startIndex = startMeta->getRelatedSample().index();
        if (startMeta->getRelatedSampleStamp() < dataStartTimestamp)
        {
            startIndex++;
        }
        auto endIndex = endMeta->getRelatedSample().index();
        if (endMeta->getRelatedSampleStamp() > dataEndTimestamp)
        {
            endIndex--;
        }
        startSample = circularBuffer_.getReadIterator(startIndex);
        endSample = circularBuffer_.getReadIterator(endIndex);
        break;
    }
    case DataPushMode::CONTINOUS_MULTI_SAMPLES:
    {
        auto pre_trigger_samples = static_cast<std::size_t>(pre_trigger_time_ / sample_to_sample_distance_nano_);
        auto post_trigger_samples = static_cast<std::size_t>(post_trigger_time_ / sample_to_sample_distance_nano_);
        startSample = circularBuffer_.getReadIterator(triggerSample.index() - pre_trigger_samples);
        endSample = circularBuffer_.getReadIterator(triggerSample.index() + post_trigger_samples - 1);
        
        bool allPostSamplesAvailable = !circularBuffer_.getCopyOfWriteIterator().isBetween(triggerSample, endSample);
        if (!allPostSamplesAvailable)
        {
            // This trigger window cannot be finished yet, since not all post trigger
            // samples were added yet. It will be finished in a later iteration.
            window->prefillMetadata(trigger_meta);
            return nullptr;
        }
        break;
    }
    default:
        throw RUNTIME_ERROR("Unsupported DataPushMode");
    }

    // In case the post trigger samples were not available the checkpoint was calculated within
    // prefillMetadata. Now check that at this point we didn't overwrite the start point
    // (complete buffer) already.
    auto validationCheckpointAtStart = window->getWindowValidationCheckpoint();
    if (circularBuffer_.n_data_pushed_total() >= validationCheckpointAtStart)
    {
        log_error_if(ClientNotificationType::TRIGGERED, "TRIGGERED - Data overwritten before the window was completed.");
        window->reset();
        return nullptr;
    }

    window->updateDataRange(startSample, endSample);

    window->updateMetaWindowAccordingToSampleWindow(false);

    return window;
}

template<typename T>
CircularWindowIterator<T>* CircularBufferManagerBase<T>::updateFullSequenceWindowIter(
        int64_t last_sequence_marker_stamp)
{
    log_trace_if (ClientNotificationType::FULL_SEQUENCE, "-------------- updateFullSequenceWindowIter start for signal " + signal_name_ + " last_sequence_marker_stamp: " + std::to_string(last_sequence_marker_stamp));

    CircularWindowIterator<T>* window = (*currentFullSequenceWindow_);
    auto startMeta = window->getMetaStart();
    if(startMeta != nullptr && metadata_buffer_.isCycledOut(startMeta))
    {
        window->reset();
        LOG_ERROR(signal_name_ + ": Error: SEQ_START event cycled out of the meta-buffer before a SEQ_END could be detected. Your streaming buffer might be too small.");
        return NULL;
    }

    std::shared_ptr<SampleMetadata<T>> sequence_marker = metadata_buffer_.getNextSequenceMarker(last_processed_sequence_marker_);
    if(last_processed_sequence_marker_)
        log_trace_if (ClientNotificationType::FULL_SEQUENCE, "FULL_SEQ - getting next seq. marker for " + std::to_string(last_processed_sequence_marker_->getTimingContextStamp()));
    if(!sequence_marker)
    {
        std::ostringstream stream;
        stream << metadata_buffer_;
        log_trace_if (ClientNotificationType::FULL_SEQUENCE, "FULL_SEQ - Metabuffer: \n" + stream.str());
        log_trace_if (ClientNotificationType::FULL_SEQUENCE, "FULL_SEQ - no sequence marker found .. leaving");
        return NULL;
    }

    // We only should move the full_seq window till last_sequence_marker_stamp (other buffers might be slower, and we want to move simultaniously)
    if(sequence_marker->getTimingContextStamp() > last_sequence_marker_stamp)
    {
        log_trace_if (ClientNotificationType::FULL_SEQUENCE, "FULL_SEQ - next sequence marker stamp '" + std::to_string(sequence_marker->getTimingContextStamp()) + "' too new. We only should move till last_sequence_marker_stamp: '" + std::to_string(last_sequence_marker_stamp) + "' .. leaving");
        return NULL;
    }

    // Ok, we are going to process this one
    last_processed_sequence_marker_ = sequence_marker;

    auto window_state = window->getWindowState();
    switch (window_state)
    {
    case CircularWindowIterator<T>::State::UNINITIALIZED:
    {
        // We need to start a new full_seq window
        // Ignore seq_end triggers
        if(DeviceDataBufferBase::isSequenceStartEvent(sequence_marker->getTimingContext()->getEventNumber()))
        {
            log_trace_if (ClientNotificationType::FULL_SEQUENCE, "FULL_SEQ - setting only meta start .. leaving");
            window->prefillMetadata(sequence_marker);
        }
        else
        {
            log_trace_if (ClientNotificationType::FULL_SEQUENCE, "FULL_SEQ - found gap start without seq-start .. leaving");
        }
        return NULL;
    }
    case CircularWindowIterator<T>::State::READY:
    {
        log_error_if(ClientNotificationType::FULL_SEQUENCE, "FULL_SEQ - window is already ready .. leaving");
        return NULL;
    }
    case CircularWindowIterator<T>::State::UNDER_CONSTRUCTION:
    {
        // Continue to finish the window.
        break;
    }
    default:
        log_error_if(ClientNotificationType::FULL_SEQUENCE, "FULL_SEQ - unknown CircularWindowIterator<T>::State");
        return NULL;
    }

    // If we arrive here, than we want to fill data start and end.
    if (startMeta == nullptr)
    {
        // Sanity check, should never happen because of previous checks.
        throw RUNTIME_ERROR("Start metadata pointer null when it shouldn't be.");
    }

    auto sequence_marker_event = sequence_marker->getTimingContext()->getEventNumber();
    std::size_t data_end_index;
    
    // We need to correctly mark the ending index of the data belonging to this sequence.
    // In case of very low sample rates it might happen that the event which started the sequence
    // points to the same data sample as the event that ended it. In that case we include the single
    // sample in that window. TODO: This case needs a unit test.
    if (startMeta->getRelatedSample() == sequence_marker->getRelatedSample())
    {
        data_end_index = sequence_marker->getRelatedSample().index();
    }
    // When the sequence window is terminated with the next sequence start, the last data to be
    // included is one sample before the new sequence start event.
    else if (DeviceDataBufferBase::isSequenceStartEvent(sequence_marker_event))
    {
        data_end_index = sequence_marker->getRelatedSample().prev_index();
    }
    // When the sequence window is terminated with the sequence end event, the last data to be
    // included contains the sample corresponding to the event that ended the sequence.
    else if (DeviceDataBufferBase::isSequenceEndEvent(sequence_marker_event))
    {
        data_end_index = sequence_marker->getRelatedSample().index();
    }
    else
    {
        throw RUNTIME_ERROR("Sequence marker does not belong to any of the expected events.");
    }

    // On sequence start the checkpoint was calculated within prefillMetadata. Now check that at
    // sequence end we didn't overwrite the start point (complete buffer) already.
    auto validationCheckpointAtStart = window->getWindowValidationCheckpoint();
    if (circularBuffer_.n_data_pushed_total() >= validationCheckpointAtStart)
    {
        log_error_if(ClientNotificationType::FULL_SEQUENCE, "FULL_SEQ - Data overwritten before the window was completed.");
        window->reset();
        return NULL;
    }

    window->updateDataRange(
        circularBuffer_.getReadIterator(startMeta->getRelatedSample().index()),
        circularBuffer_.getReadIterator(data_end_index));

    if(is_log_enabled_for(ClientNotificationType::FULL_SEQUENCE, LogLevel::TRACE))
    {
        std::ostringstream stream;
        stream << *window;
        LOG_TRACE ("FULL_SEQ - found a new window: " + stream.str());
    }

    // Switch to next window
    currentFullSequenceWindow_++;
    if (currentFullSequenceWindow_ == readDataWindowColFullSequence_.end())
    {
        currentFullSequenceWindow_ = readDataWindowColFullSequence_.begin();
    }

    auto new_window = *currentFullSequenceWindow_;
    new_window->reset();
    // If this window was ended by seq_start, that event will as well be the beginning of the next window
    if(DeviceDataBufferBase::isSequenceStartEvent(sequence_marker->getTimingContext()->getEventNumber()))
    {
        log_trace_if (ClientNotificationType::FULL_SEQUENCE, "FULL_SEQ - setting meta_start of next window");
        new_window->prefillMetadata(sequence_marker);
    }

    return window;
}

template<typename T>
CircularWindowIterator<T>* CircularBufferManagerBase<T>::updateStreamingWindowIter(
        MaxClientUpdateFrequencyType update_frequency_hz,
        int64_t                      last_sample_stamp,
        std::size_t                  n_samples)
{
    log_trace_if (ClientNotificationType::STREAMING, update_frequency_hz, "-------------- updateStreamingWindowIter start for signal " + signal_name_);

    CircularWindowIterator<T>* window_iter = readDataWindowColStreaming_[update_frequency_hz];

    if(n_samples == 0)
    {
        log_trace_if (ClientNotificationType::STREAMING, update_frequency_hz, "STREAM - Window empty .. leaving");
        return NULL;
    }

    std::size_t n_samples_till_end = 0;

    // Find the data_end which matches to 'last_sample_stamp'
    typename CircularBuffer<T>::iterator data_end;
    if(dataPushMode_ == DataPushMode::DISJUNCT_SINGLE_SAMPLES || last_sample_stamp == 0)
    {
        log_trace_if (ClientNotificationType::STREAMING, update_frequency_hz, "STREAM - DISJUNCT- Setting data_end to latest sample");
        data_end = circularBuffer_.getLatest();
        n_samples_till_end = n_samples;
    }
    else
    {
        if(window_iter->not_used_yet())
		{
            log_trace_if (ClientNotificationType::STREAMING, update_frequency_hz, "STREAM - CONTINOUS - Setting data_end to latest sample");
			data_end = circularBuffer_.getLatest();
		}
        else
        {
            // Walk through the new samples (starting with the oldest new sample) unit we
            // - find a sample a stamp newer than 'last_sample_stamp'
            // - or find the write iterator

            data_end = circularBuffer_.getReadIterator(window_iter->getDataEnd().index()); // points now to last sample of previous window
            int64_t last_stamp_previous_window = window_iter->getTimeStampOfLastSample();

            int64_t data_end_stamp = 0;
            for (std::size_t i = 0; i<n_samples;i++)
            {
                data_end_stamp = last_stamp_previous_window + floor(sample_to_sample_distance_nano_ * i);
                if(n_samples_till_end > n_samples)
                    break;
                if(data_end_stamp > last_sample_stamp)
                    break;
                if(circularBuffer_.isWriteIter(data_end))
                {
                    LOG_ERROR(signal_name_ + " The " + window_iter->getName() + " Reached write iterator while searching for data_end. That is not supposed to happen!");
                    --data_end;
                    break;
                }

                ++data_end;
                n_samples_till_end++;
            }

            if(n_samples_till_end < n_samples)
                LOG_ERROR(signal_name_ + " Not possibleto move the iterator of " + window_iter->getName() + " by " + std::to_string(n_samples) + "samples. Only " + std::to_string(n_samples_till_end) + " new samples are available.");

       }
    }

    auto valid = window_iter->moveDataWindowTo(n_samples_till_end, data_end);

    // We found ANY meta item at all since startup ?
    if(most_recent_meta_)
    {
        log_trace_if (ClientNotificationType::STREAMING, update_frequency_hz, "STREAM - updating meta-window according to sample window");
        window_iter->updateMetaWindowAccordingToSampleWindow(true);
    }

    if(!valid)
    {
        LOG_ERROR(signal_name_ + " The " + window_iter->getName() + " read iterator got invalid before finding the next data chunk ... conflict with write iterator");
        return NULL;
    }

    // as soon as the window can be moved without write pointer conflicts, the startup phase is considered to be finished
    window_iter->startupFinished();

    log_trace_if (ClientNotificationType::STREAMING, update_frequency_hz, "STREAM - moving window finsihed");
    return window_iter;
}

template<typename T>
double CircularBufferManagerBase<T>::getMatchedTriggersPercentage()
{
    // For triggers which failed to match, a 0.0 will be inserted.
    // So if this container is empty, no matching was done at all so far
    if(n_matched_triggers_.size() == 0)
        return 100.0;

    return n_matched_triggers_.get_average() * 100.;
}

template<typename T>
void CircularBufferManagerBase<T>::moveTriggeredWindow(int64_t triggerStamp, EventNumber event)
{
    if(is_log_enabled_for(clientNotificationType_, LogLevel::TRACE))
    {
        log_trace_if(clientNotificationType_, "requesting window for trigger_stamp: " + std::to_string(triggerStamp));
    }
    CircularWindowIterator<T>* window = updateTriggerWindowIter(triggerStamp, event);
    if (window == nullptr)
    {
        return;
    }

    if(is_log_enabled_for(clientNotificationType_, LogLevel::TRACE))
    {
        std::ostringstream stream;
        stream << *window;
        log_trace_if (clientNotificationType_, "Found a new window:\n " + stream.str());
    }

    std::shared_ptr<const TimingContext> timing_context;
    if(window->getRefMeta() && window->getRefMeta()->hasTimingContext())
    {
        timing_context =  window->getRefMeta()->getTimingContext();
    }

    CircularWindowIterator<T> copiedWindow = *window;
    // When the notification window is copied, reset the cached window to prepare it for the next sequences/trigger events.
    window->reset();

    // Add a new notification payload to the queue
    uint32_t id = dataReadyQueue_.push(getSignalName(),
                                        timing_context,
                                        clientNotificationType_,
                                        0,
                                        std::move(copiedWindow));

    // Notify clients that there is new data on the queue
    dataReadyCallback_(id, this);
}

template<typename T>
void CircularBufferManagerBase<T>::moveFullSequenceWindow(int64_t timestamp)
{
    CircularWindowIterator<T>* window = updateFullSequenceWindowIter(timestamp);
    if (window == nullptr)
    {
        return;
    }

    if(is_log_enabled_for(clientNotificationType_, LogLevel::TRACE))
    {
        std::ostringstream stream;
        stream << *window;
        log_trace_if (clientNotificationType_, "Found a new window:\n " + stream.str());
    }

    std::shared_ptr<const TimingContext> timing_context;
    // note that the event-stamps will almost never be exactly on a sample. Only in that case, it may happen that we count samples twice (which anyhow is not a problem)
    // Use START_SEQ trigger as notification context
    auto metaStart = window->getMetaStart();
    if(metaStart != nullptr)
    {
        timing_context = metaStart->getTimingContext();
    }
    else
    {
        std::ostringstream message;
        message << "Logical Error: No start_sequence context found for FULL_SEQ window: " << std::endl;
        message << *window;
        LOG_ERROR(message.str());
        window->resetRefMeta();
    }

    // This possibly will overwrite the 'timing_context' on the first sample (e.g. in DISJUNCT_SINGLE_SAMPLES)
    //  So it's only done after we got seq_start_context
    window->updateMetaWindowAccordingToSampleWindow(true);

    CircularWindowIterator<T> copiedWindow = *window;
    // When the notification window is copied, reset the cached window to prepare it for the next sequences/trigger events.
    window->reset();

    // Add a new notification payload to the queue
    uint32_t id = dataReadyQueue_.push(getSignalName(),
                                        timing_context,
                                        clientNotificationType_,
                                        0,
                                        std::move(copiedWindow));

    // Notify clients that there is new data on the queue
    dataReadyCallback_(id, this);
}

template<typename T>
void CircularBufferManagerBase<T>::moveStreamingWindow(int64_t lastSampleStamp, std::size_t numSamples, MaxClientUpdateFrequencyType maxClientUpdateFreq)
{
    CircularWindowIterator<T>* window = updateStreamingWindowIter(maxClientUpdateFreq,
                                                                lastSampleStamp,
                                                                numSamples);

    if (window == nullptr)
    {
        std::ostringstream message;
        message << "Failed to move streaming window for signal " << getSignalName();
        LOG_ERROR(message.str());
        return;
    }

    if(is_log_enabled_for(ClientNotificationType::STREAMING, maxClientUpdateFreq, LogLevel::TRACE))
    {
        std::ostringstream stream;
        stream << *window;
        log_trace_if (ClientNotificationType::STREAMING, maxClientUpdateFreq, "Found a new window:\n " + stream.str());
    }

    std::shared_ptr<const TimingContext> timing_context;
    if(window->getRefMeta() && window->getRefMeta()->hasTimingContext())
    {
        timing_context = window->getRefMeta()->getTimingContext();
    }

    CircularWindowIterator<T> copiedWindow = *window;

    // Add a new notification payload to the queue
    uint32_t id = dataReadyQueue_.push(getSignalName(),
                                        timing_context,
                                        clientNotificationType_,
                                        maxClientUpdateFreq,
                                        std::move(copiedWindow));

    // Notify clients that there is new data on the queue
    dataReadyCallback_(id, this);
}

} /* namespace cabad */


/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/SampleMetaData.h>
#include <cabad/CircularWindowIterator.h>
#include <cabad/Definitions.h>
#include <cabad/Util.h>
#include <cabad/DataReadyQueue.h>
#include <cabad/ContextTracker.h>
#include <cabad/DeviceDataBufferBase.h>

#include <map>
#include <deque>

namespace cabad
{

/* 
   Manages all Iterators and Meta-Data of a single CircularBuffer.
   The user of the library inheriting from CircularBufferManagerBase must make sure that the
   CircularBuffer functions are only used from the single writer thread (the exception being
   the copy function which may be used from reader threads). The CircularBuffer does not include
   any mutex synchronization.
*/ 
template<typename T>
class CircularBufferManagerBase
{

public:

    virtual ~CircularBufferManagerBase();

    double getSampleToSampleDistance() const
    {
        return sample_to_sample_distance_;
    }

    int64_t getSampleToSampleDistanceNano() const
    {
        return sample_to_sample_distance_nano_;
    }

    const std::string& getSignalName() const
    {
        return signal_name_;
    }

    void setTriggerSamples(std::size_t pre_trigger_samples, std::size_t post_trigger_samples);

    void setTriggerTime(double pre_trigger_time, double post_trigger_time);

    void setTriggerMatchingTolerance(int64_t triggerMatchingTolerance_ns)
    {
        triggerMatchingTolerance_ns_ = triggerMatchingTolerance_ns;
    }

    float getSampleRate() const
    {
        return sample_rate_hz_;
    }

    double getMatchedTriggersPercentage();

    LogLevel getLogLevelStreaming(MaxClientUpdateFrequencyType maxClientUpdateFrequency)
    {
        if(log_levels_per_type_[ClientNotificationType::STREAMING] < log_levels_per_maxClientUpdateFrequency_[maxClientUpdateFrequency])
            return log_levels_per_type_[ClientNotificationType::STREAMING];
        else
            return log_levels_per_maxClientUpdateFrequency_[maxClientUpdateFrequency];
    }

    LogLevel getLogLevelTriggered()
    {
        return log_levels_per_type_[ClientNotificationType::TRIGGERED];
    }

    LogLevel getLogLevelFullSequence()
    {
        return log_levels_per_type_[ClientNotificationType::FULL_SEQUENCE];
    }

    void setLogLevelTriggered(LogLevel log_level)
    {
        log_levels_per_type_[ClientNotificationType::TRIGGERED] = log_level;
    }

    void setLogLevelFullSequence(LogLevel log_level)
    {
        log_levels_per_type_[ClientNotificationType::FULL_SEQUENCE] = log_level;
    }

    void setLogLevelStreaming(LogLevel log_level, MaxClientUpdateFrequencyType maxClientUpdateFrequency)
    {
        log_levels_per_type_[ClientNotificationType::STREAMING] = log_level;
        log_levels_per_maxClientUpdateFrequency_[maxClientUpdateFrequency] = log_level;
    }

    void log_error_if(ClientNotificationType type, const std::string& message);
    void log_trace_if(ClientNotificationType type, const std::string& message);
    void log_trace_if(ClientNotificationType type, MaxClientUpdateFrequencyType maxClientUpdateFrequency, const std::string& message);
    bool is_log_enabled_for(ClientNotificationType type, LogLevel log_level);
    bool is_log_enabled_for(ClientNotificationType type, MaxClientUpdateFrequencyType maxClientUpdateFrequency, LogLevel log_level);

    bool getDataByID(uint32_t id, ClientNotificationData<T>& data) const;

    uint64_t n_data_pushed_total() const;

    const MetaDataBuffer<T>& getMetaBuffer() const;

protected:

    /*
     * CircularBufferManagerBase:
     * @signal_name: Name of the signal, stored in the buffer
     * @sample_rate_hz: Sample rate of the signal. Use -1 if the signal is not sampled with constant rate
     * @maxClientUpdateFrequencies: Frequencies used to notify subscribed clients
     *                              The BufferManager will try to notify in all passed frequencies,
     *                              though dependant on the incoming sample-rate, the actualupdate frequency can be lower
     * @contextTracker: Each BufferManager requires a context tracker to lookup and allign TimingEvents to the data
     * @deviceDataBuffer: Required to manage reference Triggers and Event-Signatures which might differ per device
     * @clientNotificationType: Masks the supported modes in which clients should be notified
     * @dataPushMode: The way data will be pushed into the buffer. (single samples / arrays / continous / disjunct )
     * @circularBufferSize: Size of the circular buffer.
     * @metaBufferSize: Size of the metadata buffer.
     * @dataQueueSize: Size of the data queue.
     * @dataReadyCallback: Function called when there is new data available for the client.
     *
     * Constructor
     */
    CircularBufferManagerBase(std::string signal_name,
                              float sample_rate_hz,
                              std::vector<MaxClientUpdateFrequencyType> maxClientUpdateFrequencies,
                              ContextTracker* contextTracker,
                              DeviceDataBufferBase* deviceDataBuffer,
                              ClientNotificationType clientNotificationType,
                              DataPushMode dataPushMode,
                              std::size_t circularBufferSize,
                              std::size_t metaBufferSize,
                              std::size_t dataQueueSize,
                              std::function<void(uint32_t data_id, CircularBufferManagerBase*)> dataReadyCallback);

    // Retunrs a pointer to the window if it was sucesfully moved, NULL otherwise
    // notification_context will be filled within the method
    // last_sample pointer to last sample which is to notify
    // last_sample_stamp: The new window only should have samples with stamps <= last_sample_stamp
    // n_samples_max maximum number of samples to notify (if there are more samples until the previous windows, they are dropped)
    CircularWindowIterator<T>* updateStreamingWindowIter(
                              MaxClientUpdateFrequencyType update_frequency_hz,
                              int64_t                      last_sample_stamp,
                              std::size_t                  n_samples_max);

    // Retunrs true if window was sucessfully moved
    // notification_context will be filled within the method
    // @last_sequence_marker_stamp: Only move the full_Sequence window till here (in order to sync buffer movement between different buffers)
    CircularWindowIterator<T>* updateFullSequenceWindowIter(int64_t last_sequence_marker_stamp);

    // Retunrs true if window was sucessfully moved
    // notification_context will be filled within the method
    CircularWindowIterator<T>* updateTriggerWindowIter(int64_t trigger_stamp, EventNumber eventNumber);

    void pushBackDataFinished();

    // Very basic function to add any kind of metadata
    int addMetaData(const std::shared_ptr<SampleMetadata<T>>& meta);

    std::shared_ptr<SampleMetadata<T>> addMeta(int64_t  meta_timestamp,
                                            uint64_t related_sample_absolute_offset,
                                            bool     isTriggerMeta,
                                            std::shared_ptr<MetaPayload> payload);

    // Add metadata which is correlated to a WR-Stamp
    // The related sample is just stamped with some WR Timestamp, not related to any WR-Event
    std::shared_ptr<SampleMetadata<T>> addWrStampMetaData(int64_t  wr_stamp,
                                                       uint64_t related_sample_absolute_offset,
                                                       std::shared_ptr<MetaPayload> payload = nullptr);

    // Add metadata which is correlated to WR-Event and its Hardware Trigger
    // In this case, the related WRContext will be searched and added to the related sample.
    // The old, unprecise trigger stamp will be replaced by the according WR-STamp
    // The delay between trigger stmap and WR stamp will be stored, and added to all other stamped non-trigger samples to correctly allign them in the data stream
    // The matching tolerance to match a trigger stamp with a WRStamp can be controlled via "setTriggerMatchingTolerance"
    std::shared_ptr<SampleMetadata<T>> addTriggerMetaData(int64_t  hardware_trigger_stamp,
                                                       uint64_t related_sample_absolute_offset,
                                                       std::shared_ptr<MetaPayload> payload = nullptr);

    std::shared_ptr<SampleMetadata<T>> addTriggerMetaData(const std::shared_ptr<const TimingContext>& context,
                                                       uint64_t related_sample_absolute_offset,
                                                       int64_t related_sample_stamp,
                                                       std::shared_ptr<MetaPayload> payload = nullptr);

    int findSampleWindowMarkers (const typename CircularBuffer<T>::iterator& firstSample,
                                 const typename CircularBuffer<T>::iterator& lastSample,
                                 int64_t& firstSampleStamp,
                                 int64_t& firstContextSearchStamp,
                                 int64_t& lastContextSearchStamp,
                                 int64_t& first_sample_abs_offset);

    // Adds all MultiplexingContexts to the data stream which are found between the time of "firstSample" and the time of "lastSample"
    // This method requires to have a least one call of "addWrStampMetaData", so that a timebase can be build and the contexts can be alligned to the correct samples
    int addMultiplexingContextDataForRange(const typename CircularBuffer<T>::iterator& firstSample,
                                           const typename CircularBuffer<T>::iterator& lastSample);

    void moveTriggeredWindow(int64_t timestamp, EventNumber event);
    void moveFullSequenceWindow(int64_t timestamp);
    void moveStreamingWindow(int64_t lastSampleStamp, std::size_t numSamples, MaxClientUpdateFrequencyType maxClientUpdateFreq);

    CircularBuffer<T> circularBuffer_;

    // Each metaData element points to a specific sample, same like tags in gnuradio
    MetaDataBuffer<T> metadata_buffer_;

    // Distance between two samples in seconds / nanoseconds
    double sample_to_sample_distance_;
    double sample_to_sample_distance_nano_; // as well double required .. can be less than 1ns for samp_rate > 1GS/S

    ContextTracker* contextTracker_;

    // Name of this Signal
    std::string signal_name_;

    // Pointer to the related DeviceDataBuffer
    DeviceDataBufferBase* deviceDataBuffer_;

    // All streaming Data Windows which can be read out by properties (stored per update frequency)
    std::map<MaxClientUpdateFrequencyType, CircularWindowIterator<T>*> readDataWindowColStreaming_;

    DataReadyQueue<T> dataReadyQueue_;

    std::function<void(uint32_t data_id, CircularBufferManagerBase*)> dataReadyCallback_;

    // stamps when the last streaming update happened for each update frequency
    std::map<MaxClientUpdateFrequencyType, uint64_t> lastStreamingUpdate_;

    // The FULL_SEQUENCE window which can be read out by properties
    // More than one window is required if there is more than one full sequences in one FESA Sink update (e.g. 1Hz Sink)
    std::vector<CircularWindowIterator<T>*> readDataWindowColFullSequence_;
    std::size_t numberOfFullSequenceWindows_;
    typename std::vector<CircularWindowIterator<T>*>::iterator currentFullSequenceWindow_;

    // Latest meta which was added to this circular buffer
    std::shared_ptr<SampleMetadata<T>> most_recent_meta_;

    // Latest meta which was added to this circular buffer
    std::shared_ptr<SampleMetadata<T>> most_recent_timing_event_meta_;

    // MetaData of the most recent seq start and end events
    std::shared_ptr<SampleMetadata<T>> most_recent_sequence_marker_;

    // We already updated the sfull-sequence window till here
    std::shared_ptr<SampleMetadata<T>> last_processed_sequence_marker_;

    // MetaData of the most recent trigger data we received
    std::shared_ptr<SampleMetadata<T>> most_recent_trigger_meta_;

    // MetaDataIter until the method addMultiplexingContextDataForRange already added multiplexing context info
    std::shared_ptr<SampleMetadata<T>> mux_contexts_info_added_till_here_;

    int64_t last_context_search_end_stamp_;

    // All Triggered Data Windows, one per eventNumber
    std::map<EventNumber, CircularWindowIterator<T>*> readDataWindowColTriggered_;

    // Time before and after the trigger event which is provided in the trigger window.
    double pre_trigger_time_;
    double post_trigger_time_;

    // Bit enum to remember which notification types this buffer supports
    ClientNotificationType clientNotificationType_;

    bool new_sequence_marker_detected_;
    bool new_trigger_detected_;

    DataPushMode dataPushMode_;

    // The maximum tolerance which will be accepted to match incoming trigger stamps with their according WRContext Timestamps
    int64_t triggerMatchingTolerance_ns_;

    float sample_rate_hz_;

    // For diagnostics
    std::map<ClientNotificationType, LogLevel>       log_levels_per_type_;
    std::map<MaxClientUpdateFrequencyType, LogLevel> log_levels_per_maxClientUpdateFrequency_;

    // Will have a 1 for each matched trigger, a 0 for a missmatch
    average_filter<double> n_matched_triggers_;

    // offset between the utc stamp on incoming data and the real WR time (positive offset --> matched WR stamp is older than stamp on sample)
    // Once a trigger stamp was matched, that delay will be used to correctly allgn all non-trigger stamps
    average_filter<int64_t> alligment_offset_data_timestamp_WRStamp_;
    std::shared_ptr<const TimingContext> last_matching_trigger_context_;
};

} /* namespace cabad */

#include <cabad/CircularBufferManagerBaseImpl.h>
/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/TimingContext.h>
#include <cabad/DeviceDataBufferBase.h>

#include <map>
#include <deque>
#include <memory>
#include <mutex>

namespace cabad
{

/*!
 * \brief A class holding last N timing contexts
 *
 */
class ContextTracker
{
public:

    ContextTracker(std::size_t buffer_capacity);

    void addContext(const std::shared_ptr<const TimingContext>& context);

    std::deque < std::shared_ptr<const TimingContext> > getContextRange(int64_t rangeStartStamp,  int64_t rangeEndStamp);

    std::shared_ptr<const TimingContext> getNextTriggerContext(int64_t searchStartStamp, const DeviceDataBufferBase& deviceDataBuffer);

    std::shared_ptr<const TimingContext> getContextByID(int64_t id);

    void clear()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        buffer_.clear();
    }

    std::size_t max_size()
    {
        return max_size_;
    }

private:

    std::mutex mutex_;

    std::size_t max_size_;

    // Need to use a std-container since we store smart pointers
    std::deque< std::shared_ptr<const TimingContext> > buffer_;
};

} /* namespace cabad */

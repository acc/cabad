/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <cabad/Util.h>
#include <cabad/Definitions.h>

#include <string>
#include <ctime>


// Black magic to get a string out of a macro
#define STRINGIFY2(X) #X
#define STRINGIFY(X) STRINGIFY2(X)

namespace cabad
{

// Init global logger with "empty"
void empty(LogLevel, const std::string&, int, const std::string&, const std::string&)
{
    // nothing
}

std::string getVersion()
{
    return STRINGIFY(CABAD_VERSION);
}

uint64_t get_timestamp_nano_utc_internal()
{
    timespec start_time;
    clock_gettime(CLOCK_REALTIME, &start_time);
    return (start_time.tv_sec * 1000000000) + (start_time.tv_nsec);
}

uint64_t get_timestamp_nano_utc()
{
    return globalGetTimestampFunction();
}

void setLogFunction(LogFunction log_function)
{
    globalLogFunction = log_function;
}

void setTimeStampFunction(GetTimestampFunction function)
{
    globalGetTimestampFunction = function;
}

//Set defaults for global functions
LogFunction globalLogFunction = empty;
GetTimestampFunction globalGetTimestampFunction = get_timestamp_nano_utc_internal;

} // end namespace

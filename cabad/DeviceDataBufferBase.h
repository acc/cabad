/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/Definitions.h>

#include <string>
#include <vector>
#include <map>

namespace cabad
{

struct TriggerEvent
{
    // Name of the Event. E.g. CMD_SEQ_START
    std::string name;

    // ID of the event. E.g.: 257
    EventNumber id;

    // True if this event should be used be this DeviceDataBuffer
    bool enabled;
};

class DeviceDataBufferBase
{
public:

    /*
     * Map of Event ID's and a struct holding related event-information
     */
    typedef std::map <EventNumber, TriggerEvent> TriggerEventsByEventIDType;


    /*
     * DeviceDataBufferBase:
     * @fesaConcreteEventNames : List of event names, usually comming from the instance file.
     *                           Format: "SchedulingUnitName::EventName#ID"
     *                           E.g.: "MySchedulingUnit::CMD_SEQ_START#257"
     * @fesaDeviceName         : Name of the related fesa device. E.g. via 'device->getName()'
     * @RefMetaTriggerEvent : Event which should be used as reference Trigger. Format: EventName#ID. E.g: CMD_SEQ_START#257
     * @RefMetaTriggerEvent_fallback1: If @RefMetaTriggerEvent is not played, this event will be used as reference Trigger
     * @RefMetaTriggerEvent_fallback2: If @RefMetaTriggerEvent_fallback2 is not played, this event will be used as reference Trigger
     */
    DeviceDataBufferBase(const std::vector<std::string>& fesaConcreteEventNames,
                         const std::string& fesaDeviceName,
                         const std::string& RefMetaTriggerEvent = "",
                         const std::string& RefMetaTriggerEvent_fallback1 = "",
                         const std::string& RefMetaTriggerEvent_fallback2 = "");

    /*
     * getName:
     *
     * Return value: The device Name of this DeviceDataBuffer
     */
    std::string getName() const
    {
        return fesaDeviceName_;
    }

    /*
     * eventID2eventName:
     * @eventID: E.g. 257
     *
     * Return value: The name of the event. E.g: CMD_SEQ_START
     * Throws if not found
     */
    std::string eventID2eventName(EventNumber eventID) const;

    /*
     * eventName2eventID:
     * @eventName: E.g. CMD_SEQ_START
     *
     * Return value: The id of the event. E.g: 257
     * Throws if not found
     */
    unsigned int eventName2eventID(const std::string& eventName) const;

    /*
     * isTriggerEvent:
     * @eventName: E.g. CMD_SEQ_START
     *
     * Return value: True if the event is found in the internal list of Trigger events
     */
    bool isTriggerEvent(const std::string& eventName) const;

    /*
     * isTriggerEvent:
     * @eventID: E.g. 257
     *
     * Return value: True if the event is found in the internal list of Trigger events
     */
    bool isTriggerEvent(EventNumber eventID) const;

    /*
     * isTriggerEventEnabled:
     * @eventID: E.g. 257
     *
     * Return value: True if the event is found in the internal list of Trigger events and is enabled
     * Throws if not found
     */
    bool isTriggerEventEnabled(EventNumber eventID) const;

    /*
     * setTriggerEventEnableState:
     * @eventID: E.g. 257
     * @enable_state: New enable state of the event
     *
     * Throws if not found
     */
    void setTriggerEventEnableState(EventNumber eventID, bool enable_state);

    /*
     * isSequenceStartEvent:
     * @eventID: E.g. 257
     *
     * Return value: true if @eventID == Events::getSequenceStartEvent()
     */
    static bool isSequenceStartEvent(EventNumber eventID);

    /*
     * isSequenceEndEvent:
     * @eventID: E.g. 255
     *
     * Return value: true if @eventID == Events::getSequenceEndEvent()
     */
    static bool isSequenceEndEvent(EventNumber eventID);

    /*
     * getTriggerEvents
     *
     * Returnjs th map of available trigger events and event specific information
     */
    TriggerEventsByEventIDType getTriggerEvents() const;

    /*
     * extractEventName:
     * @fesaConcreteEventName: A concrete EventName.
     *                         Format: "SchedulingUnitName::EventName#ID"
     *                         E.g.: "MySchedulingUnit::CMD_SEQ_START#257" or "CMD_SEQ_START#257"
     *
     * Return value: Only name of the event. E.g: CMD_SEQ_START
     * Throws if the format is bad
     */
    std::string extractEventName(const std::string& fesaConcreteEventName) const;

    /*
     * extractEventID:
     * @fesaConcreteEventName: A concrete EventName.
     *                         Format: "SchedulingUnitName::EventName#ID"
     *                         E.g.: "MySchedulingUnit::CMD_SEQ_START#257" or "CMD_SEQ_START#257"
     *
     * Return value: Only ID of the event. E.g: 257
     * Throws if the format is bad
     */
    unsigned int extractEventID(const std::string& fesaConcreteEventName) const;

    uint32_t getRefMetaTriggerEventID() const
    {
        return RefMetaTriggerEventID_;
    }

    uint32_t getRefMetaTriggerEventIDFallback1() const
    {
        return RefMetaTriggerEventID_fallback1_;
    }

    uint32_t getRefMetaTriggerEventIDFallback2() const
    {
        return RefMetaTriggerEventID_fallback2_;
    }

protected:

    /*
     * validateRefMetaTriggerEvent:
     * @fesaConcreteEventName: A concrete FEsa EventName.
     *                         Format: "SchedulingUnitName::EventName#ID"
     *                         E.g.: "MySchedulingUnit::CMD_SEQ_START#257" or "CMD_SEQ_START#257"
     *
     * Validates if the passed event name is available in the list of trigger-events, and the ID matches the name
     * Throws if not valid
     */
    void validateRefMetaTriggerEvent(std::string fesaConcreteEventName) const;

private:

    /*
     * List of events which can be used by this DeviceDataBuffer and it's related CircularBufferManagers
     */
    TriggerEventsByEventIDType triggerEvents_;

    /*
     * ID of the events which should be used as reference for all samples.
     * The fallbacks only will be used as referenceTrigger if the "RefMetaTriggerEvent" is not played or got invalid
     */
    uint32_t RefMetaTriggerEventID_;
    uint32_t RefMetaTriggerEventID_fallback1_;
    uint32_t RefMetaTriggerEventID_fallback2_;

    /*
     * Name of the related Fesa Device (Nomenklatur)
     */
    std::string fesaDeviceName_;
};

} // close namespace

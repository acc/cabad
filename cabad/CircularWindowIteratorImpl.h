/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <cabad/Util.h>

#include <cmath>
#include <sstream>

namespace cabad
{

template<typename T>
int CircularWindowIterator<T>::startup_finished_count_ = 0;

template<typename T>
CircularWindowIterator<T>::CircularWindowIterator(const std::string& name,
                                               CircularBuffer<T>* circularBuffer,
                                               MetaDataBuffer<T>* metadataBuffer,
                                               bool considerForStartupFinishedCount,
                                               DataPushMode dataPushMode,
                                               double sample_to_sample_distance):
                                                    window_state_(State::UNINITIALIZED),
                                                    circularBuffer_(circularBuffer),
                                                    metadataBuffer_(metadataBuffer),
                                                    startup_finished_(false),
                                                    dataPushMode_(dataPushMode),
                                                    name_(name),
                                                    sample_to_sample_distance_(sample_to_sample_distance),
                                                    stamp_last_sample_prev_(0),
                                                    windowValidationCheckpoint_(std::numeric_limits<uint64_t>::max())

{
    if(considerForStartupFinishedCount)
        startup_finished_count_ ++;
    else
        startup_finished_ = true;

    sample_to_sample_distance_nano_ = sample_to_sample_distance_ * 1000000000.;

    if (circularBuffer_ == nullptr)
    {
        throw RUNTIME_ERROR("Circularbuffer pointer must not be null.");
    }

    if (metadataBuffer_ == nullptr)
    {
        throw RUNTIME_ERROR("Metadatabuffer pointer must not be null.");
    }

    data_start_ = circularBuffer->pEnd();
    data_end_ = circularBuffer->pEnd();
}

template<typename T>
CircularWindowIterator<T>::CircularWindowIterator(MetaDataBuffer<T>* metadataBuffer)
    : window_state_(State::UNINITIALIZED),
      circularBuffer_(nullptr),
      metadataBuffer_(metadataBuffer),
      startup_finished_(false),
      dataPushMode_(DataPushMode::DISJUNCT_SINGLE_SAMPLES),
      name_(""),
      sample_to_sample_distance_(0.0),
      sample_to_sample_distance_nano_(0.0),
      stamp_last_sample_prev_(0),
      windowValidationCheckpoint_(std::numeric_limits<uint64_t>::max())
{

}

template<typename T>
std::size_t CircularWindowIterator<T>::windowSize() const
{
    // distance 5 to 9  is 4 --> 5 samples
    return data_start_.distance_to(data_end_) + 1;
}

template<typename T>
void CircularWindowIterator<T>::reset()
{
    ref_meta_.reset();
    data_start_ = circularBuffer_->pEnd();
    data_end_ = circularBuffer_->pEnd();
    window_state_ = State::UNINITIALIZED;
    windowValidationCheckpoint_ = std::numeric_limits<uint64_t>::max();
}

template<typename T>
std::string CircularWindowIterator<T>::getName() const
{
    return name_;
}

template<typename T>
void CircularWindowIterator<T>::startupFinished()
{
    if(!startup_finished_)
    {
        //std::cout << "CircularWindowIterator::startupFinished 1" << std::endl;
        startup_finished_count_ --;
        if(startup_finished_count_ == 0)
        {
            std::cout << "All Sequence Window Iterators of all Channels are online now" << std::endl;
            LOG_TRACE("All Sequence Window Iterators of all Channels are online now" );
        }
        startup_finished_ = true;
    }
}

template<typename T>
bool CircularWindowIterator<T>::moveDataWindowTo(std::size_t n_samples, const typename CircularBuffer<T>::iterator& data_end)
{
    if(n_samples == 0)
    {
        return true;
    }

    int32_t data_start_index = data_end.index() - (n_samples - 1);
    if(data_start_index < 0)
        data_start_index += circularBuffer_->size();

    auto data_start = circularBuffer_->getReadIterator(data_start_index);
    auto old_start = circularBuffer_->getReadIterator(data_start_.index());
    auto old_end = circularBuffer_->getReadIterator(data_end_.index());

    updateDataRange(data_start, data_end);

    // Now make sure the write iterator is not inside the new window
    if(isInWindow(circularBuffer_->getCopyOfWriteIterator()))
    {
        std::ostringstream stream;
        stream << "old_start : " <<  old_start.index() << std::endl;
        stream << "old_end   : " << old_end.index() << std::endl;
        stream << "new_start : " << data_start.index() << std::endl;
        stream << "new_end   : " << data_end.index() << std::endl;
        stream << "write iter: " << circularBuffer_->getCopyOfWriteIterator().index() << std::endl;
        LOG_ERROR("CircularWindowIterator::moveDataWindowTo - Fail, Window moved into write iterator. - n_samples: " + std::to_string(n_samples) + " " + stream.str());
        return false;
    }
    return true;
}

template<typename T>
bool CircularWindowIterator<T>::isInWindow(const typename CircularBuffer<T>::iterator& sample) const
{
    if(data_start_ == data_end_)
    	return sample == data_start_;

    return sample.isBetween(data_start_, data_end_);
}

template<typename T>
bool CircularWindowIterator<T>::isInWindow(const std::shared_ptr<const TimingContext>& timingContext) const
{
    for(const auto& iter : window_metadata_)
    {
        if(iter->hasTimingContext())
        {
            if( iter->getTimingContextStamp() == timingContext->getTimeStamp())
                return true;
        }
    }
    return false;
}

template<typename T>
int64_t CircularWindowIterator<T>::distanceRefMetaToWindow() const
{
    if( !ref_meta_)
        throw RUNTIME_ERROR("The CircularWindowIterator'" + name_ + "' does not have a ref_meta (yet)");
    if(isInWindow(ref_meta_->getRelatedSample()))
    {
        return data_start_.distance_to(ref_meta_->getRelatedSample()) * -1; // negative distance
    }
    else
    {
        return ref_meta_->getRelatedSample().distance_to(data_start_);
    }
}

template<typename T>
std::vector<float> CircularWindowIterator<T>::getTimeSinceRefMetaForEachSample (uint64_t foreign_ref_meta_stamp) const
{
    std::size_t window_size = windowSize();
    std::vector<float> timesSinceRefMeta(window_size);
    fillTimeSinceRefMetaForEachSample(&(timesSinceRefMeta[0]), window_size, foreign_ref_meta_stamp);
    return timesSinceRefMeta;
}

template<typename T>
void CircularWindowIterator<T>::fillTimeSinceRefMetaForEachSample(float*      timesSinceRefMeta,
                                                               std::size_t timesSinceRefMeta_size,
                                                               uint64_t    foreign_ref_meta_stamp) const
{
    uint64_t ref_meta_stamp = foreign_ref_meta_stamp;
    if( ref_meta_stamp == 0)
    {
        if(!ref_meta_)
            throw RUNTIME_ERROR("The CircularWindowIterator'" + name_ + "' does not have a ref_meta (yet)");
        ref_meta_stamp = ref_meta_->getTimingContext()->getTimeStamp();
    }

    if(windowSize() > timesSinceRefMeta_size)
        throw RUNTIME_ERROR("The passed C-Array is to short");

    //printMeta();
    if(dataPushMode_ == DataPushMode::DISJUNCT_SINGLE_SAMPLES)
    {
        auto sample_iter = circularBuffer_->getReadIterator(data_start_.index());
        std::size_t sample_count = 0;
        //std::cout << "windowSize(): " <<  windowSize()<< std::endl;
        while (true)
        {
            if (window_metadata_.empty())
                throw RUNTIME_ERROR("No meta-window defined");
           //std::cout << "sample_count: " <<  sample_count<< std::endl;

            for(const auto& meta_iter : window_metadata_)
            {
                //std::cout << "meta_iter->getRelatedSample()->index()      : " << meta_iter->getRelatedSample()->index() << std::endl;
                //std::cout << "meta_count: " << std::endl;

                //std::cout << "sample_iter->index(): " << sample_iter->index() << std::endl;

                // For single sample mode, some samples will have two meta items. A raw one, and an item which holds a WRContext.
                // Ignore one of both in order to prevent duplicate, additional stamps
                if(meta_iter->getRelatedSample().index() == sample_iter.index())
                {
                    timesSinceRefMeta[sample_count] = (float(int64_t(meta_iter->getRelatedSampleStamp() - ref_meta_stamp))/ 1000000000.);
                    break;
                }
            }

            if(sample_iter.index() == data_end_.index() )
                break;
            ++sample_iter;
            ++sample_count;
        }
    }
    else
    {
        if( foreign_ref_meta_stamp != 0 )
            throw RUNTIME_ERROR("Support for foreign ref_meta_stamp so far only for DataPushMode::DISJUNCT_SINGLE_SAMPLES");

        if(!ref_meta_)
        {
            throw RUNTIME_ERROR("The CircularWindowIterator'" + name_ + "' does not have a ref_meta (yet)");
        }

        // Will be positive when WREvent happened after the related sample
        int64_t wREventOffsetToSample_ns = int64_t(ref_meta_stamp - ref_meta_->getRelatedSampleStamp());

        double distanceRefMetaToWindow_time = double(distanceRefMetaToWindow()) * sample_to_sample_distance_ - double(wREventOffsetToSample_ns) / 1000000000.;

        double stamp = distanceRefMetaToWindow_time;
        for (std::size_t i=0;i< windowSize();i++ )
        {
            timesSinceRefMeta[i] = (float(stamp));
            stamp += sample_to_sample_distance_;
        }

        //std::cout << "first sample index : "  << data_start_->index() << " stamp: " << ref_meta_->getTimingContext()->getTimeStamp() + int64_t(timesSinceRefMeta[0] * 1000000000) << std::endl;
        //std::cout << "last sample index  : "  << data_end_->index() << " stamp: " << ref_meta_->getTimingContext()->getTimeStamp() + int64_t(timesSinceRefMeta[windowSize()-1] * 1000000000) << std::endl;
    }
}

template<typename T>
const std::vector<std::shared_ptr<SampleMetadata<T>>>& CircularWindowIterator<T>::getMetaDataCol() const
{
    return window_metadata_;
}

template<typename T>
void CircularWindowIterator<T>::prefillMetadata(std::shared_ptr<SampleMetadata<T>> metadata)
{
    window_metadata_.clear();
    window_state_ = State::UNDER_CONSTRUCTION;
    auto dataStart = metadata->getRelatedSample();
    windowValidationCheckpoint_ = circularBuffer_->calculateValidationCheckpoint(dataStart);
    window_metadata_.emplace_back(std::move(metadata));
}

template<typename T>
std::shared_ptr<SampleMetadata<T>> CircularWindowIterator<T>::getMetaStart() const
{
    if (window_metadata_.empty())
    {
        return {nullptr};
    }
    switch (window_state_)
    {
    case State::UNDER_CONSTRUCTION:
    case State::READY:
        return window_metadata_.back();
    default:
        return {nullptr};
    }
}

template<typename T>
typename CircularWindowIterator<T>::State CircularWindowIterator<T>::getWindowState() const
{
    return window_state_;
}

template<typename T>
uint32_t CircularWindowIterator<T>::getWindowStatus() const
{
    uint32_t sum_status = 0;
    for(const auto& meta_iter : window_metadata_)
      sum_status |= meta_iter->getStatus();

    return sum_status;
}

template<typename T>
int CircularWindowIterator<T>::timeSinceRefMetaSanityCheck(std::vector<float>& timeSinceRefMetaForEachSample,
                                                        uint64_t            foreign_ref_meta_stamp,
                                                        bool                enableLogging)
{
    if (timeSinceRefMetaForEachSample.empty())
        return 0;

    uint64_t ref_meta_stamp = foreign_ref_meta_stamp;
    if(ref_meta_stamp == 0)
    {
        if (!ref_meta_)
            return 0;
        if (ref_meta_->hasTimingContext() == false) // So far we only support WR Timing
            return 0;

        ref_meta_stamp = ref_meta_->getTimingContext()->getTimeStamp();
    }

    // check if timeSinceRefMeta is strictly increasing
    float last = std::nan("");
    for (auto& iter: timeSinceRefMetaForEachSample)
    {
        if (last != std::nan(""))
        {
            if (iter < last)
            {
                if (enableLogging)
                {
                    std::string message = "getTimeSinceRefMetaForEachSample - sanity check failed. timeSinceRefMeta have wrong order";
                    message += "old stamp: " + std::to_string (last) + " ";
                    message += "new stamp: " + std::to_string (iter);
                    LOG_WARNING (message);
                }
                return 1;
            }
            else if (iter == last)
            {
                if (enableLogging)
                {
                    std::string message = "getTimeSinceRefMetaForEachSample - sanity check failed. Same timeSinceRefMeta used twice";
                    message += "stamp: " + std::to_string (iter);
                    LOG_WARNING (message);
                }
                return 2;
            }
        }
        last = iter;
    }

    // check that timeSinceRefMeta from the last update does not overlap with the current update

    int64_t stamp_first_sample = ref_meta_stamp + (timeSinceRefMetaForEachSample.front() * 1000000000 );
    int64_t stamp_last_sample = ref_meta_stamp + (timeSinceRefMetaForEachSample.back() * 1000000000 );
    //std::cout << "stamp_last_sample_prev_: " << stamp_last_sample_prev_ << std::endl;
    //std::cout << "stamp_first_sample     : " << stamp_first_sample << std::endl;
    if (stamp_first_sample < stamp_last_sample_prev_)
    {
        if (enableLogging)
        {
            std::string message = "getTimeSinceRefMetaForEachSample - sanity check failed. timeSinceRefMeta across two updates have wrong order: ";
            message += "Previous last sample stamp: " + std::to_string (stamp_last_sample_prev_) + " ";
            message += "Next first sample stamp: " + std::to_string (stamp_first_sample);
            LOG_WARNING (message);
        }
        stamp_last_sample_prev_ = 0;
        return 3;
    }
    else if (stamp_first_sample == stamp_last_sample_prev_)
    {
        if (enableLogging)
        {
            std::string message = "getTimeSinceRefMetaForEachSample - sanity check failed. Same timeSinceRefMeta used twice across two updates";
            message += "stamp: " + std::to_string (stamp_last_sample_prev_);
            LOG_WARNING (message);
        }
        stamp_last_sample_prev_ = 0;
        return 4;
    }
    stamp_last_sample_prev_ = stamp_last_sample;
    return 0;
}

template<typename T>
void CircularWindowIterator<T>::updateMetaWindowAccordingToSampleWindow(bool update_ref_meta)
{
    std::shared_ptr<SampleMetadata<T>> prev_meta_end;
    if (!window_metadata_.empty())
    {
        prev_meta_end = window_metadata_.front();
    }

    window_metadata_.clear();

    if(!metadataBuffer_->empty())
    {
        bool window_found = false;
        // Add all metaitems which are located in this sample-window
        metadataBuffer_->for_each_meta_data([&] (const std::shared_ptr<SampleMetadata<T>>& iter)
        {
            if(isInWindow(iter->getRelatedSample()))
            {
                window_metadata_.push_back(iter);
                window_found = true;
            }
            else
            {
                // Once we left the window, we dont need to further search
                if(window_found)
                  return false; // this will break the for_each;
            }
            return true;
        });
    }

    // window_metadata_ contains metadata from newest to oldest.

    // Update ref meta
    if(update_ref_meta)
    {
        if(window_metadata_.empty())
        {
            if(prev_meta_end != nullptr)
            {
                // Use previous end to search for the next ref-trigger, if no new meta was found
                ref_meta_ = metadataBuffer_->findRefMeta(prev_meta_end, prev_meta_end);
            }
            // Otherwise the previous ref-trigger will just be reused/kept
        }
        else
        {
            // window_metadata_ contains metadata from newest to oldest, hence we put back() as start and front() as end.
            ref_meta_ = metadataBuffer_->findRefMeta(window_metadata_.back(), window_metadata_.front());
        }
    }
    window_state_ = State::READY;
}

template<typename T>
void CircularWindowIterator<T>::updateDataRange(
            const typename CircularBuffer<T>::iterator& start,
            const typename CircularBuffer<T>::iterator& end)
{
    data_start_ = start;
    data_end_ = end;
    calculateWindowValidationCheckpoint();
}

template<typename T>
int64_t CircularWindowIterator<T>::getTimeStampOfLastSample()
{
    if(dataPushMode_ == DataPushMode::DISJUNCT_SINGLE_SAMPLES)
    {
        if (window_metadata_.empty())
        {
            return 0;
        }
        auto meta_end = window_metadata_.front();
        if (meta_end == nullptr)
        {
            return 0;
        }
        return meta_end->getRelatedSampleStamp();
    }
    else
    {
        if(!ref_meta_)
            return 0;

        int64_t ref_meta_stamp = ref_meta_->getTimingContext()->getTimeStamp();

        // Will be positive when WREvent happened after the related sample
        int64_t wREventOffsetToSample_ns = int64_t(ref_meta_stamp - ref_meta_->getRelatedSampleStamp());

        int64_t distanceRefMetaToWindow_ns = distanceRefMetaToWindow() * sample_to_sample_distance_nano_- wREventOffsetToSample_ns;
        return ref_meta_stamp + (distanceRefMetaToWindow_ns +  (windowSize() - 1) * sample_to_sample_distance_nano_);
    }
}


template<typename T>
uint64_t CircularWindowIterator<T>::getWindowValidationCheckpoint() const
{
    return windowValidationCheckpoint_;
}

template<typename T>
const typename CircularBuffer<T>::iterator& CircularWindowIterator<T>::getDataStart() const
{
    return data_start_;
}

template<typename T>
const typename CircularBuffer<T>::iterator& CircularWindowIterator<T>::getDataEnd() const
{
    return data_end_;
}

template<typename T>
void CircularWindowIterator<T>::calculateWindowValidationCheckpoint()
{
    windowValidationCheckpoint_ = circularBuffer_->calculateValidationCheckpoint(data_start_, data_end_);
}

template<typename T>
std::ostream& operator<<(std::ostream& os, const CircularWindowIterator<T>& window)
{
    os << "WIN - Window Info for   Window : " << window.name_ << std::endl;
    os << "WIN - Write iterator      index: " << window.circularBuffer_->getCopyOfWriteIterator().index() << std::endl;
    os << "WIN - Window read iterator range(index): [" << window.data_start_.index() << "," << window.data_end_.index() << "]" << std::endl;

    if(window.window_metadata_.empty())
    {
        os << "WIN - No metadata found for that window" << std::endl;
        return os;
    }

    for(const auto& iter : window.window_metadata_)
    {
        if (!iter)
        {
            os << "WIN - Error, failed to traverse till the end !" << std::endl;
            break;
        }
        os << "WIN - " << *iter;
    }

    if(window.ref_meta_)
    	os << "WIN - ref trigger: " << *(window.ref_meta_);

    return os;
}

} /* namespace cabad */


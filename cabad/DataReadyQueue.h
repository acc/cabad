/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/Definitions.h>
#include <cabad/TimingContext.h>
#include <cabad/CircularWindowIterator.h>

#include <vector>
#include <string>
#include <deque>
#include <mutex>
#include <memory>

namespace cabad
{

// Hack:
// static idCount (and therefore as well mutex) required for FESA usage
// id needs to unique across all channels in Fesa, since it is used to find the proper channel
static std::mutex queue_mutex_;
static uint32_t queue_idCount_ = 0;

template<typename T>
class ClientNotificationData
{

public:

    ClientNotificationData(ClientNotificationType notificationType,
                           const std::string& sinkName,
                           std::shared_ptr<const TimingContext> context,
                           MaxClientUpdateFrequencyType updateFrequency,
                           CircularWindowIterator<T> window):
        id_(0),
        notificationType_(notificationType),
        context_(std::move(context)),
        sinkName_(sinkName),
        updateFrequency_(updateFrequency),
        window_(std::move(window))
    {
    }

    ClientNotificationData()
    : id_(0),
      notificationType_(ClientNotificationType::FULL_SEQUENCE),
      updateFrequency_(1)
    {
    }

    uint32_t id_;
    ClientNotificationType notificationType_;
    std::shared_ptr<const TimingContext> context_;
    std::string sinkName_;
    MaxClientUpdateFrequencyType updateFrequency_; // only relevant for streaming mode
    CircularWindowIterator<T> window_;
};

// In FESA clients are updated by sending a message through a "Notification Queue".
// Since it is not possible (yet) to transfere objects, We sent just an id_ through this FESA queue and later gather the real data from this queue by using the id
template<typename T>
class DataReadyQueue
{
public:
    DataReadyQueue(std::size_t max_size);

    uint32_t push(const std::string& sinkName,
                  const std::shared_ptr<const TimingContext>& context,
                  ClientNotificationType notificationType,
                  MaxClientUpdateFrequencyType maxClientUpdateFrequency,
                  CircularWindowIterator<T> window);

    // returns true if found, false otherwise
    bool requestData(uint32_t id, ClientNotificationData<T>& data) const;

private:
    std::deque<ClientNotificationData<T>> queue_;
    std::size_t max_size_;
};

template<typename T>
DataReadyQueue<T>::DataReadyQueue(std::size_t max_size)
    : max_size_(max_size)
{
}

template<typename T>
uint32_t DataReadyQueue<T>::push(const std::string& sinkName,
                              const std::shared_ptr<const TimingContext>& context,
                              ClientNotificationType notificationType,
                              MaxClientUpdateFrequencyType maxClientUpdateFrequency,
                              CircularWindowIterator<T> window)
{
    ClientNotificationData<T> data(notificationType, sinkName, context, maxClientUpdateFrequency, std::move(window));// update frequency is irrelevant for triggered mode
    std::lock_guard<std::mutex> lock(queue_mutex_);
    data.id_ = ++queue_idCount_;
    queue_.push_back (data);
    if(queue_.size() > max_size_)
        queue_.pop_front();
    return data.id_;
}

template<typename T>
bool DataReadyQueue<T>::requestData(uint32_t id, ClientNotificationData<T>& data ) const
{
    std::lock_guard<std::mutex> lock(queue_mutex_);
    for(auto iter = queue_.begin(); iter != queue_.end(); iter ++)
    {
        if(iter->id_ == id)
        {
            data.context_ = iter->context_;
            data.sinkName_ = iter->sinkName_;
            data.notificationType_ = iter->notificationType_;
            data.updateFrequency_ = iter->updateFrequency_;
            data.id_ = iter->id_;
            data.window_ = iter->window_;
            return true;
        }
    }
    return false;
}
} /* namespace */
